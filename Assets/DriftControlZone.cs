﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DriftControlZone : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{
	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		PhysicsBallControl.Instance.BeginJoystick ();

	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		PhysicsBallControl.Instance.JoystickControls (eventData.delta.x);
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		PhysicsBallControl.Instance.ReleaseJoystick ();

	}

	#endregion

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
