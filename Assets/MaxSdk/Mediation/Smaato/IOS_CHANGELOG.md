# Changelog

## 10.2.1.0
* Certified with SDK 10.2.1

## 10.2.0.2
* Add support for initialization status.

## 10.2.0.1
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 10.2.0.0
* Certified with SDK 10.2.0.
* GDPR fixes.

## 10.1.2.1
* GDPR fixes.
  
## 10.1.2.0
* Initial commit.
