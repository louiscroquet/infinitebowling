# Changelog

## 9.1.6.0
* Certified with Smaato SDK 9.1.6.

## 9.1.5.6
* Add support for initialization status.

## 9.1.5.5
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 9.1.5.4
* Fix `NoMethodFoundException`'s.

## 9.1.5.3
* GDPR fixes.

## 9.1.5.2
* GDPR fixes.

## 9.1.5.1
* Dynamically reference against Smaato SDK version number.

## 9.1.5.0
* Initial commit.
