# Changelog

## 2.3.0.3.0
* Certified with SDK 2.3.0.3.

## 2.1.0.2.1
* Use `fullscreenVideoAdDidVisible:` and `rewardedVideoAdDidVisible:` for the `-[MAAdDelegate didDisplayAd:]` callback. 

## 2.1.0.2.0
* Certified with SDK 2.1.0.2.
* Add support for initialization status.

## 2.0.1.2.1
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 2.0.1.2.0
* Certified with SDK 2.0.1.2.

## 1.9.8.5.0
* Certified with SDK 1.9.8.5.

## 1.9.8.2.0
* Initial commit.
