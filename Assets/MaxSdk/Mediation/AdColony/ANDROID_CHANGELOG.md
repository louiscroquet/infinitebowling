# Changelog

## 3.3.11.1
* Add proguard rule to not obfuscate public AdColony SDK classes.

## 3.3.11.0
* Certified with AdColony SDK 3.3.11.
* Add support for initialization status.
* Fix potential crash for when the AdColony expires.

## 3.3.10.1
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 3.3.10.0
* Certified with AdColony SDK 3.3.10.

## 3.3.8.1
* Update adapter logging.

## 3.3.8.0
* Initial commit.
