# Changelog

## 3.3.8.2
* Fix crash related to AdColony ad expiration.

## 3.3.8.1
* Add support for initialization status.
* Do not reload expired AdColony ads from adapter (MAX SDK will handle it).

## 3.3.8.0
* Certified with AdColony SDK 3.3.8.

## 3.3.7.2
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 3.3.7.1
* Add error code for when SDK is not initialized.
* Update adapter logging.

## 3.3.7.0
* Initial commit.
