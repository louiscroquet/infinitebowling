# Changelog

## 6.4.11.0
* Certified with Vungle SDK 6.4.11.
* Add support for initialization status.

## 6.3.24.6
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 6.3.24.5
* Update adapter logging.

## 6.3.24.4
* Dynamically reference against Vungle SDK version number.

## 6.3.24.3
* Bundle in Unity Plugin `/Assets/MaxSdk/Plugins/Android/Shared Dependencies` the following Android dependencies:
    1. `converter-gson.jar`
    2. `common.jar`
    3. `fetch.jar`
    4. `gson.jar`
    5. `okhttp.jar`
    6. `okio.jar`
    7. `retrofit.jar`

## 6.3.24.2
* Use unique package name in Android Manifest.

## 6.3.24.1
* Added Proguard rules required by Vungle SDK.

## 6.3.24.0
* Initial commit.
