# Changelog

## 6.3.2.3
* Add support for initialization status.

## 6.3.2.2
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 6.3.2.1
* Update adapter logging.

## 6.3.2.0
* Initial commit.
