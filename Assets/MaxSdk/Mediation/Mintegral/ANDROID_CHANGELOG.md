# Changelog

## 9.13.2.0
* Certified with Mintegral SDK 9.13.2.

## 9.12.4.0
* Add support for bidding.
* Check for ad readiness before showing for interstitials.
* Add support for initialization status.

## 9.10.4.1
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.
* Correctly map NO FILLs.

## 9.10.4.0
* Certified with Mintegral SDK 9.10.4 (MAL_9.10.41).

## 9.8.0.1
* Dynamically reference against Mintegral SDK version number.

## 9.8.0.0
* Certified with Mintegral SDK 9.8.0.

## 9.1.1.2
* Use unique package name in Android Manifest.

## 9.1.1.1
* Added Proguard rules required by Mintegral SDK.

## 9.1.1.0
* Initial commit.
