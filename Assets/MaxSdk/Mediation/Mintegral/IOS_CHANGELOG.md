# Changelog

## 5.5.3.0
* Certified with MIntegral SDK 5.5.3.

## 5.5.0.0
* Add support for bidding.
* Check for ad readiness before showing for interstitials and rewarded ads.
* Better error code mapping.
* Add support for initialization status.

## 5.3.2.2
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.
* Correctly map NO FILLs.

## 5.3.2.1
* Use Mintegral's new `onVideoAdDidClosed:` callback for firing [AD HIDDEN] callback.

## 5.3.2.0
* Certified with MIntegral SDK 5.3.2.

## 4.9.4.0
* Certified with MIntegral SDK 4.9.4.

## 4.7.0.0
* Initial commit.
