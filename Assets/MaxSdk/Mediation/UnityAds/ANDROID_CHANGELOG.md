# Changelog

## 3.2.0.0
* Certified with UnityAds SDK 3.2.0.
* Add support for per-placement loading. Requires whitelisted game ID and 'enable_per_placement_load' server parameter set to true on initialize.

## 3.1.0.1
* Add support for initialization status.

## 3.1.0.0
* Certified with UnityAds SDK 3.1.0.
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 3.0.3.0
* Certified with UnityAds SDK 3.0.3.

## 3.0.1.1
* Minor adapter improvements.

## 3.0.1.0
* Certified with UnityAds SDK 3.0.1.

## 3.0.0.3
* Use unique package name in Android Manifest.

## 3.0.0.2
* Removed Redundant `activity` tags from AndroidManifest.

## 3.0.0.1
* Added Proguard rules required by Unity Ads SDK.

## 3.0.0.0
* Initial commit.
