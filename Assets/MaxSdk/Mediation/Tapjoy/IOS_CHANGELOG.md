# Changelog

## 12.3.1.0
* Certified with Tapjoy SDK 12.3.1.
* Use `NSJSONReadingAllowFragments` option for deserializing bid response.
* Add support for initialization status.

## 12.3.0.1
* Add support for setting whether the user is below the age of consent or not.

## 12.3.0.0
* Certified with Tapjoy SDK 12.3.0.
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Support for tracking clicks.
* Add support for extra reward options.

## 12.2.1.1
* Minor adapter improvements.

## 12.2.1.0
* Certified with Tapjoy SDK 12.2.1.

## 12.2.0.2
* Bundle Tapjoy resources the way it is done in their Unity plugin.

## 12.2.0.1
* Set mediation to "applovin" per Tapjoy's request.

## 12.2.0.0
* Initial commit.
