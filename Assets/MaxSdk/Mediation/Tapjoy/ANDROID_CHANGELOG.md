# Changelog

## 12.3.1.0
* Add support for initialization status.

## 12.3.0.1
* Add support for setting whether the user is below the age of consent or not.

## 12.3.0.0
* Certified with Tapjoy SDK 12.3.0.
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.
* Support for tracking clicks.

## 12.2.1.1
* Minor adapter improvements.

## 12.2.1.0
* Certified with Tapjoy SDK 12.2.1.

## 12.2.0.0
* Certified with Tapjoy SDK 12.2.0.

## 12.1.0.2
* Use unique package name in Android Manifest.

## 12.1.0.1
* Added Proguard rules required by Tapjoy SDK.

## 12.1.0.0
* Initial commit.