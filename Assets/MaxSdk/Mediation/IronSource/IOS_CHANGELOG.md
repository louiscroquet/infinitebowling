# Changelog

## 6.8.4.2.0
* Certified with ironSource SDK 6.8.4.2.

## 6.8.4.0.2
* Add support for initialization status.

## 6.8.4.0.1
* Fix `didDisplayAd:` not being called due to ironSource's 6.8.4 SDK switching the interstitial callback from `interstitialDidShow:` to `interstitialDidOpen:`.

## 6.8.4.0.0
* Certified with ironSource SDK 6.8.4.0 without the rewarded video preloading logic.
* Better error code mapping.

## 6.8.3.0.3
* Fixed an edge case issue where ad load might be called even when an ad is not available.

## 6.8.3.0.2
* Minor adapter fix from previous push.

## 6.8.3.0.1
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 6.8.3.0.0
* Certified with ironSource SDK 6.8.3.0.

## 6.8.3.0.0
* Minor adapter improvements.

## 6.8.1.0.2
* Revert setting ironSource mediation type to "applovin<sdk_version_code>".

## 6.8.1.0.1
* Add extra debug logging.
* Set ironSource mediation type to "applovin<sdk_version_code>".

## 6.8.1.0.0
* Support for ironSource SDK 6.8.1.0.

## 6.8.0.0.0
* Initial commit.
