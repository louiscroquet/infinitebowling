# Changelog

## 6.8.4.0.5
* Fix rewarded video errors.

## 6.8.4.0.4
* Forward application pause/resume events to ironSource SDK.

## 6.8.4.0.3
* Add proguard rule to not obfuscate public ironSource SDK classes.

## 6.8.4.0.2
* Add transitive dependency to automatically require self-hosted ironSource SDK with matching version.

## 6.8.4.0.1
* Add support for initialization status.

## 6.8.4.0.0
* Certified with ironSource SDK 6.8.4.0 without the rewarded video preloading logic.
* Better error code mapping.

## 6.8.3.0.2
* Fixed an edge case issue where ad load might be called even when an ad is not available.

## 6.8.3.0.1
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 6.8.3.0.0
* Certified with ironSource SDK 6.8.3.0.

## 6.8.2.0.1
* Minor adapter improvements.

## 6.8.2.0.0
* Certified with ironSource SDK 6.8.2.0.

## 6.8.1.0.1
* Use unique package name in Android Manifest.

## 6.8.1.0.0
* Support for ironSource SDK 6.8.1.0.

## 6.8.0.1.1
* Removed transitive dependency on ironSource's SDK as they do not allow it.

## 6.8.0.1.0
* Initial commit.
