# Changelog

## 7.3.0.0
* Certified with InMobi SDK 7.3.0.

## 7.2.9.0
* Add support for initialization status.

## 7.2.8.1
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 7.2.8.0
* Certified with InMobi SDK 7.2.8.

## 7.2.7.3
* Update adapter logging.

## 7.2.7.2
* In the InMobi adapters Unity Plugin, moved the `picasso.jar` dependency into `Assets/MaxSdk/Plugins/Android/Shared Dependencies`.

**Please delete `picasso.jar` from the `Assets/MaxSdk/Plugins/Android/InMobi` folder:**

## 7.2.7.1
* Use `onAdWillDisplay()` instead of `onAdDisplayed()` for impression tracking.

## 7.2.7.0
* Initial commit.
