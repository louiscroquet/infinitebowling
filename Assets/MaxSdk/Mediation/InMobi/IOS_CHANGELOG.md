# Changelog

## 7.3.2.1
* Format `IM_GDPR_CONSENT_AVAILABLE` value to true/false.

## 7.3.2.0
* Certified with SDK 7.3.2.

## 7.3.1.1
* Uncertify  SDK 7.3.1, re-certify against SDK 7.2.9 due to `isReady` bug.

## 7.3.1.0
* Certified with SDK 7.3.1.
* Add support for initialization status.

## 7.2.9.0
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).
* Add support for extra reward options.

## 7.2.7.2
* Minor adapter improvements.

## 7.2.7.1
* Use `interstitialWillPresent:` instead of `interstitialDidPresent:` for impression tracking.

## 7.2.7.0
* Initial commit.
