# Changelog

## 2.2.17.0.3
* Add support for initialization status.

## 2.2.17.0.2
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).

## 2.2.17.0.1
* Fix passing in of incorrect Amazon SDK version.

## 2.2.17.0.0
* Updated to support Amazon SDK version 2.2.17.0.
* Support for GDPR.
* NOTE: For this version, please make sure to delete the previous version of the Amazon Ads SDK framework before importing this package.

## 2.2.15.1.0
* Initial commit.
