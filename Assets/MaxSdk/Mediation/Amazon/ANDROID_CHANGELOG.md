# Changelog

## 5.9.0.0.4
* Add support for initialization status.

## 5.9.0.0.3
* Add Unity support for automatic dependency resolution. Please ensure that you are on the latest [AppLovin MAX Unity Plugin](https://bintray.com/applovin/Unity/applovin-max-unity-plugin).

## 5.9.0.0.2
* Use unique package name in Android Manifest.

## 5.9.0.0.1
* Fixed a bug where price floors weren't setting for banners or interstitials.

## 5.9.0.0.0
* Updated to support Amazon SDK version 5.9.0.
* Added GDPR support.

##  5.8.1.1.0
* Initial commit.
