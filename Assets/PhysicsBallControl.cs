﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsBallControl : MonoBehaviour
{
	public static PhysicsBallControl Instance;
	[Header ("PHYSICS SETTINGS")]
	public float MovingForce = 50f;
	public float RotationForce = 50f;

	public bool DRIFT = false;
	public Transform DriftArrow;
	public float AmplitudeMax;
	public float Drift_MovingForce = 50f;

	public float Drift_RotationForce = 50f;

	Rigidbody r;

	void Awake ()
	{
		Instance = this;
	}
	// Use this for initialization
	void Start ()
	{
		r = GetComponentInChildren<Rigidbody> ();
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		SetRotation ();
		transform.Rotate (new Vector3 (0f, currentRotation * RotationMultiplier, 0f));
		//r.AddForce (PlayerModel.forward, ForceMode.VelocityChange);
		//return;

		if (DRIFT)
			DriftControls ();
		else
			StraightControls ();
		return;
	}

	public void StraightControls ()
	{

		if (Input.GetKey (KeyCode.UpArrow)) {
			//Vector3 neovelo = r.velocity;

			r.AddForce (transform.forward, ForceMode.VelocityChange);
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			r.AddTorque (Vector3.up * -RotationForce);
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			r.AddTorque (Vector3.up * RotationForce);
		}
	}

	public void DriftControls ()
	{

		if (Input.GetKey (KeyCode.UpArrow)) {
			//Vector3 neovelo = r.velocity;

			r.AddForce (DriftArrow.forward, ForceMode.VelocityChange);
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			DriftArrow.transform.localEulerAngles += Vector3.up * -Drift_RotationForce * Time.deltaTime;
			//r.AddTorque (Vector3.up * -RotationForce);
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			DriftArrow.transform.localEulerAngles += Vector3.up * Drift_RotationForce * Time.deltaTime;

			//r.AddTorque (Vector3.up * RotationForce);
		}
		//Vector3 neo = Vector3.RotateTowards (r.velocity, DriftArrow.forward * r.velocity.magnitude, Time.deltaTime, Time.deltaTime);
		//r.velocity = neo;

		
	}

	[Header ("Thumb Drift")]
	public Transform Pivot;
	public Transform PlayerModel;
	public float RotationMultiplier = 1f;
	public float MaxAngle = 45f;
	[Range (-1f, 1f)]
	public float DEBUG_Slide = 0f;

	public float currentRotation = 0f;

	public void SetRotation ()
	{
		//Pivot.transform.localEulerAngles = new Vector3 (0f, currentRotation, 0f);
		currentRotation = Mathf.Clamp (currentRotation, -MaxAngle, MaxAngle);
		PlayerModel.transform.localEulerAngles = new Vector3 (0f, -currentRotation, 0f);
	}

	[Header ("JOYSTICK")]
	public float Sensitivity;
	public float ReturnToZero = 0.25f;

	public void BeginJoystick ()
	{
		LeanTween.cancel (gameObject);	
		//	LeanTween.value (gameObject, currentRotation, 0f, ReturnToZero);
	}

	public void JoystickControls (float f)
	{
		f *= Sensitivity;
		//f = Mathf.Clamp (f, -1f, 1f);
		currentRotation += f * MaxAngle;



	}


	public void ReleaseJoystick ()
	{
		LeanTween.cancel (gameObject);	
		LeanTween.value (gameObject, ltUpdateRotation, currentRotation, 0f, ReturnToZero);
	}

	void ltUpdateRotation (float f)
	{
		currentRotation = f;
	}

}
