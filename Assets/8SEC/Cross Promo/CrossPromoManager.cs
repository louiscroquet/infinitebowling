﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using GameAnalyticsSDK;

[System.Serializable]
public struct  CrossPromoGameInfos
{
    public string GameTitle;
    public string GetURL;
    public VideoClip VideoPreview;
}
public class CrossPromoManager : MonoBehaviour
{

    
    public Text GameTitleDisplayer;
    public VideoPlayer PreviewPlayer;

    public static int CurrentGameDisplayedIndex;
    public CrossPromoGameInfos CurrentGameDisplayed
    {
        get
        {
            return AvailableGames[CurrentGameDisplayedIndex];
        }
    }
    public List<CrossPromoGameInfos> AvailableGames = new List<CrossPromoGameInfos>();
    // Start is called before the first frame update
    void Start()
    {
        DisplayNextGame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayGame(int i)
    {
        CurrentGameDisplayedIndex = i;

        GameTitleDisplayer.text = CurrentGameDisplayed.GameTitle;
        PreviewPlayer.clip = CurrentGameDisplayed.VideoPreview;
    }

    public void Action()
    {

        Application.OpenURL(CurrentGameDisplayed.GetURL);
        DisplayNextGame();

        GameAnalytics.NewDesignEvent("CrossPromo:Clicked:" + CurrentGameDisplayed.GameTitle);

    }

    [ContextMenu("Show Next preview")]
    public void DisplayNextGame()
    {
        CurrentGameDisplayedIndex++;
        if (CurrentGameDisplayedIndex >= AvailableGames.Count)
            CurrentGameDisplayedIndex = 0;
        DisplayGame(CurrentGameDisplayedIndex);

    }
}
