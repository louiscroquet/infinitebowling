﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackManager : MonoBehaviour {

    public static FeedbackManager Instance;

    public ParticleSystem smokeParticles;
    public ParticleSystem explosionParticles;

    public List<TextMeshPro> scoreFeedbackTexts = new List<TextMeshPro> ();

    public Image bombFlash;

    public Text timeBonusText;

    private bool scoreFeedbackIsPlaying;
    private int scoreChain;

    public RectTransform SlowMoVignette;

    public ParticleSystem leftFogFireworks;
    public ParticleSystem rightFogFireworks;

    public ParticleSystem leftMultiplierFireworks;
    public ParticleSystem rightMultiplierFireworks;

    public GameObject cam;

    public List<ParticleSystem> PinExplosionParticles = new List<ParticleSystem> ();

    public ParticleSystem ObstacleDestroyedFX;

    public ParticleSystem rewardParticles;
    public RectTransform mainUi;

    void Awake () {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        //ComboSlider.maxValue = GameManager.Instance.FireOnCombo;
        //StopPlayFeedbackScore ();
        //ComboFeedback3D.transform.localScale = Vector3.zero;
        foreach (TextMeshPro scoreTexts in scoreFeedbackTexts) {
            //scoreTexts.GetComponent<Renderer> ().sortingLayerName = "Score";
            scoreTexts.useGUILayout = true;
        }

    }

    // Update is called once per frame
    void Update () {
#if UNITY_EDITOR
        if (Input.GetKeyDown (KeyCode.X)) {
            for (int i = 0; i < 50; i++) {
                //  Debug.Log (i + "before tween : " + LeanTween.tweensRunning);

                PlayFeedbackText (Vector3.zero, 1f, 0f, 0f, 0f, 1f, "TEST", Color.white);
                //  Debug.Log (i + "after tween : " + LeanTween.tweensRunning);
            }
            //FeedbackManager.Instance.Show3DTextFeedback("GOOD!", Player.Instance.transform.position, Color.white);
            //ShowMissedPinFeedback (Player.Instance.transform.position + Vector3.forward * 0.5f);
            //	ShowStrikeFeedback (Player.Instance.transform.position);
        }
        if (Input.GetKeyDown (KeyCode.C)) {
            LeanTween.cancelAll ();
            Debug.Log ("cancelled all tweens : " + LeanTween.tweensRunning);

        }
        if (Input.GetKeyDown (KeyCode.T)) {

            //  LeanTween.cancelAll ();
            Debug.Log ("running tweens : " + LeanTween.tweensRunning);

        }
#endif
    }

    private int scorePlayedNumber;

    public void PlayFeedbackText (Vector3 pos, float size, float forwardStartingOffset, float verticalStartingOffset, float verticalAnimOffset, float animDuration, string sentenceToDisplay, Color color, Transform parent = null) {
        // Find available text

        if (LeanTween.tweensRunning > 500) {
            Debug.LogError ("Clearing feedback tweens" + LeanTween.tweensRunning);

            foreach (TextMeshPro feedbackText in scoreFeedbackTexts) {
                LeanTween.cancel (feedbackText.gameObject);

            }
            Debug.LogError ("Cleared feedback tweens" + LeanTween.tweensRunning);

        }

        TextMeshPro text = null;

        foreach (TextMeshPro feedbackText in scoreFeedbackTexts) {

            if (LeanTween.isTweening (feedbackText.gameObject) == false) {
                text = feedbackText;
                break;
            }
        }

        // si pas de text dispo, on en crée un et on l'ajoute à la liste (ben ouais tant qu'à faire morray)
        if (text == null) {
            text = (TextMeshPro) Instantiate (scoreFeedbackTexts[0]);
            ResetObject (text);
            scoreFeedbackTexts.Add (text);
            text.transform.parent = this.transform;
        }

        // Change text
        text.text = sentenceToDisplay;

        // Move text
        Vector3 newPos = pos;
        //  newPos.x = Mathf.Clamp(pos.x + Random.Range(-1f, 1f), -2.5f, 2.5f);
        newPos.z = newPos.z + forwardStartingOffset;
        //    newPos.y = newPos.y + verticalStartingOffset + Random.Range(-0.5f, 0.5f);
        //newPos.y = 10f;
        //newPos.y = newPos.y + verticalStartingOffset;
        text.transform.position = newPos;
        //LeanTween.moveY (text.gameObject, text.transform.position.y + verticalAnimOffset, animDuration / 2f).setIgnoreTimeScale (true);
        //LeanTween.moveY (text.gameObject, 0f, animDuration / 2f).setIgnoreTimeScale (true);

        text.transform.localEulerAngles = new Vector3 (15, 0f, Random.Range (-12f, 12f));

        // Update alpha
        Color startingTextColor = color;
        startingTextColor.a = 1f;
        text.color = startingTextColor;
        //LeanTween.alpha (text.gameObject, 0f, animDuration / 2f).setDelay (animDuration / 2f).setIgnoreTimeScale (true);

        // Scale text
        text.fontSize = size;
        text.transform.localScale = Vector3.zero;
        LeanTween.scale (text.gameObject, Vector3.one, 0.1f).setEaseInCubic ().setEaseOutBack ().setIgnoreTimeScale (true);
        LeanTween.scale (text.gameObject, Vector3.zero, 0.5f).setDelay (0.25f).setEaseInBack ().setIgnoreTimeScale (true);

        if (parent != null)
            text.transform.parent = parent;
        //  LeanTween.moveLocalZ(text.gameObject, -20f, animDuration * 5f);

        // Call object reset
        //LeanTween.delayedCall (text.gameObject, 3f, ResetObject).setOnCompleteParam (text as Object).setIgnoreTimeScale (true);

        scorePlayedNumber++;
    }

    public void DisableFeedbackTexts () {
        foreach (var txt in scoreFeedbackTexts) {
            if (txt != null) {
                txt.enabled = false;
            }

        }
    }

    public void PlayBonusTimeFeedback (float timeValue) {
        timeBonusText.text = "+ " + timeValue + " sec!";
        LeanTween.cancel (timeBonusText.gameObject);
        timeBonusText.transform.localScale = Vector3.zero;
        LeanTween.scale (timeBonusText.gameObject, Vector3.one * 1.25f, 0.5f).setEaseInCubic ();
        LeanTween.scale (timeBonusText.gameObject, Vector3.zero, 0.2f).setEaseInCubic ().setDelay (2f);
    }

    void StopPlayBonusTimeFeedback () {

    }

    public void PlaySmokeParticles (Vector3 pos, Vector3 orientation) {
        return;
        Vector3 newPos = pos;
        smokeParticles.transform.forward = orientation;
        newPos.z += 0.75f;
        smokeParticles.transform.position = newPos;
        smokeParticles.Play ();
    }

    public void PlayExplosionParticles (Vector3 pos) {
        Vector3 newPos = pos;
        newPos.z += 1.5f;

        explosionParticles.transform.position = newPos;
        explosionParticles.Play ();
    }

    public void PlayRewardParticles () {
        Vector3 pos = new Vector3 (Input.mousePosition.x / Screen.width * mainUi.sizeDelta.x, Input.mousePosition.y / Screen.height * mainUi.sizeDelta.y, Input.mousePosition.z);
        rewardParticles.transform.localPosition = pos; // Input.mousePosition;
        rewardParticles.Play ();
    }

    void ResetObject (object obj) {

        TextMeshPro textToReset = obj as TextMeshPro;
        //LeanTween.alpha(textToReset.gameObject, 1f, 0f);
        textToReset.color = Color.white;
        LeanTween.cancel (textToReset.gameObject);
        //textToReset.transform.localScale = Vector3.zero;
    }

    int ltflash;

    public void PlayFlash () {

        LeanTween.cancel (ltflash);
        ltflash = LeanTween.value (gameObject, UpdateBombFlash, 0, 1f, 0.1f).setLoopPingPong (1).setIgnoreTimeScale (true).id;
    }

    public void PlayFlashInstant () {

        LeanTween.cancel (ltflash);
        ltflash = LeanTween.value (gameObject, UpdateBombFlash, 0.175f, 0f, 0.15f).setIgnoreTimeScale (true).id;
    }

    void UpdateBombFlash (float color) {
        bombFlash.color = new Color (1, 1, 1, color);
    }

    private TextMesh textToScale;

    void ScaleText (float value) {
        textToScale.gameObject.transform.localScale = new Vector3 (value, value, 1f);
        //currentScoreFeedbackText.transform.position = new Vector3 (currentScoreFeedbackText.transform.position.x, startingPosText + 1f + value, 0f);

    }

    private TextMesh textToUpdateColor;

    void UpdateColor (float alpha) {
        if (textToUpdateColor != null) {
            textToUpdateColor.color = new Color (textToUpdateColor.color.r, textToUpdateColor.color.g, textToUpdateColor.color.b, alpha);
        }

    }

    private Color startBlinkingColor;
    private SpriteRenderer plateformRenderer;

    public void BeginBlink (SpriteRenderer sr) {
        startBlinkingColor = sr.color;
        plateformRenderer = sr;
        Invoke ("Blink", 0.2f);

    }

    void Blink () {
        if (plateformRenderer.color == Color.white) {
            plateformRenderer.color = startBlinkingColor;
        } else {
            plateformRenderer.color = Color.white;
        }
        Invoke ("Blink", 0.2f);
    }

    public void PlayFogFireworks (Vector3 pos) {
        Vector3 newPos = pos;
        newPos.x = -5f;
        newPos.y = 0f;
        newPos.z += 20f;
        leftFogFireworks.transform.position = newPos;

        newPos.x = 5f;
        rightFogFireworks.transform.position = newPos;

        leftFogFireworks.Play ();
        rightFogFireworks.Play ();

    }

    public void PlayMultiplierFireworks (Vector3 pos) {
        Vector3 newPos = pos;
        newPos.x = -5f;
        newPos.z += 10f;
        leftMultiplierFireworks.transform.position = newPos;

        newPos.x = 5f;
        rightMultiplierFireworks.transform.position = newPos;

        leftMultiplierFireworks.Play ();
        rightMultiplierFireworks.Play ();

    }

    public void PlayTimeBonusText (Vector3 pos, float size, float forwardStartingOffset, float verticalStartingOffset, float verticalAnimOffset, float animDuration, string sentenceToDisplay) {

    }

    public void Screenshake () {
        return;
        LeanTween.cancel (cam);
        cam.transform.position = Vector3.zero;
        LeanTween.moveY (cam, Random.Range (-0.2f, 0.2f), 0.02f).setLoopPingPong (3);
        LeanTween.moveY (cam, Random.Range (-0.3f, 0.3f), 0.02f).setLoopPingPong (3);
    }

    public void ZoomEffect () {
        cam.transform.position = Vector3.zero;
        LeanTween.moveZ (cam, -5f, 0.2f).setEaseInOutCubic ().setLoopPingPong (1);
    }

    public GameObject StrikeFeedbackGO;
    public Text StrikeFeedbackText;
    public FollowGameObject StrikeFollowPlayer;

    public void ShowStrikeFeedback (Vector3 p) {
        ShowStrikeFeedback (p, -1);
    }

    public void ShowStrikeFeedback (Vector3 p, int score) {
        if (score > 0)
            StrikeFeedbackText.text = "STRIKE!\n<size=250>" + score.ToString () + "$</size>";
        else
            StrikeFeedbackText.text = "STRIKE!";

        if (OnFire) {
            Show3DFireTextFeedback (StrikeFeedbackText.text, p + Vector3.up * 3f, Color.white);
            return;
        }

        //		Debug.Log ("strike feedback : " + p.ToString ());

        LeanTween.cancel (StrikeFeedbackGO);
        StrikeFeedbackGO.transform.localScale = Vector3.zero;

        Vector3 res = p + new Vector3 (0f, 1f, -10f); //7.5f);
        StrikeFeedbackGO.transform.localEulerAngles = new Vector3 (0f, 0f, Random.Range (-7.5f, 7.5f)); //p.x / 5f * 25f);

        res.x = Mathf.Clamp (res.x, -2f, 2f);
        //StrikeFeedbackGO.transform.position = res;

        if (StrikeFollowPlayer != null)
            StrikeFollowPlayer.StartFollow ();
        LeanTween.cancel (StrikeFeedbackGO);
        LeanTween.scale (StrikeFeedbackGO, Vector3.one, 0.25f).setEase (LeanTweenType.easeOutBack);
        LeanTween.scale (StrikeFeedbackGO, Vector3.zero, 0.5f).setDelay (0.5f).setEase (LeanTweenType.easeInBack).setOnStart (stopStrikeFollow);

        //LeanTween.moveZ (StrikeFeedbackGO, StrikeFeedbackGO.transform.position.z + 20f, 0.5f);
    }

    void stopStrikeFollow () {
        if (StrikeFollowPlayer != null)
            StrikeFollowPlayer.StopFollow ();
    }

    public Text TextFeedback;
    public GameObject MayhemFeedbackGO;
    public FollowGameObject MayhemFollowPlayer;

    public Text FireTextFeedback;
    public GameObject FireTextFeedbackGO;
    public FollowGameObject FireTextFollowPlayer;

    public void ShowMayhemFeedback (Vector3 p) {
        //Debug.Log ("mayhem feedback : " + p.ToString ());
        TextFeedback.text = "MAYHEM!";
        TextFeedback.color = Color.white;
        LeanTween.cancel (MayhemFeedbackGO);
        MayhemFeedbackGO.transform.localScale = Vector3.zero;

        Vector3 res = p + new Vector3 (0f, 3f, 0f); //7.5f);
        MayhemFeedbackGO.transform.localEulerAngles = new Vector3 (0f, 0f, p.x / 5f * 25f);

        res.x = Mathf.Clamp (res.x, -1f, 1f);
        MayhemFeedbackGO.transform.position = res;

        MayhemFollowPlayer.StartFollow ();
        LeanTween.cancel (MayhemFeedbackGO);
        LeanTween.scale (MayhemFeedbackGO, Vector3.one, 0.25f).setEase (LeanTweenType.easeOutBack);
        LeanTween.scale (MayhemFeedbackGO, Vector3.zero, 0.5f).setDelay (0.5f).setEase (LeanTweenType.easeInBack).setOnStart (stopMayhemFollow);
    }

    public TextMeshPro TMPFeedback;
    public void Show3DTMPFeedback (string txt, Vector3 p, float duration) {
        var f = Instantiate (TMPFeedback, null);

        f.text = txt;

        //LeanTween.cancel (MayhemFeedbackGO);
        f.transform.localScale = Vector3.zero;

        Vector3 res = p; //+new Vector3 (0f, 2f, 2f);
        res.x += Random.Range (-1f, 1f);
        res.y += Random.Range (0f, 1f);
        f.transform.localEulerAngles = new Vector3 (0f, 0f, Random.Range (-7.5f, 7.5f)); // p.x / 5f * 25f);
        //Debug.Log("text feedback : " + f.transform.localEulerAngles.ToString());

        //		res.x = Mathf.Clamp (res.x, -1f, 1f);
        f.transform.position = res;

        //f.transform.rotation = Player.Instance.transform.rotation;
        //f.transform.forward = Player.Instance.JoystickCam.transform.forward;
        //	MayhemFollowPlayer.StartFollow ();
        LeanTween.cancel (f.gameObject);
        LeanTween.scale (f.gameObject, Vector3.one, 0.15f).setEase (LeanTweenType.easeOutBack);
        LeanTween.scale (f.gameObject, Vector3.zero, 0.5f).setDelay (duration).setEase (LeanTweenType.easeInBack);
        //  LeanTween.alpha(f.gameObject, 0f, 0.5f).setDelay(duration).setEase(LeanTweenType.easeInBack);
    }

    public void Show3DTextFeedback (string txt, Vector3 p, Color c) {
        Show3DTextFeedback (txt, p, c, 0.5f);
    }

    public void Show3DTextFeedback (string txt, Vector3 p, Color c, float duration) {
        var f = Instantiate (TextFeedback, TextFeedback.transform.parent);

        /*if (OnFire) {
			Show3DFireTextFeedback (txt, p, c, duration);

			return;
		}*/
        //	Debug.Log ("text feedback : " + p.ToString ());
        f.text = txt;
        f.color = c;

        //LeanTween.cancel (MayhemFeedbackGO);
        f.transform.localScale = Vector3.zero;

        Vector3 res = p; //+new Vector3 (0f, 2f, 2f);
        res.x += Random.Range (-1f, 1f);
        res.y += Random.Range (0f, 1f);
        f.transform.localEulerAngles = new Vector3 (0f, 0f, Random.Range (-7.5f, 7.5f)); // p.x / 5f * 25f);
        //Debug.Log("text feedback : " + f.transform.localEulerAngles.ToString());

        //		res.x = Mathf.Clamp (res.x, -1f, 1f);
        f.transform.position = res;

        //f.transform.rotation = Player.Instance.transform.rotation;
        //f.transform.forward = Player.Instance.JoystickCam.transform.forward;
        //	MayhemFollowPlayer.StartFollow ();
        LeanTween.cancel (f.gameObject);
        LeanTween.scale (f.gameObject, Vector3.one, 0.15f).setEase (LeanTweenType.easeOutBack);
        LeanTween.scale (f.gameObject, Vector3.zero, 0.5f).setDelay (duration).setEase (LeanTweenType.easeInBack).setOnStart (stopMayhemFollow);
        LeanTween.alpha (f.gameObject, 0f, 0.5f).setDelay (duration).setEase (LeanTweenType.easeInBack).setOnStart (stopMayhemFollow);
    }

    void stopMayhemFollow () {
        if (MayhemFollowPlayer != null)
            MayhemFollowPlayer.StopFollow ();
    }

    public void Show3DFireTextFeedback (string txt, Vector3 p, Color c) {
        Show3DFireTextFeedback (txt, p, c, 1f);
    }

    int ltfireScale = -1;

    public void Show3DFireTextFeedback (string txt, Vector3 p, Color c, float duration) {
        //	Debug.Log ("text feedback : " + p.ToString ());
        FireTextFeedback.text = txt;
        //FireTextFeedback.color = c;

        LeanTween.cancel (ltfireScale);
        FireTextFeedbackGO.transform.localScale = Vector3.zero;

        Vector3 res = p + new Vector3 (0f, 0f, -2f); //7.5f);
        FireTextFeedbackGO.transform.localEulerAngles = new Vector3 (0f, 0f, Random.Range (-7.5f, 7.5f)); //p.x / 5f * 25f);

        //	res.x = Mathf.Clamp (res.x, -1f, 1f);
        FireTextFeedbackGO.transform.position = res;
        MayhemFeedbackGO.transform.rotation = Player.Instance.transform.rotation;

        //	FireTextFollowPlayer.StartFollow ();
        //LeanTween.cancel (FireTextFeedbackGO);
        ltfireScale = LeanTween.scale (FireTextFeedbackGO, Vector3.one, 0.25f).setEase (LeanTweenType.easeOutBack).id;
        ltfireScale = LeanTween.scale (FireTextFeedbackGO, Vector3.zero, 0.5f).setDelay (duration).setEase (LeanTweenType.easeInBack).setOnStart (stopFireTextFollow).id;
    }

    void stopFireTextFollow () {
        if (FireTextFollowPlayer != null)
            FireTextFollowPlayer.StopFollow ();
    }

    public GameObject MissedPinFeedbackGO;
    public FollowGameObject MissedPinFollowPlayer;

    public void ShowMissedPinFeedback (Vector3 p) {

        Debug.Log ("missed pin feedback : " + p.ToString ());

        LeanTween.cancel (StrikeFeedbackGO);
        MissedPinFeedbackGO.transform.localScale = Vector3.zero;

        Vector3 res = p; // + new Vector3 (0f, 3f, 5f);
        MissedPinFeedbackGO.transform.localEulerAngles = new Vector3 (0f, 0f, p.x / 5f * 25f);

        res.x = Mathf.Clamp (res.x, -1f, 1f);
        res.y = 2f;
        res.z = p.z;
        MissedPinFeedbackGO.transform.position = res;

        if (MissedPinFollowPlayer != null)
            MissedPinFollowPlayer.StartFollow ();
        LeanTween.cancel (MissedPinFeedbackGO);
        LeanTween.scale (MissedPinFeedbackGO, Vector3.one, 0.25f).setEase (LeanTweenType.easeOutBack);
        LeanTween.scale (MissedPinFeedbackGO, Vector3.zero, 0.5f).setDelay (0.5f).setEase (LeanTweenType.easeInBack).setOnStart (stopMissedFollow);
    }

    void stopMissedFollow () {
        if (MissedPinFollowPlayer != null)
            MissedPinFollowPlayer.StopFollow ();
    }

    public Text ComboFeedback3D;
    public Gradient ComboFeedbackColor;
    public int ComboColors = 50;
    int lastCombo;
    public Slider ComboSlider;
    public ParticleSystem SliderFireParticles;
    public Outline SliderFireOutline;

    public void UpdateComboFeedback (int combo) {
        return;
        ComboSlider.value = combo;
        ComboFeedback3D.color = ComboFeedbackColor.Evaluate ((float) combo / (float) ComboColors);
        //ComboFeedback3D.transform.localEulerAngles = new Vector3 (0f, 0f, 25f);
        if (combo == 0) {
            LeanTween.cancel (ComboFeedback3D.gameObject);
            LeanTween.scale (ComboFeedback3D.gameObject, Vector3.one, 0.25f);
        } else if (combo == 1) {
            ComboFeedback3D.text = combo.ToString ();

            return;
        }
        if (combo < GameManager.Instance.FireOnCombo) {
            LeanTween.cancel (ComboFeedback3D.gameObject);
            ComboFeedback3D.transform.localScale = Vector3.one;
            LeanTween.scale (ComboFeedback3D.gameObject, Vector3.one * 1.1f, 0.05f).setOnComplete (c => {
                ComboFeedback3D.text = combo.ToString ();
                LeanTween.scale (ComboFeedback3D.gameObject, Vector3.one, 0.05f);
            });
        } else if (combo >= GameManager.Instance.FireOnCombo) {
            /*
			LeanTween.cancel (ComboFeedback3D.gameObject);
			ComboFeedback3D.transform.localScale = Vector3.one;
			ComboFeedback3D.text = "FIRE!";
			LeanTween.scale (ComboFeedback3D.gameObject, Vector3.one * 1.1f, 0.05f).setLoopPingPong (1);
*/

        }

        lastCombo = combo;
    }

    public void HideComboFeedback (ComboRating cr, bool impact) {
        return;
        //	ComboFeedback3D.color = ComboFeedbackColor.Evaluate ((float)lastCombo / (float)ComboColors);
        Debug.LogError ("hide combo : ");
        if (impact) {
            ComboFeedback3D.color = Color.red;
            ComboFeedback3D.text = "Oops!";

        } else {
            ComboFeedback3D.color = cr.FeedbackColor;
            ComboFeedback3D.text = cr.Rating;
            if (cr.Length > 0) {
                ComboFeedback3D.text = cr.Rating;
                //GameManager.Instance.Score (10);
            } else {
                ComboFeedback3D.text = cr.Rating;

            }
        }

        //ComboFeedback3D.transform.localEulerAngles = Vector3.zero;
        LeanTween.cancel (ComboFeedback3D.gameObject);
        //ComboFeedback3D.transform.localScale = Vector3.one;
        LeanTween.scale (ComboFeedback3D.gameObject, Vector3.zero, 0.25f).setEase (LeanTweenType.easeInBack).setDelay (0.5f);
    }

    private bool OnFire = false;
    private int ltfire = -1;

    public void SetFire (bool state) {
        if (OnFire == state)
            return;
        OnFire = state;
        if (state) {
            if (SliderFireOutline != null)
                SliderFireOutline.effectColor = Color.white;
            if (SliderFireParticles != null)
                SliderFireParticles.Play ();
            LeanTween.cancel (ltfire);
            if (ComboSlider != null)
                ltfire = LeanTween.value (gameObject, updateFireSliderValue, ComboSlider.maxValue, 0f, GameManager.Instance.FireDuration).id;

        } else {
            if (SliderFireOutline != null)
                SliderFireOutline.effectColor = Color.black;
            if (SliderFireParticles != null)
                SliderFireParticles.Stop ();
            LeanTween.cancel (ltfire);

        }
    }

    void updateFireSliderValue (float f) {
        ComboSlider.value = f;
    }

    public void PlayPinExplosionParticles (Vector3 pos) {

        var s = PinExplosionParticles[(int) Mathf.Clamp (CI_PinExplosionParticles.PinExplosionIndex, 0, PinExplosionParticles.Count)];

        if (CaptureManager.Instance != null) {
            s = CaptureManager.Instance.pinExplosionParticles;
        }

        s.transform.position = pos;
        s.Emit (120);

    }

    public void PlayObstacleDestroyedParticles (Vector3 pos, MeshRenderer obstacleMesh) {
        if (ObstacleDestroyedFX == null)
            return;

        var shape = ObstacleDestroyedFX.shape;
        shape.meshRenderer = obstacleMesh;
        ObstacleDestroyedFX.transform.position = pos;
        ObstacleDestroyedFX.Play ();
    }
}