﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public class BonusManager : MonoBehaviour
{
	public static BonusManager Instance;
	public DateTime LastRewardCollectionTime;

	public GameObject ActiveBonusGO;
	public GameObject InactiveBonusGO;
	public Text BonusDisplayer;
	public Text CooldownDisplayer;
	public Upgrade CashBonusUpgrade;
	public int CooldownSeconds;

	private int m_CurrentBonusState;

	public int CurrentBonusState {
		get {
			return m_CurrentBonusState;
		}
	}

	public List<GameObject> BONUS_LettersGO = new List<GameObject> ();

	public int SecondsSinceLastReward {
		get {
			TimeSpan t = DateTime.Now - LastRewardCollectionTime;
			return (int)t.TotalSeconds;
		}
	}


	void Awake ()
	{
		Instance = this;
		LoadInfo ();
	}
	// Use this for initialization
	void Start ()
	{
		m_CurrentBonusState = PlayerPrefs.GetInt ("BM_CurrentBonus", 0);
		InvokeRepeating ("updateDisplay", 0f, 0.5f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		//updateDisplay ();
	}

	public void updateDisplay ()
	{
		if (SecondsSinceLastReward < CooldownSeconds) {
			ActiveBonusGO.SetActive (false);
			InactiveBonusGO.SetActive (true);
			int SecondsUntilReward = CooldownSeconds - SecondsSinceLastReward;
			int hours = SecondsUntilReward / 60 / 60;
			int min = (SecondsUntilReward - hours * 60 * 60) / 60;
			int sec = SecondsUntilReward % 60;
			string res = hours.ToString ("00") + ":" + min.ToString ("00") + ":" + sec.ToString ("00");
			/*
			res += hours.ToString ("00") + "<size=100>h</size>";
			
			res += min.ToString ("00") + "<size=100>m</size>";// + sec.ToString () + "<size=100>s</size>";

			res += sec.ToString ("00") + "<size=100>s</size>";
*/
			CooldownDisplayer.text = res;
		} else {
			ActiveBonusGO.SetActive (true);
			InactiveBonusGO.SetActive (false);
			for (int i = 0; i < 5; i++) {
				if (i < m_CurrentBonusState)
					BONUS_LettersGO [i].SetActive (true);
				else
					BONUS_LettersGO [i].SetActive (false);

			}
		}
	}

	[ContextMenu ("Collect Letter")]
	public void CollectBonusLetter ()
	{
		var l = BONUS_LettersGO [m_CurrentBonusState];
		l.transform.localScale = Vector3.zero;
		l.SetActive (true);
		LeanTween.scale (l, Vector3.one, 0.5f).setEase (LeanTweenType.easeOutBack);
		SoundManager.Instance.Play (SoundManager.Instance.BonusLetterCollect);

		m_CurrentBonusState++;
		PlayerPrefs.SetInt ("BM_CurrentBonus", m_CurrentBonusState);
		if (m_CurrentBonusState == 5) {
			Invoke ("GiveBonus", 0.5f);
			//GiveBonus ();
			LastRewardCollectionTime = DateTime.Now;
			SaveInfo ();
		}


	}

	public void GiveBonus ()
	{
		Player.Instance.Freeze (1f);
		double res = CashBonusUpgrade.CurrentUpgradeCost * 2;
		m_CurrentBonusState = 0;
		PlayerPrefs.SetInt ("BM_CurrentBonus", 0);
		LastRewardCollectionTime = DateTime.Now;
		MoneyManager.Instance.AddCash (res, false);
		SoundManager.Instance.Play (SoundManager.Instance.CollectOfflineCash);
		FeedbackManager.Instance.Show3DFireTextFeedback ("+" + res.ToString () + "$", Player.Instance.transform.position + Player.Instance.transform.up * 2f, Color.white);

		LeanTween.scale (ActiveBonusGO, Vector3.one * 1.25f, 0.5f).setEase (LeanTweenType.easeOutExpo).setOnComplete (x => {
			LeanTween.scale (ActiveBonusGO, Vector3.zero, 0.25f).setEase (LeanTweenType.easeInBack);
			
		});
	
	}



	#region SAVE/LOAD

	const string saveFilename = "bm.sec";

	public string GetSaveFilePath ()
	{
		return Path.Combine (Application.persistentDataPath, saveFilename);
	}

	public void SaveInfo ()
	{
		BinaryFormatter binary = new BinaryFormatter ();
		FileStream fStream = File.Create (GetSaveFilePath ());

		bool success = true;
		string message = "";

		try {
			binary.Serialize (fStream, LastRewardCollectionTime);
			message = "Saved file to : " + GetSaveFilePath ();
			//	Debug.Log (message);

		} catch (Exception e) {
			success = false;
			message = "Unable to serialize Daily Reward: " + e.ToString ();
			//Debug.Log (message);
		}
		fStream.Close ();



	}

	public void LoadInfo ()
	{

		bool success = true;
		string message = "";
		if (File.Exists (GetSaveFilePath ())) {
			BinaryFormatter binary = new BinaryFormatter ();
			FileStream fStream = File.Open (GetSaveFilePath (), FileMode.Open);
			try {
				LastRewardCollectionTime = (DateTime)binary.Deserialize (fStream);
				message = "Loaded file from : " + GetSaveFilePath ();
				//Debug.Log (message);

			} catch (Exception e) {
				success = false;
				message = "Unable to deserialize Daily infos: " + e.ToString ();
				Debug.Log (message);
			}

			fStream.Close ();

		} else {
			success = false;
			message = "No file to load at path : " + GetSaveFilePath ();
			Debug.LogWarning (message);
			LastRewardCollectionTime = DateTime.MinValue;
			SaveInfo ();
		}

	}


	#endregion
}
