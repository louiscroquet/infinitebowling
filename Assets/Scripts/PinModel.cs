﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinModel : MonoBehaviour {

    public MeshRenderer PinBodyRenderer;
    public MeshRenderer PinBorderRenderer;
}
