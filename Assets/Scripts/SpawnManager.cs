﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
	
	public static SpawnManager Instance;
	//public float HeadStartZ = 0f;
	public Pin PinModelGO;
	public PinGroup PinGroupGO;

    public List<PinModel> pinModels = new List<PinModel>();
    public List<Pin> spawnedPins = new List<Pin>();


	[Header ("Curve Patterns")]
	public int PinsPerCurve = 15;
	public int PinGroupPercentage = 15;
	[Header ("PATTERNS")]
	public Transform GROUND_Parent;
	public Transform SECTIONS_Parent;
	public Transform TRANSITIONS_Parent;
	public GameObject GroundModelGO;
	public float GroundLength = 100f;

	public GameObject startingSection;
	private int spawnedSectionNb;
	public Pattern successPattern;
	public List<Pattern> patterns = new List<Pattern> ();
	public List<Pattern> BonusPatterns = new List<Pattern> ();
	public List<Pattern> BonusLetterPatterns = new List<Pattern> ();

	private Pattern lastPatternSpawned;

	public float currentSpawnDistanceZ;
	public float maxSpawnPosX;

	public float minRandomSpawnDistance;
	public float maxRandomSpawnDistance;

	public int BonusSectionEveryX = 6;

	public float singlePinSpawnPercentage;
	public float pinGroupSpawnPercentage;
	public float boosterSpawnPercentage;

	public List<Pattern> spawnedPatterns = new List<Pattern> ();

	public float timerActivationPercentage;


	public DistanceMark distanceMark;
	private DistanceMark lastDistanceMarkSpawned;

	// BACKGROUND COLORS
	[Header ("COLORS")]
	public float distanceBetweenColorswitch;
	public List<Material> materialsA = new List<Material> ();
	public List<Material> materialsB = new List<Material> ();
	public List<Color> distanceMarkColors = new List<Color> ();

	public List<Color> fogColors = new List<Color> ();

	public List<Color> stickerColors = new List<Color> ();

	private List<MeshRenderer> grounds = new List<MeshRenderer> ();
	private List<ThemeSwitcher> spawnedTransitions = new List<ThemeSwitcher> ();


	[Header ("Static Levels")]
	public bool StaticLevels = true;

	private void Awake ()
	{
		Instance = this;
		lastDistanceMarkSpawned = distanceMark;
        
	}

	// Use this for initialization
	void Start ()
	{
        if (CaptureManager.Instance != null)
        {
            currentSpawnDistanceZ = CaptureManager.Instance.overrideCurrentSpawnZ;
        }

    }

    void LateUpdate()
    {
        if (canInstantiatePins)
        {
            InstantiatePinsModels();
        }
    }

	public float DistanceBetweenTransitions = 1000f;
	public ThemeSwitcher TransitionPatternGO;

	public void ClearEndlessLevel ()
	{
		foreach (Transform t in GROUND_Parent) {
			Destroy (t.gameObject);
		}

		foreach (Transform t in SECTIONS_Parent) {
			Destroy (t.gameObject);
		}

		foreach (Transform t in TRANSITIONS_Parent) {
			Destroy (t.gameObject);
		}
	}

    public void SpawnEndlessLevel ()
	{
		spawnedSectionNb = 0;
        currentSpawnDistanceZ = 50;

        // si il y a déjà un niveau spawné, on le détruit
        /*if(spawnedPatterns.Count > 0)
        {
            for (int i = 0; i < spawnedPatterns.Count; i++)
            {
                Destroy(spawnedPatterns[i].gameObject);
            }
            spawnedPatterns.Clear();
        }*/

        ClearEndlessLevel();

		//currentSpawnDistanceZ = 0f;
		lastGroundZ = 0f;

		SpawnEndlessSection ();
		//SpawnEndlessSection ();


		return;
		//SpawnEndlessSection ();
		//SpawnEndlessSection ();


		for (int i = 0; i < 50; i++) {
			SpawnGround ();
			Spawn ();
		}
		transitions = GameManager.Instance.CurrentEndlessLevel + 2;
		//foreach (var f in ThemeDistances) {

		for (int i = 1; i < 50; i++) {
			SpawnTransition (DistanceBetweenTransitions * i);

		
		}

		ApplySpawnedPatternsToCurve ();
	
	}

	public void SpawnEndlessSection ()
	{
		//return;
		float currentZ = currentSpawnDistanceZ;
		float targetZ = currentZ + (DistanceBetweenTransitions + 50f + LevelManager.Instance.CurrentLevelLength);
        Debug.Log("target" + targetZ.ToString());
		while (currentSpawnDistanceZ < targetZ) {
			SpawnGround ();
			Spawn ();
		}
		SpawnTransition (DistanceBetweenTransitions * (spawnedTransitions.Count + 1));
		ApplySpawnedPatternsToCurve ();

	}

	public void InitSpawn (int levelIndex)
	{
		//	if (levelIndex == 0)
		//		currentSpawnDistanceZ += 100f;

		for (int i = 0; i < patterns.Count; i++) {
			if (patterns [i].MinimumLevel > LevelManager.Instance.CurrentLevel) {
				//Debug.Log ("removing : " + patterns [i].name);

				patterns.RemoveAt (i);
				i--;
			}
		}

        if (StaticLevels)
        {
            Random.InitState(levelIndex);
        }

        
        SpawnEndlessLevel ();


        //InstantiatePinsModels();
        canInstantiatePins = true;
        //Invoke("InstantiatePinsModels", 0.1f);


        return;

        for (int i = 0; i < LevelManager.Instance.CurrentLevelSectionTotal; i++) {

			Spawn ();
			SpawnGround ();
		}
		SpawnSuccess ();
	}

    private PinModel currentPinModel;
    private bool canInstantiatePins;
    void InstantiatePinsModels()
    {
        //Debug.LogError("spawned pins count: " + spawnedPins.Count.ToString());
        canInstantiatePins = false;

        currentPinModel = pinModels[(int)Mathf.Clamp(CI_PinModel.PinModelIndex, 0, pinModels.Count)];

        if(currentPinModel == null)
        {
            Debug.LogError("No pin model found");
        }

        for (int i = 0; i < spawnedPins.Count; i++)
        {
            if (spawnedPins[i] != null)
            {
                PinModel pm = (PinModel)Instantiate(currentPinModel, spawnedPins[i].transform);
                pm.transform.localScale = Vector3.one;
                pm.transform.localPosition = Vector3.zero;
                pm.gameObject.SetActive(true);
                spawnedPins[i].PinBodyRenderer = pm.PinBodyRenderer;
                spawnedPins[i].PinBorderRenderer = pm.PinBorderRenderer;
            }
        }

    }

	private bool levelOver = false;
	public float SuccessZ;

	public void SpawnSuccess ()
	{
		if (levelOver || GameManager.Instance.ENDLESS_MODE)
			return;
		levelOver = true;
		spawnedSectionNb++;

		float randomDistanceZ = Random.Range (minRandomSpawnDistance, maxRandomSpawnDistance);
		//float randomPositionX = Random.Range(maxSpawnPosX * -1f, maxSpawnPosX);

		currentSpawnDistanceZ += randomDistanceZ;
		SuccessZ = currentSpawnDistanceZ;
		Vector3 randomPosition = new Vector3 (0f, 0f, currentSpawnDistanceZ);

		if (successPattern != null) {
			lastPatternSpawned = Instantiate (successPattern, randomPosition, Quaternion.identity);
			lastPatternSpawned.gameObject.SetActive (true);
		} else {
			Debug.LogError ("No pattern to spawn");
		}

		if (lastPatternSpawned != null) {
			//lastPatternSpawned.transform.position = randomPosition;

			currentSpawnDistanceZ += lastPatternSpawned.trigger.transform.localPosition.z;



			// Optiiiii
			spawnedPatterns.Add (lastPatternSpawned);
			/*
			if (spawnedPatterns.Count > 20) {

				if (startingSection != null)
					Destroy (startingSection);

				Destroy (spawnedPatterns [0].gameObject);
				spawnedPatterns.Remove (spawnedPatterns [0]);


			}

			*/

		}
	}

	[Header ("Bonus Letters Settings")]
	public int minSectionForBonusLetter;
	public int maxSectionForBonusLetter;
	public float bonusLetterPercentage;
	bool spawnedBonusLetter = false;

	public void SpawnBonusLetter ()
	{
		if (spawnedBonusLetter)
			return;
		if (levelOver)
			return;

		if (BonusManager.Instance.SecondsSinceLastReward < BonusManager.Instance.CooldownSeconds) {
			return;
		}


		int indexpattern = BonusManager.Instance.CurrentBonusState;

		spawnedSectionNb++;

		float randomDistanceZ = Random.Range (minRandomSpawnDistance, maxRandomSpawnDistance);
		//float randomPositionX = Random.Range(maxSpawnPosX * -1f, maxSpawnPosX);

		currentSpawnDistanceZ += randomDistanceZ;

		Vector3 randomPosition = new Vector3 (0f, 0f, currentSpawnDistanceZ);
		//Debug.Log ("spawn : " + BonusPatterns [randomPattern].name + " at " + randomPosition.ToString ());
		if (BonusLetterPatterns [indexpattern] != null) {
			lastPatternSpawned = Instantiate (BonusLetterPatterns [indexpattern], randomPosition, Quaternion.identity, SECTIONS_Parent);
			lastPatternSpawned.gameObject.SetActive (true);
			spawnedBonusLetter = true;

		} else {
			Debug.LogError ("No pattern to spawn");
		}
		lastPatternSpawned.spawnedInLevel = (int)currentSpawnDistanceZ / (int)DistanceBetweenTransitions;

		if (lastPatternSpawned != null) {
			//lastPatternSpawned.transform.position = randomPosition;

			currentSpawnDistanceZ += lastPatternSpawned.trigger.transform.localPosition.z;



			// Optiiiii
			spawnedPatterns.Add (lastPatternSpawned);

			if (spawnedPatterns.Count > 20) {
				/*	
				if (startingSection != null)
					Destroy (startingSection);

				Destroy (spawnedPatterns [0].gameObject);
				spawnedPatterns.Remove (spawnedPatterns [0]);
*/

			}
			//	lastPatternSpawned.SetTheme (LevelManager.Instance.AvailableThemes [themeNumber]);

			//	SpawnBuilding ();

		} else {
			Debug.LogError ("last spawned pattern empty");
		}


	}

	int transitions = 2;
	public List<Color> TransitionColors = new List<Color> ();

	public void SpawnTransition (float z)
	{
		//	spawnedSectionNb++;


		//currentSpawnDistanceZ += z;
		//	Debug.LogError ("SPAWNING TRANSITIONAT : " + z);
		currentSpawnDistanceZ += minRandomSpawnDistance * 1.5f;
		SuccessZ = currentSpawnDistanceZ;
		Vector3 randomPosition = new Vector3 (0f, 0f, SuccessZ);

        

        if (TransitionPatternGO != null) {
			var g = Instantiate (TransitionPatternGO, randomPosition, Quaternion.identity, TRANSITIONS_Parent);
			g.Init (transitions, LevelManager.Instance.AvailableThemes [(transitions + 1) % TransitionColors.Count].ThemeSwitcherColor);
			transitions++;
			g.gameObject.SetActive (true);
			spawnedTransitions.Add (g);

        } else {
			Debug.LogError ("No pattern to spawn");
		}

	
	}

	int lastSpawnIndex = -1;


	public void Spawn ()
	{
		if (levelOver)
			return;

		/*if (!GameManager.Instance.ENDLESS_MODE && spawnedSectionNb == LevelManager.Instance.CurrentLevelSectionTotal) {
			SpawnSuccess ();
			return;
		}*/


		if (spawnedSectionNb % 5 == 0) {
			//		SpawnDistanceMark ();
		}



		int randomPattern = Random.Range (0, patterns.Count);
		if (randomPattern == lastSpawnIndex) {
			Spawn ();
			return;
		}
        else {
			lastSpawnIndex = randomPattern;

		}
		if (spawnedSectionNb % BonusSectionEveryX == 0) {
			
				SpawnBonus ();

			return;
		}
		spawnedSectionNb++;

		float randomDistanceZ = Random.Range (minRandomSpawnDistance, maxRandomSpawnDistance);
		//float randomPositionX = Random.Range(maxSpawnPosX * -1f, maxSpawnPosX);

		currentSpawnDistanceZ += randomDistanceZ;

		Vector3 randomPosition = new Vector3 (0f, 0f, currentSpawnDistanceZ);
		//Debug.Log ("spawn : " + patterns [randomPattern].name + " at " + randomPosition.ToString ());
		if (patterns [randomPattern] != null) {
			lastPatternSpawned = Instantiate (patterns [randomPattern], randomPosition, Quaternion.identity, SECTIONS_Parent);
			lastPatternSpawned.gameObject.SetActive (true);
		} else {
			Debug.LogError ("No pattern to spawn");
		}

		lastPatternSpawned.spawnedInLevel = (int)currentSpawnDistanceZ / (int)DistanceBetweenTransitions;
		if (lastPatternSpawned != null) {
			//lastPatternSpawned.transform.position = randomPosition;

			currentSpawnDistanceZ += lastPatternSpawned.trigger.transform.localPosition.z;


			spawnedPatterns.Add (lastPatternSpawned);
			
			

		} else {
			Debug.LogError ("last spawned pattern empty");
		} 

	}

	public void SpawnBonus ()
	{
		if (levelOver)
			return;





		int randomPattern = Random.Range (0, BonusPatterns.Count);

		spawnedSectionNb++;

		float randomDistanceZ = Random.Range (minRandomSpawnDistance, maxRandomSpawnDistance);
		//float randomPositionX = Random.Range(maxSpawnPosX * -1f, maxSpawnPosX);

		currentSpawnDistanceZ += randomDistanceZ;

		Vector3 randomPosition = new Vector3 (0f, 0f, currentSpawnDistanceZ);
		//Debug.Log ("spawn : " + BonusPatterns [randomPattern].name + " at " + randomPosition.ToString ());
		if (BonusPatterns [randomPattern] != null) {
			lastPatternSpawned = Instantiate (BonusPatterns [randomPattern], randomPosition, Quaternion.identity, SECTIONS_Parent);
			lastPatternSpawned.gameObject.SetActive (true);
		} else {
			Debug.LogError ("No pattern to spawn");
		}
		lastPatternSpawned.spawnedInLevel = (int)currentSpawnDistanceZ / (int)DistanceBetweenTransitions;

		if (lastPatternSpawned != null) {
			//lastPatternSpawned.transform.position = randomPosition;

			currentSpawnDistanceZ += lastPatternSpawned.trigger.transform.localPosition.z;



			// Optiiiii
			spawnedPatterns.Add (lastPatternSpawned);

			if (spawnedPatterns.Count > 20) {
				/*	
				if (startingSection != null)
					Destroy (startingSection);

				Destroy (spawnedPatterns [0].gameObject);
				spawnedPatterns.Remove (spawnedPatterns [0]);
*/

			}
			//	lastPatternSpawned.SetTheme (LevelManager.Instance.AvailableThemes [themeNumber]);

			//	SpawnBuilding ();

		} else {
			Debug.LogError ("last spawned pattern empty");
		}




	}

	float lastGroundZ = 0f;



	void SpawnGround ()
	{
		//Debug.LogError ("spawn ground : ");
		var g = Instantiate (GroundModelGO, GROUND_Parent);
		g.SetActive (true);
		g.transform.position = new Vector3 (0f, 0f, lastGroundZ + GroundLength);
		var mr = g.GetComponent<MeshRenderer> ();
		//mr.material = LevelManager.Instance.AvailableThemes [themeIndex].GroundMaterial;
		grounds.Add (mr);
		lastGroundZ += GroundLength;//g.transform.position.z;
	}

	public void SetFire (bool state)
	{
		if (state) {
			foreach (var g in grounds) {
				if (g != null)
					g.sharedMaterial = LevelManager.Instance.FireTheme.GroundMaterial;
			}
		} else {
			foreach (var g in grounds) {
				if (g != null)
					g.sharedMaterial = LevelManager.Instance.CurrentTheme.GroundMaterial;
			}
		}
	}


	public void SpawnDistanceMark ()
	{
		if (lastDistanceMarkSpawned != null) {
			float newDistance = lastDistanceMarkSpawned.transform.position.z + 100f;
			lastDistanceMarkSpawned = Instantiate (distanceMark);

			lastDistanceMarkSpawned.transform.position = new Vector3 (0f, 0f, newDistance);


			int colorNumber = Mathf.FloorToInt (newDistance / distanceBetweenColorswitch);
			colorNumber = Mathf.Clamp (colorNumber, 0, distanceMarkColors.Count - 1);

			// est-ce que la distance mark est à un palier de multiplicateur
			if (newDistance % Player.Instance.distanceBetweenMultipliers == 0) {
				lastDistanceMarkSpawned.Initialize (true, false, (int)newDistance, Color.white);
			}
            // est-ce que la distance mark est à un palier de de nouvelle zone
            else if (newDistance % distanceBetweenColorswitch == 0) {
				lastDistanceMarkSpawned.Initialize (false, true, (int)newDistance, distanceMarkColors [colorNumber]);
			}
            // c'est un pallier tout ce qu'il y a de plus banal
            else {
				lastDistanceMarkSpawned.Initialize (false, false, (int)newDistance, distanceMarkColors [colorNumber]);
			}


		}
        
	}


	public void SwitchFogColor ()
	{
		int colorNumber = Mathf.FloorToInt (Player.Instance.transform.position.z / (distanceBetweenColorswitch - 50f));
		colorNumber = Mathf.Clamp (colorNumber, 0, distanceMarkColors.Count - 1);

		LeanTween.value (gameObject, UpdateFogColor, RenderSettings.fogColor, fogColors [colorNumber], 2f).setEaseInCubic ();
	}

	void UpdateFogColor (Color color)
	{
		RenderSettings.fogColor = color;
	}

	public void ApplySpawnedPatternsToCurve ()
	{
		//Debug.LogError ("APPLYING PATTENRS : ");


		foreach (var p in SpawnManager.Instance.spawnedPatterns) {
			if (p != null)
				p.ApplyPatternToGeneratedCurve ();
		}
		foreach (var t in spawnedTransitions) {
			if (t != null)
            {
                t.ApplyToGeneratedCurve();
                Player.Instance.levelSuccessDistance = t.transform.localPosition.z + 100f;
            }
				

		}

	}


	public void CleanPatternsInRelativeLevel (int lvl)
	{
		for (int i = 0; i < spawnedPatterns.Count - 1; i++) {
			if (spawnedPatterns [i] != null && spawnedPatterns [i].spawnedInLevel == lvl) {
				Destroy (spawnedPatterns [i].gameObject);
			}
		}

	}



}
