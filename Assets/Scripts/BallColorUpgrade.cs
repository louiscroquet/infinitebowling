﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallColorUpgrade : Upgrade {

    [Header("Ball colors")]
    public new List<Color> ballColors = new List<Color>();

    public Image upgradeBallIcon;
    public Text newBallComingSoonText;
    public Material ballMaterial;

    protected override void UpdateButtonState()
    {
        if (UpgradeNameDisplayer != null)
            UpgradeNameDisplayer.text = UpgradeName.ToString();
        if (UpradeCostDisplayer != null)
            UpradeCostDisplayer.text = CurrentUpgradeCost.ToString() + " $";
        if (CurrentLevelDisplayer != null)
            CurrentLevelDisplayer.text = (CurrentLevel + 1).ToString() + "<size=50>$</size>";

        UpdateBallColor();
    }

    public override void TryUpgrade()
    {
        // s'il y a encore du skin à acheter, c'est bon frère fais ta vie
        if (CurrentLevel < ballColors.Count - 1)
        {
            base.TryUpgrade();
        }
        else
        {
            base.FailedUpgradeAnimation();
        }   
    }

    public override void ValidateUpgrade()
    {
        base.ValidateUpgrade();
        upgradeBallIcon.color = ballColors[CurrentLevel];
        UpdateBallColor();
        
    }

    void UpdateBallColor()
    {
        ballMaterial.color = ballColors[Mathf.Min(CurrentLevel, ballColors.Count - 1)];

        if (CurrentLevel >= ballColors.Count - 1)
        {
            newBallComingSoonText.gameObject.SetActive(true);
            upgradeBallIcon.gameObject.SetActive(false);
        }
        else
        {
            upgradeBallIcon.gameObject.SetActive(true);
            //upgradeBallIcon.color = ballColors[CurrentLevel + 1];
        }
    }
}
