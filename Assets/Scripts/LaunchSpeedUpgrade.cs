﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchSpeedUpgrade : Upgrade
{

	protected override void UpdateButtonState ()
	{
		if (UpgradeNameDisplayer != null)
			UpgradeNameDisplayer.text = UpgradeName.ToString ();
		if (UpradeCostDisplayer != null)
			UpradeCostDisplayer.text = CurrentUpgradeCost.ToString () + " $";
		if (CurrentLevelDisplayer != null)
			CurrentLevelDisplayer.text = "LEVEL " + (CurrentLevel + 1).ToString ();//(Player.Instance.cruiseSpeed * CurrentPercentage / 100f).ToString () + "<size=50>mph</size>";
	}
}
