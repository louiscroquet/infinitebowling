﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct ComboRating {
    public int Length;
    public string Rating;
    public Color FeedbackColor;
}

public class GameManager : MonoBehaviour {
    public bool ENDLESS_MODE = true;

    public static GameManager Instance;

    public bool isGameOver;
    public bool isGameStarted = false;
    public float currentScore = 0;

    public Text pinLevelDetailText;
    public Text scoreText;

    private float bestScore;
    public Text newBestScoreText;

    public int currentMultiplier = 1;
    public Text multiplierText;

    public RectTransform gameOverPanel;

    public Slider LevelProgress;
    public TextMesh LevelName3DStartMark;
    public Text CurrentLevelNumber;
    public Text CurrentLevelNumberGameplay;
    public Text NextLevelNumber;

    public int PinsInLevel;

    public Text IntroLevelNumberText;

    [Header ("Endless Levels Settings")]
    public Upgrade LevelStartUpgrade;
    public int ScoreBonusPerLevel;
    public float SpeedIncreasePerLevel = 10f;
    public Upgrade CashUpgrade;

    public AudioSource AmbiantLoop;
    public float pitchIncreaseOnLevelUp = 0.05f;
    public float maxPitch = 1.25f;
    public Material FirePinExplodeMaterial;

    private void Awake () {
        // LeanTween.cancelAll ();

        LeanTween.init (2000, 2500);

        Instance = this;

        Application.targetFrameRate = 60;

        bestScore = PlayerPrefs.GetFloat ("Best Score", 0f);

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    // Use this for initialization
    void Start () {
        //CurrentEndlessLevel = LevelStartUpgrade.CurrentLevel;
        //ApplyEndlessLevel (CurrentEndlessLevel, true);
        //InitLevel (CurrentEndlessLevel, -1);

        LevelProgress.minValue = 0f;
        LevelProgress.value = 0f;
        //LevelProgress.maxValue = SpawnManager.Instance.DistanceBetweenTransitions;

        if (MonetizationManager.Instance != null) {
            if (!MonetizationManager.Instance.isRewardedAdAvailable) {
                //MonetizationManager.Instance.RequestRewardBasedVideo();
            }

            //MonetizationManager.Instance.ShowInterstitial();

        }

        //		InvokeRepeating ("UpdateScoreWithZ", 0f, 0.25f);
    }

    void updateProgress () {
        LevelProgress.value = Player.Instance.transform.position.z;
    }

    // Update is called once per frame
    void Update () {
#if UNITY_EDITOR
        if (Input.GetKeyDown (KeyCode.F))
            SetFire (!OnFire);
#endif
        updateProgress ();
    }

    public void Retry () {

        Application.LoadLevel (Application.loadedLevel);

        return;

        if (MonetizationManager.Instance.isInterstitialAdAvailable) {
            MonetizationManager.Instance.ShowInterstitial ();
        } else {
            Application.LoadLevel (Application.loadedLevel);
        }

    }

    private string progressDetailSuffix;

    public void InitLevel (int currentLevel, int maxPins) {

        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent (GameAnalyticsSDK.GAProgressionStatus.Start, "Level" + LevelManager.Instance.CurrentLevel.ToString ("000"));

        LevelName3DStartMark.text = "LEVEL " + (currentLevel + 1).ToString ();

        if (CurrentLevelNumber != null)
            CurrentLevelNumber.text = (currentLevel + 1).ToString ();
        if (CurrentLevelNumberGameplay != null)
            CurrentLevelNumberGameplay.text = "LEVEL " + (currentLevel + 1).ToString ();

        if (NextLevelNumber != null)
            NextLevelNumber.text = (currentLevel + 2).ToString ();
        if (IntroLevelNumberText != null)
            IntroLevelNumberText.text = "LEVEL " + (currentLevel + 1).ToString ();
        currentScore = 0f;
        currentMultiplier = 1;
        if (progressDetailSuffix != null)
            progressDetailSuffix = "/" + PinsInLevel.ToString ();
        if (pinLevelDetailText != null)
            pinLevelDetailText.text = currentScore.ToString ("000") + progressDetailSuffix;

        if (ENDLESS_MODE)
            SpawnManager.Instance.SpawnEndlessLevel ();
        else
            SpawnManager.Instance.InitSpawn (currentLevel);
        if (LevelProgress != null)
            LevelProgress.minValue = 0;
        PinsInLevel = maxPins;
        //LevelProgress.maxValue = SpawnManager.Instance.SuccessZ;

        //  DisplayIntroPanel();

        InvokeRepeating ("updateProgress", 0.1f, 0.1f);
    }

    public void StartLevel () {
        UIController.Instance.ShowPanel (0);
        UIController.Instance.HideCashOverlay ();
        if (LevelManager.Instance.CurrentLevel == 0) {
            ShowTutorial ();
        }
        Player.Instance.DelayedLaunchPhase ();
    }

    public GameObject TutorialAnimGO;

    public void ShowTutorial () {
        TutorialAnimGO.SetActive (true);
        Invoke ("HideTutorial", 3f);
    }

    public void HideTutorial () {
        TutorialAnimGO.SetActive (false);

    }

    public void DisplayIntroPanel () {
        UIController.Instance.ShowPanelInstantly (3);
    }

    public void HideShopRewardPanel () {
        UIController.Instance.HidePanelInstantly (6);
        UIController.Instance.HidePanelInstantly (7);
    }

    [Header ("Combo Settings")]

    public List<ComboRating> Ratings = new List<ComboRating> ();

    public int CurrentCombo = 0;
    public bool OnFire = false;
    public float ComboCooldown = 1.5f;
    private float lastComboEnd = -1f;
    private float lastFire = -1f;
    public Upgrade FireTimeUpgrade;
    public int FireOnCombo = 25;
    public float BaseFireDuration = 5f;

    public float FireDuration {
        get {
            if (FireTimeUpgrade != null)
                return BaseFireDuration * FireTimeUpgrade.CurrentPercentage / 100f;
            else
                return BaseFireDuration;

        }

    }

    public int ComboLostPerMiss;
    public Upgrade FireDurationUpgrade;
    int lastScore;

    public void Score (int scoretoAdd) {
        currentScore += scoretoAdd;
        if (lastScore != (int) currentScore) {
            //LeanTween.cancel (scoreText.gameObject);
            //LeanTween.scale (scoreText.gameObject, Vector3.one * 1.1f, 0.1f).setLoopPingPong (1);
            scoreText.text = "$" + ((int) currentScore).ToString ();
            lastScore = (int) currentScore;
        }

        if (currentScore > bestScore && newBestScoreText.gameObject.activeSelf == false) {
            DisplayNewBestScoreText ();
            SoundManager.Instance.Play (SoundManager.Instance.HighScore);
        }

        return;

        //	if (Time.time > lastComboEnd + ComboCooldown) {

        if (!Player.Instance.launchingPhase && !OnFire) {
            CurrentCombo++;

            //FeedbackManager.Instance.UpdateComboFeedback (CurrentCombo);

            if (CurrentCombo == FireOnCombo) {
                SetFire (true);
                CancelInvoke ("BreakCombo");
                //	float direDuration = FireDuration * FireDurationUpgrade.CurrentPercentage / 100f;
                Debug.Log ("fireDuration : " + FireDuration);
                Invoke ("BreakCombo", FireDuration);

                //BreakCombo ();
                //ComboRating rating = GetComboRating ();
                //FeedbackManager.Instance.HideComboFeedback (rating);
            } else {

            }
        }

        //	}

        if (currentScore == PinsInLevel) {
            pinLevelDetailText.GetComponent<ColorTintFade> ().FadeToNextColor ();
        }

        if (currentScore < PinsInLevel)
            pinLevelDetailText.text = currentScore.ToString ("000") + progressDetailSuffix;
        else
            pinLevelDetailText.text = "REACH THE END!";

        return;

    }

    ComboRating GetComboRating () {
        ComboRating res = Ratings[0];
        for (int i = 0; i < Ratings.Count; i++) {
            if (CurrentCombo > Ratings[i].Length) {
                res = Ratings[i];
            }
        }
        return res;
    }

    public int CurrentCircle = 0;

    public void TriggerCircle () {
        return;
        CurrentCircle++;
        int score = LevelManager.Instance.CurrentLevel + 1;
        GameManager.Instance.Score (score);
        FeedbackManager.Instance.Show3DTextFeedback ("+" + score.ToString (), Player.Instance.transform.position, Color.white);
        SoundManager.Instance.PlayCircleSound (CurrentCircle);
        CameraFeedback.Instance.ScreenshakeSoft ();
        FeedbackManager.Instance.PlayFlashInstant ();
    }

    public void DecreaseCombo () {
        if (OnFire)
            return;
        if (CurrentCombo >= 1) {
            CurrentCombo -= ComboLostPerMiss;

            CurrentCombo = Mathf.Max (CurrentCombo, 0);
            if (CurrentCombo == 0) {
                ComboRating rating = GetComboRating ();
                FeedbackManager.Instance.HideComboFeedback (rating, true);
            } else
                FeedbackManager.Instance.UpdateComboFeedback (CurrentCombo);
        }
    }

    public void BreakCombo () {
        BreakCombo (false);
    }

    public void BreakCombo (bool impact) {
        return;
        CurrentCombo = 0;
        if (!impact) {

            //if (CurrentCombo <= 0)
            //	return;
            //	if (Time.time < lastFire + FireDuration && !isGameOver)
            //		return;
        }

        //FeedbackManager.Instance.UpdateComboFeedback (CurrentCombo);

        //if (!impact && CurrentCombo > 1)
        //	return;

        CancelInvoke ("BreakCombo");

        SetFire (false);
        lastComboEnd = Time.time;
        ComboRating rating = GetComboRating ();
        if (impact) {
            FeedbackManager.Instance.Show3DTextFeedback ("Oops!", FeedbackManager.Instance.TextFeedback.transform.position, Color.red);
            //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.ImpactHeavy);
            VibrationManager.Instance.VibrateHeavy ();

        } else if (rating.Length > 0) {
            FeedbackManager.Instance.Show3DTextFeedback (rating.Rating, FeedbackManager.Instance.TextFeedback.transform.position, Color.white);
            //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.ImpactMedium);
            VibrationManager.Instance.VibrateMedium ();

        } else {
            //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.ImpactLight);
            VibrationManager.Instance.VibrateLight ();
        }
        CurrentCombo = 0;
        FeedbackManager.Instance.HideComboFeedback (rating, impact);
    }

    int lastZScore = -1;

    public void UpdateScoreWithZ () {

        currentScore = Player.Instance.transform.position.z / SpawnManager.Instance.minRandomSpawnDistance;
        if (lastZScore != (int) currentScore) {
            LeanTween.cancel (scoreText.gameObject);
            LeanTween.scale (scoreText.gameObject, Vector3.one * 1.1f, 0.1f).setLoopPingPong (1);
            scoreText.text = ((int) currentScore).ToString ();
            lastZScore = (int) currentScore;
        }
    }

    [Header ("Game Over screen")]
    public Text LevelPercentageReached;
    public GameObject GameOverAnimGO1;
    public GameObject GameOverAnimGO2;
    public GameObject GameOverAnimGO3;
    public GameObject HighScoreGO;

    public Text FinalResultText;
    public Text HighScoreText;

    public Slider GameOverLevelProgresSlider;
    public Text GameOverLevelProgresDetail;
    public Text GameOverLevelProgresCurrentLevel;
    public Text GameOverLevelProgresNextLevel;
    public AudioClip GameOverSound1;
    public AudioClip GameOverSound2;
    public AudioClip GameOverSound3;

    public Text GameOverPercentageReached;

    public void GameOver () {
        if (isGameOver)
            return;
        HideTutorial ();

        if (FinalResultText != null)
            FinalResultText.text = currentScore.ToString ();
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent (GameAnalyticsSDK.GAProgressionStatus.Fail, "Level" + LevelManager.Instance.CurrentLevel.ToString ("000"));

        //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.Failure);
        VibrationManager.Instance.VibrateFailure ();
        GameOverLevelProgresSlider.minValue = LevelProgress.minValue;
        GameOverLevelProgresSlider.maxValue = LevelProgress.maxValue;
        GameOverLevelProgresSlider.value = LevelProgress.value;

        OfflineReward.Instance.Save ();

        //#if !UNITY_EDITOR

        MonetizationManager.Instance.ShowInterstitial ();

        //#endif

        isGameOver = true;
        BreakCombo (false);
        //GameOverPercentageReached.text = GetScorePercentage ((int)currentScore).ToString ("0.00") + "<size=150>%</size>";
        GameOverPercentageReached.text = GetLevelPercentage (LevelManager.Instance.CurrentLevel).ToString ("0.00") + "<size=150>%</size>";
        UIController.Instance.ShowPanel (1);
        UIController.Instance.ShowCashOverlay ();
        float percentage = ((float) LevelProgress.value / LevelProgress.maxValue * 100f);
        percentage = Mathf.Clamp (percentage, 0f, 99f);
        LevelPercentageReached.text = percentage.ToString ("0") + "%";
        GameOverLevelProgresDetail.text = percentage.ToString ("0") + "%";
        GameOverLevelProgresCurrentLevel.text = (CurrentLevelNumber.text);
        GameOverLevelProgresNextLevel.text = (NextLevelNumber.text);

        GameOverAnimGO1.transform.localScale = new Vector3 (1f, 0f, 1f);
        GameOverAnimGO2.transform.localScale = new Vector3 (1f, 0f, 1f);
        GameOverAnimGO3.transform.localScale = Vector3.zero;

        LeanTween.scaleY (GameOverAnimGO1, 1f, 0.5f).setEase (LeanTweenType.easeOutBack);
        LeanTween.scaleY (GameOverAnimGO2, 1f, 0.5f).setDelay (0.75f).setEase (LeanTweenType.easeOutBack).setOnStart (playGameOverSound2);
        LeanTween.scale (GameOverAnimGO3, Vector3.one, 0.5f).setDelay (1.5f).setEase (LeanTweenType.easeOutBack).setOnStart (playGameOverSound3);

        PlayerPrefs.SetFloat ("Best Score", currentScore);
        /*
        if (currentScore > bestScore)
        {
            PlayerPrefs.SetFloat("Best Score", currentScore);
            HighScoreGO.SetActive(true);

            HighScoreText.gameObject.SetActive(true);
            HighScoreGO.transform.localScale = Vector3.zero;

            LeanTween.scale(HighScoreGO, Vector3.one, 0.5f).setDelay(0.6f).setEase(LeanTweenType.easeOutBounce).setOnComplete(c =>
            {
                SoundManager.Instance.Play(SoundManager.Instance.HighScoreGameOver);
                LeanTween.scale(HighScoreGO, Vector3.one * 1.05f, 0.5f).setLoopPingPong();
            });

        }
        else
        {
            HighScoreGO.SetActive(false);
            HighScoreText.gameObject.SetActive(false);

            //HighScoreText.text = "HIGH SCORE : " + bestScore.ToString ("0");
        }

        */
        //	Debug.LogError ("Game Over");

        LeanTween.moveY (gameOverPanel, 0f, 0.5f);

    }

    public Text ReviveScoreDisplayer;
    public float reviveCountdownTime;
    public Image reviveCountdownImage;

    public void TryToDisplayRevive () {

        SoundManager.Instance.Play (GameOverSound1);
        SoundManager.Instance.Play (SoundManager.Instance.GameOver);
        VibrationManager.Instance.VibrateFailure ();
        ReviveScoreDisplayer.text = currentScore.ToString () + "<size=200>$</size>";

        // si rewarded dispo dans le monetization manager ET si on a pas déjà revived on display le panel, sinon on passe direct au game over
        if (!Player.Instance.Revived && MonetizationManager.Instance != null && MonetizationManager.Instance.isRewardedAdAvailable) {
            UIController.Instance.ShowPanel (4);
            BeginReviveCountdown ();
        } else {
            GameOver ();
        }

    }
    int countdownId = -1;
    void BeginReviveCountdown () {
        countdownId = LeanTween.value (reviveCountdownImage.gameObject, UpdateCountdown, 1f, 0f, reviveCountdownTime).setOnComplete (GameOver).id;
    }

    void UpdateCountdown (float value) {
        reviveCountdownImage.fillAmount = value;
    }

    public void EndReviveCountdown () {
        LeanTween.cancel (countdownId);
        //MonetizationManager.Instance.gameOverRewardedButton.DisableButton();
    }

    public void Revive () {
        // Rendre invulnérable le joueur 3 secondes et le relancer si le joueur a bien maté la pub rewarded en entier
        EndReviveCountdown ();
        Player.Instance.Revive ();
    }

    public int UltimateScore;
    public int debugScore;

    [ContextMenu ("Test percentage")]
    public void debugScoreTest () {
        //	GetScorePercentage (debugScore);
        Debug.Log ("PErcentage : " + GetLevelPercentage (debugScore).ToString ());
    }

    [Header ("Scoring Percentage Faker")]
    public List<int> TierScores = new List<int> ();
    // 100 500 1000 5000 10k 25k 50k 100k 500k
    public List<int> TierLevels = new List<int> ();
    // 5   15   25	 50	 100 200  350  500 1000
    public List<float> TierPercentages = new List<float> ();
    // 90  83 	75	 50   40  20  10    5 0f

    public float GetScorePercentage (int s) {
        float maxPercentage = 100f;

        float minPercentage = 0f;

        int tier;
        int prev = 0;
        float t;
        for (int i = 0; i < TierScores.Count - 1; i++) {
            if (s < TierScores[i]) {

                if (i > 0) {
                    maxPercentage = TierPercentages[i - 1];
                    prev = TierScores[i - 1];
                }

                minPercentage = TierPercentages[i];
                tier = TierScores[i];
                t = (float) (s - prev) / (float) (tier - prev);
                float res = Mathf.Lerp (maxPercentage, minPercentage, t);
                //			Debug.Log ("res : " + res);
                return res;
            }
        }
        //	Debug.Log ("res : 0f");
        return 0f;
    }

    public float GetLevelPercentage (int s) {
        float maxPercentage = 100f;

        float minPercentage = 0f;

        int tier;
        int prev = 0;
        float t;
        for (int i = 0; i < TierLevels.Count - 1; i++) {
            if (s < TierLevels[i]) {

                if (i > 0) {
                    maxPercentage = TierPercentages[i - 1];
                    prev = TierLevels[i - 1];
                }

                minPercentage = TierPercentages[i];
                tier = TierLevels[i];
                t = (float) (s - prev) / (float) (tier - prev);
                float res = Mathf.Lerp (maxPercentage, minPercentage, t);
                //		Debug.Log ("res : " + res);
                return res;
            }
        }
        //Debug.Log ("res : 0f");
        return 0f;
    }

    void playGameOverSound2 () {
        SoundManager.Instance.Play (GameOverSound2);

    }

    void playGameOverSound3 () {
        SoundManager.Instance.Play (GameOverSound3);

    }

    [Header ("Level Success Screen")]
    public Text PlayerReachedSuccess;
    public Text LevelCompleteText;
    public GameObject SuccessAnimGO1;
    public GameObject SuccessAnimGO2;
    public GameObject SuccessAnimGO3;
    public GameObject SuccessAnimGO4;
    public Text collectLevelSuccessRewardText;
    public LevelSuccessRewardedButton levelSuccessButton;

    public void LevelSuccess () {
        if (isGameOver)
            return;
        if (SoundManager.Instance != null)
            SoundManager.Instance.Play (SoundManager.Instance.LevelSuccess);
        isGameOver = true;
        //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.Success);
        if (VibrationManager.Instance != null)
            VibrationManager.Instance.VibrateSuccess ();

        if (LevelCompleteText != null)
            LevelCompleteText.text = "LEVEL " + (LevelManager.Instance.CurrentLevel + 1).ToString () + "\nCOMPLETE!";

        MonetizationManager.Instance.ShowInterstitial ();

        LevelManager.Instance.LevelUp ();

        collectLevelSuccessRewardText.text = "$" + ValueConvertor.ConvertDoubleToString (currentScore);
        levelSuccessButton.tripleRewardText.text = "<size=150>$</size>" + ValueConvertor.ConvertDoubleToString (GameManager.Instance.currentScore * 3);

        if (SuccessAnimGO1 != null)
            SuccessAnimGO1.transform.localScale = Vector3.zero;
        if (SuccessAnimGO2 != null)
            SuccessAnimGO2.transform.localScale = Vector3.zero;
        if (SuccessAnimGO3 != null)
            SuccessAnimGO3.transform.localScale = Vector3.zero;
        if (SuccessAnimGO4 != null)
            SuccessAnimGO4.transform.localScale = Vector3.zero;
        if (SuccessAnimGO1 != null)
            LeanTween.scale (SuccessAnimGO1, Vector3.one, 0.5f).setEase (LeanTweenType.easeOutBack);
        //	LeanTween.scale (SuccessAnimGO2, Vector3.one, 0.5f).setDelay (0.25f).setEase (LeanTweenType.easeOutBack);
        if (SuccessAnimGO2 != null)
            LeanTween.scale (SuccessAnimGO2, Vector3.one, 0.5f).setDelay (1f).setEase (LeanTweenType.easeOutBack);
        if (SuccessAnimGO3 != null)
            LeanTween.scale (SuccessAnimGO3, Vector3.one, 0.5f).setDelay (1.25f).setEase (LeanTweenType.easeOutBack);
        if (SuccessAnimGO4 != null)
            LeanTween.scale (SuccessAnimGO4, Vector3.one, 0.5f).setDelay (1.5f).setEase (LeanTweenType.easeOutBack);

        UIController.Instance.ShowPanel (2);
        UIController.Instance.ShowCashOverlay ();

        var follow = Camera.main.transform.parent.GetComponent<FollowGameObject> ();
        if (follow != null)
            follow.StopFollow ();
        PlayerPrefs.SetFloat ("Best Score", currentScore);

        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent (GameAnalyticsSDK.GAProgressionStatus.Complete, "Level" + LevelManager.Instance.CurrentLevel.ToString ("000"));
    }

    private bool levelSuccessRewardCollected;
    public GameObject levelSuccessRewardPanel;
    public GameObject tripleLevelSuccessRewardButton;
    public void CollectLevelSuccessReward () {
        if (levelSuccessRewardCollected) {
            return;
        }

        levelSuccessRewardCollected = true;
        MoneyManager.Instance.AddCash (currentScore, false);
        LeanTween.scale (levelSuccessRewardPanel, Vector3.zero, 0.25f);

        if (MonetizationManager.Instance.isRewardedAdAvailable) {
            MonetizationManager.Instance.UpdateAllRewardedButtonStates ();
            // On display la proposition de lottery
            UIController.Instance.ShowPanel (10);
        } else {
            Invoke ("Retry", 0.5f);
        }
    }

    void DisplayNewBestScoreText () {
        newBestScoreText.gameObject.SetActive (true);
        LeanTween.scale (newBestScoreText.gameObject, Vector3.one * 0.8f, 0.5f).setLoopPingPong (0);
    }

    public void SetFire (bool state) {
        SetFire (state, false);
    }

    public void SetFire (bool state, bool skipTheme) {
        //	Debug.LogError ("SET FIRE : " + state);
        if (OnFire == state)
            return;

        OnFire = state;
        if (OnFire)
            lastFire = Time.time;
        CancelInvoke ("StopFire");
        if (OnFire) {
            Invoke ("StopFire", FireDuration);
            SoundManager.Instance.Play (SoundManager.Instance.StartFireSound);
        } else {
            SoundManager.Instance.Play (SoundManager.Instance.EndFireSound);

        }
        Player.Instance.SetFire (state);
        FeedbackManager.Instance.SetFire (state);
        //	CancelInvoke ("BreakCombo");
        //	Invoke ("BreakCombo", FireDuration);
        //	if (!skipTheme) {
        LevelManager.Instance.SetFire (state);
        SpawnManager.Instance.SetFire (state);
        //Camera.main.GetComponent<BloomPro> ().enabled = state;

        //	}
    }

    public void StopFire () {
        SetFire (false, false);
    }

    public int CurrentEndlessLevel = 0;
    int relativeLevel = 0;

    public void LevelUp () {
        //SpawnManager.Instance.CleanPatternsInRelativeLevel (relativeLevel - 1);
        //SpawnManager.Instance.SpawnEndlessSection ();
        relativeLevel++;
        Player.Instance.cruiseSpeed += SpeedIncreasePerLevel;
        Player.Instance.Freeze (1f);
        if (!Player.Instance.OnFire)
            Player.Instance.currentSpeed = Player.Instance.cruiseSpeed;
        Player.Instance.SpeedUpFOVFx ();
        CurrentEndlessLevel++;
        int bonus = CurrentEndlessLevel * 10 * (1 + GameManager.Instance.CashUpgrade.CurrentLevel);
        if (AmbiantLoop != null) {
            AmbiantLoop.pitch += pitchIncreaseOnLevelUp;
            AmbiantLoop.pitch = Mathf.Clamp (AmbiantLoop.pitch, 0f, maxPitch);
        }
        SoundManager.Instance.Play (SoundManager.Instance.LevelUpSound);
        //currentScore += CurrentEndlessLevel * ScoreBonusPerLevel;
        Score (bonus);
        Vector3 fp = Player.Instance.transform.position + Player.Instance.transform.up * 5f;

        //CustomizationManager.Instance.updateShopItems ();

        //fp.x = 0f;
        //fp.y += 2f;
        FeedbackManager.Instance.Show3DFireTextFeedback ("LEVEL UP\n+" + bonus.ToString () + "$", fp, Color.white, 0.5f);
        ApplyEndlessLevel (CurrentEndlessLevel, false);
        //Debug.LogError ("LEVEL UP : " + CurrentEndlessLevel.ToString ());
        //LevelManager.Instance.ApplyTheme (LevelManager.Instance.AvailableThemes [Mathf.Clamp (CurrentEndlessLevel, 0, LevelManager.Instance.AvailableThemes.Count)]);
    }

    public void UpgradeEndlessStartLevel () {
        SpawnManager.Instance.ClearEndlessLevel ();
        SpawnManager.Instance.SpawnEndlessLevel ();
        CurrentEndlessLevel = LevelStartUpgrade.CurrentLevel;
        ApplyEndlessLevel (CurrentEndlessLevel, true);
        FeedbackManager.Instance.PlayFlashInstant ();
        Application.LoadLevel (0);
    }

    public void ApplyEndlessLevel (int n, bool applyPreviousStages) {
        if (applyPreviousStages) {
            //	currentScore = 0;
            /*
			for (int i = 1; i < n; i++) {
				currentScore += ScoreBonusPerLevel;
			}
*/
            LevelName3DStartMark.text = "LEVEL " + (n + 1).ToString ();
        } else {
            LeanTween.cancel (CurrentLevelNumberGameplay.gameObject);
            LeanTween.scale (CurrentLevelNumberGameplay.gameObject, Vector3.one * 1.1f, 0.1f).setLoopPingPong (1);
        }
        //currentScore += ScoreBonusPerLevel;
        LevelManager.Instance.ApplyTheme (n);

        CurrentLevelNumberGameplay.text = "LEVEL " + (n + 1).ToString ();
        CurrentLevelNumber.text = (n + 1).ToString ();
        NextLevelNumber.text = (n + 2).ToString ();

    }

}