﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CI_PinExplosionParticles : CustomItem {
    public int ExplosionIndex;
    public static int PinExplosionIndex;
    public override void ApplyCustomItem()
    {
        PinExplosionIndex = ExplosionIndex;
    }

    public override void Action()
    {
        base.Action();

        Preview();

    }

    public override void Preview()
    {

        if (UnlockByLevel && !Owned)
        {
            return;
        }

        if (ShopMenu.Instance.lastPreviewExplosionParticles != null)
        {
            Destroy(ShopMenu.Instance.lastPreviewExplosionParticles.gameObject);
        }


        ParticleSystem p = (ParticleSystem)Instantiate(FeedbackManager.Instance.PinExplosionParticles[ExplosionIndex], ShopMenu.Instance.pinPreview.transform);
        p.transform.localScale = Vector3.one;
        p.loop = true;
        //var em = p.emission;
        //em.rateOverTime = 0f;
        var main = p.main;
        main.duration = 1.5f;
        //p.emission.SetBurst(0, new ParticleSystem.Burst(0.1f, 0, 250, 0, 2.5f));
        p.gameObject.SetActive(true);
        p.Play();

        ShopMenu.Instance.lastPreviewExplosionParticles = p;
    }

    public override void DiscountPreview()
    {
        ShopMenu.Instance.ballPreview.SetActive(false);
        ShopMenu.Instance.pinPreview.SetActive(true);
        Preview();
    }

}
