﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CI_PinModel : CustomItem {

    public int PinIndex;
    public static int PinModelIndex;
    public override void ApplyCustomItem()
    {
        PinModelIndex = PinIndex;
    }


    public override void Action()
    {
        base.Action();

        Preview();

    }

    public override void TryEquipItem()
    {
        base.TryEquipItem();

        ShopMenu.Instance.hasChangedPinModel = true;

    }

    public override void Preview()
    {

        if (UnlockByLevel && !Owned)
        {
            return;
        }

        GameObject pinPreview = Instantiate(SpawnManager.Instance.pinModels[PinIndex].gameObject, ShopMenu.Instance.pinPreview.transform);
        pinPreview.transform.localScale = Vector3.one;
        pinPreview.SetActive(true);

        if (ShopMenu.Instance.lastPinModelPreview != null)
        {
            Destroy(ShopMenu.Instance.lastPinModelPreview);
        }

        ShopMenu.Instance.lastPinModelPreview = pinPreview;
    }

    public override void DiscountPreview()
    {
        ShopMenu.Instance.ballPreview.SetActive(false);
        ShopMenu.Instance.pinPreview.SetActive(true);
        Preview();
    }



}
