﻿using System.Collections;
using System.Collections.Generic;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public static Player Instance;

    // CONTROLS VALUES
    [Header ("CONTROLS")]
    public bool canMove;
    public float maxPosX;
    public GameObject ball;
    public GameObject sphere;
    public Material defaultMaterial;

    public float currentSpeed;

    public bool increaseSpeedWithTime;
    public float increaseSpeedValue;
    public float increaseSpeedFrequency;
    public float maxCruiseSpeed;

    [Header ("INTRO")]
    public bool introPhase;
    public ParticleSystem introParticles;
    // LAUNCH VALUES
    [Header ("LAUNCH")]
    public bool launchingPhase;
    public float launchDuration;
    public float launchSpeed;
    public float launchSensitivity;

    // CRUISE VALUES
    [Header ("CRUISE")]
    public bool cruisePhase;
    public float cruiseSpeed;
    public float cruiseSensitivity;

    [Header ("FIRE")]
    public float FireSpeedBoost;
    public float LaunchFireDuration = 5f;
    // TIMER VALUE
    [Header ("TIMER")]
    public float gameDuration;
    public float endTime;
    public Slider timerSlider;
    public bool gameEnded;

    public float singlePinTimeBonusValue;
    public float strikeTimeBonusValue;

    public float gameDurationDecreasingFrequency;
    public float gameDurationDecreasingValue;
    public float minGameDuration;

    public float endDecreaseSpeedDuration;

    // SCORING
    [Header ("SCORING")]
    public int strikeScoreBonus;
    public int singlePinScoreBonus;
    private float distanceReached;
    private float pinScore;
    public Upgrade pinCashUpgrade;

    // multiplier
    public int currentMutliplier;
    public float distanceBetweenMultipliers;
    public Text currentMultiplierText;

    public float levelSuccessDistance = 4000f;

    // BOOSTER
    [Header ("BOOSTER")]
    private bool isBoosting;
    public float boostMultiplier;
    public float boostDuration;

    public float increaseBoosterValue;

    // SLOWER
    [Header ("SLOWER")]
    private bool isSlowed;
    public float slowerMultiplier;
    public float slowerDuration;

    public float decreaseSlowerValue;

    // FX
    [Header ("FX")]
    public ParticleSystem launchParticles;
    public float defaultFOVValue;
    public float speedUpFovValue;
    public float stoppedFOVValue;
    public TrailRenderer playerTrail;
    //public Gradient defaultTrailGradient;
    public Gradient gameEndedTrailGradient;

    public ParticleSystem slowParticles;
    public ParticleSystem speedParticles;
    public GameObject ballSpeedEffectsGO;
    public Color scoreBonusColor;
    public Color timeBonusColor;

    public MeshRenderer ballRenderer;
    public Material gameOverBallMaterial;
    public Material gameOverObstacleMaterial;
    public Material ballWhiteMaterial;
    public ParticleSystem GameOverParticles;

    private Rigidbody r;

    public GameObject impactSprite;
    public ParticleSystem impactParticles1;
    public ParticleSystem impactParticles2;

    private float lastDistanceZ;

    public float lastExplosionX = 1f;

    [Header ("Jump")]
    public Vector3 JumpForce;
    public Transform JumpExplosionCenter;
    public float ImpactExplosionForce;
    public float ImpactExplosionRadius;
    private bool explosionAvailable = false;
    public ParticleSystem ExplosionParticles;

    [Header ("Mayhem settings")]
    public float ExplosionForce;
    public float ExplosionRadius;
    public Vector3 ExplosionOffet;

    [Header ("FUEL SETTINGS")]
    public bool USE_FUEL = true;
    public float FuelReserveBase = 100f;

    public float FuelEndingDuration = 3f;
    public Slider FuelJauge;
    public Upgrade FuelReserveUpgrade;
    public Upgrade FuelConsumptionUpgrade;

    public float FuelConsumptionBase;

    public float CurrentFuelMax {
        get {
            return FuelReserveBase * FuelReserveUpgrade.CurrentPercentage / 100f;
        }
    }

    public float CurrentFuelConsumption {
        get {
            return FuelConsumptionBase / (FuelConsumptionUpgrade.CurrentPercentage / 100f);
        }
    }

    [Header ("Upgrade mode")]
    public bool USE_UPGRADES = true;
    public float BaseLaunchSpeed = 50f;
    public Upgrade LaunchSpeedUpgrade;

    public float UPGRADE_LaunchSpeed {
        get {
            return BaseLaunchSpeed * LaunchSpeedUpgrade.CurrentPercentage / 100f;
        }
    }

    public float BaseDragPerSecond = 5f;
    public Upgrade DragUpgrade;

    public float UPGRADE_DragPerSecond {
        get {
            return BaseDragPerSecond / DragUpgrade.CurrentPercentage * 100f;
        }
    }

    public float SpeedThresholdGameOver = 5f;
    [Header ("Speed FX")]
    public float FX_MaxSpeed = 100f;
    public ParticleSystem Ball_SpeedTrails;
    public float Ball_MinSpeed;
    public float Ball_MaxSpeed;
    public float Ball_MinTrailLength;
    public float Ball_MaxTrailLength;
    public float Ball_MinEmission;
    public float Ball_MaxEmission;
    public Color Ball_MinTrailColor;
    public Color Ball_MaxTrailColor;

    public ParticleSystem World_SpeedTrails;
    public float World_MinSpeed;
    public float World_MaxSpeed;

    public float World_MinTrailLength;
    public float World_MaxTrailLength;
    public float World_MinEmission;
    public float World_MaxEmission;
    public Color World_MinTrailColor;
    public Color World_MaxTrailColor;

    public AudioSource BallRollLoop;

    [Header ("TWISTY SETTINGS")]
    public bool TWISTY = false;
    public float TwistyZ = 0f;

    private float startingPosX;

    private void Awake () {
        Time.timeScale = 1f;

        r = GetComponent<Rigidbody> ();
        Instance = this;
        Application.targetFrameRate = 60;
        ballRenderer.sortingLayerName = "Default";
        introPhase = true;

        speedParticles.Stop ();

    }

    // Use this for initialization
    void Start () {

        //LaunchPhase ();
        TryUpdateSpeedWithABTest ();

        startingPosX = ball.transform.localPosition.x;

        InitializeFirecombo ();

    }

    public float LaneSize = 2.5f;
    float td;
    Vector3 tt;
    // Update is called once per frame
    void Update () {
#if UNITY_EDITOR

        if (Input.GetKeyDown (KeyCode.S)) {
            LevelSuccess ();
            r.isKinematic = true;
        }

        if (Input.GetKeyDown (KeyCode.W)) {
            TriggerFireFromGameplay ();
        }
        if (Input.GetKeyDown (KeyCode.Escape)) {
            Time.timeScale = 1f;
            Application.LoadLevel (0);
        }
#endif

        if (Input.GetKeyDown (KeyCode.C)) {
            //  CaptureManager.Instance.InitializeCaptureTheme();
        }

        if (TWISTY) {
            if (canMove)
                TwistyZ += currentSpeed * Time.deltaTime;
            /* WITH SINGLE BGCurve On Player
			transform.position = TwistyPathMath.CalcPositionByDistance (TwistyZ);
			var neo = new BGCurveBaseMath (TwistyPath, new BGCurveBaseMath.Config (BGCurveBaseMath.Fields.PositionAndTangent));
			transform.forward = neo.CalcByDistance (BGCurveBaseMath.Field.Tangent, TwistyZ, false);
*/
            transform.position = CurveManager.Instance.CalcPositionByDistance (TwistyZ);
            var tan = CurveManager.Instance.CalcTangentByDistance (TwistyZ);
            if (!tan.Equals (Vector3.zero))
                transform.forward = tan;

            if (canMove && !ShopMenu.Instance.ShopOpen) {
                JoystickControlsTWISTY ();

                if (Input.GetKeyDown (KeyCode.Space)) {
                    startingPosX *= -1f;
                    Vector3 newBallPos = ball.transform.localPosition;
                    newBallPos.x = startingPosX;
                    ball.transform.localPosition = newBallPos;
                }

                ballSpeedEffectsGO.transform.localEulerAngles = new Vector3 (0f, 180f + lastDelta * 45f, 0f);
                //if (Input.GetKeyDown (KeyCode.Space))
                //Jump ();
            }
            return;
        }
#if UNITY_EDITOR

        if (Input.GetKeyDown (KeyCode.LeftArrow) && transform.position.x > -LaneSize) {
            transform.position += Vector3.right * -LaneSize;
        } else if (Input.GetKeyDown (KeyCode.RightArrow) && transform.position.x < LaneSize) {
            transform.position += Vector3.right * LaneSize;
        } else {
            //transform.position.x = 0f;

        }

#endif
        if (introPhase) {

        }
        if (HardCeiling) {
            CapVerticalVelocity ();
        }
        if (canMove && !ShopMenu.Instance.ShopOpen) {
            JoystickControls ();

            ballSpeedEffectsGO.transform.localEulerAngles = new Vector3 (0f, 180f + lastDelta * 45f, 0f);
            if (Input.GetKeyDown (KeyCode.Space))
                Jump ();
        }

        if (cruisePhase == true && gameEnded == false) {
            UpdateTimer ();
            if (increaseSpeedWithTime) {

            }
        }

        distanceReached = transform.position.z;
        //GameManager.Instance.UpdateScore(distanceReached + pinScore);

        lastDistanceZ = transform.position.z;

        UpdateMultiplier ();

        //updateSpeedAndFeedback ();
    }

    void FixedUpdate () {
        if (canMove) {
            MoveForward ();

            if (transform.position.z >= levelSuccessDistance) {
                LevelSuccess ();
            }

        }

    }

    public void updateSpeedAndFeedback () {
        if (cruisePhase && !OnFire && USE_UPGRADES) {
            currentSpeed -= UPGRADE_DragPerSecond * Time.deltaTime;
            currentSpeed = Mathf.Max (currentSpeed, 0f);

        }
        //if (currentSpeed > Ball_MinSpeed)
        {
            float t = (currentSpeed - Ball_MinSpeed) / (Ball_MaxSpeed - Ball_MinSpeed);
            t = Mathf.Clamp01 (t);
            var emi = Ball_SpeedTrails.emission;
            emi.rateOverTimeMultiplier = t * Ball_MaxEmission;
            var col = Ball_SpeedTrails.colorOverLifetime;
            col.color = Color.Lerp (Ball_MinTrailColor, Ball_MaxTrailColor, t);
            //	Debug.Log ("tball : " + t);
        }

        //if (currentSpeed > World_MinSpeed) 
        {
            float t = (currentSpeed - World_MinSpeed) / (World_MaxSpeed - World_MinSpeed);
            t = Mathf.Clamp01 (t);
            var emi = World_SpeedTrails.emission;
            emi.rateOverTimeMultiplier = t * World_MaxEmission;
            var col = World_SpeedTrails.colorOverLifetime;
            col.color = Color.Lerp (World_MinTrailColor, World_MaxTrailColor, t);
            //	Debug.Log ("tworld : " + t);

        }

    }

    public bool HardCeiling = true;
    public float HardCeilingHeight = 4.5f;
    public float HardCeilingBuffer = 1f;

    public void CapVerticalVelocity () {
        float y = transform.position.y; // 4f
        float s = HardCeilingHeight - HardCeilingBuffer; // 4.5f-1f = 3.5f
        Vector3 velo = r.velocity;
        if (y > s && r.velocity.y > 0f) { // if y > 3.5f
            float ratio = (y - HardCeilingHeight) / HardCeilingBuffer; //0.5f
            ratio = Mathf.Clamp01 (ratio); //0.5f
            velo.y = r.velocity.y * ratio;
            r.velocity = velo;
        }

    }

    void IncreaseSpeed () {
        if (gameEnded == false && cruiseSpeed <= maxCruiseSpeed) {
            cruiseSpeed += increaseSpeedValue;
            currentSpeed = cruiseSpeed;
            Invoke ("IncreaseSpeed", increaseSpeedFrequency);
        }
    }

    public void Jump () {
        r.AddForce (JumpForce, ForceMode.Impulse);
        explosionAvailable = true;

    }

    public void ScorePin (float pinScoretoAdd) {
        pinScore += pinScoretoAdd;
    }

    void MoveForward () {
        //transform.Translate (Vector3.forward * currentSpeed * Time.deltaTime);
        r.velocity = new Vector3 (0f, r.velocity.y, currentSpeed);
        //r.AddForce (new Vector3 (0f, 0f, currentSpeed), ForceMode.Acceleration);
        sphere.transform.RotateAroundLocal (Vector3.right, currentSpeed / 150f);
        sphere.transform.localEulerAngles = new Vector3 (sphere.transform.localEulerAngles.x, sphere.transform.localEulerAngles.y, lastDelta * 90f);

    }

    public void Freeze () {

        Freeze (0.15f);
    }

    public void Freeze (float duration) {
        if (!canMove)
            return;
        //	Debug.Log ("freeze : ");
        canMove = false;

        //impactSprite.SetActive(true);

        //LeanTween.scale(impactSprite, Vector3.one * 0.15f, 0.05f).setLoopPingPong(1);

        r.velocity = new Vector3 (0f, 0f, 0f);
        CancelInvoke ("StopFreeze");
        Invoke ("StopFreeze", duration);
    }

    void StopFreeze () {

        //impactSprite.SetActive(false);

        //impactSprite.transform.localScale = Vector3.zero;

        canMove = true;
    }

    void UpdateFOV (float value) {
        Camera.main.fieldOfView = value;
        JoystickCam.fieldOfView = value;
    }

    void updateSensitivity (float value) {
        sensitivity = value;
    }

    void updateSpeed (float value) {
        currentSpeed = value;
    }

    void DecreaseGameDuration () {
        if (gameDuration > minGameDuration) {
            gameDuration -= gameDurationDecreasingValue;
            Invoke ("DecreaseGameDuration", gameDurationDecreasingFrequency);
        }
    }

    public void DelayedLaunchPhase () {
        CancelInvoke ("LaunchPhase");
        Invoke ("LaunchPhase", 0.15f);
    }

    void LaunchPhase () {
        GameManager.Instance.isGameStarted = true;
        //SpawnManager.Instance.ApplySpawnedPatternsToCurve ();
        if (UserConfig.SoundFX)
            BallRollLoop.Play ();
        //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.ImpactHeavy)
        FeedbackManager.Instance.UpdateComboFeedback (0);
        //	Debug.Log ("Launch Phase : ");
        //	if (LevelManager.Instance.CurrentLevel > 0)
        //		GameManager.Instance.SetFire (true, false);
        //GameManager.Instance.CurrentCombo = 25;
        //FeedbackManager.Instance.UpdateComboFeedback (25);
        SoundManager.Instance.Play (SoundManager.Instance.LaunchSound);
        VibrationManager.Instance.VibrateHeavy ();

        introPhase = true;
        introParticles.Stop ();
        speedParticles.Play ();
        if (USE_UPGRADES)
            currentSpeed = UPGRADE_LaunchSpeed;
        else
            currentSpeed = launchSpeed;
        launchingPhase = true;

        launchParticles.Play ();

        // Eloigner la caméra
        LeanTween.cancel (ltFov);
        ltFov = LeanTween.value (gameObject, UpdateFOV, defaultFOVValue, speedUpFovValue, 0.5f).setEaseInOutCubic ().id;
        //sensitivity = launchSensitivity;
        speedParticles.emissionRate *= 2f;

        Invoke ("CruisePhase", launchDuration);
    }

    private int ltfuel = -1;

    void CruisePhase () {
        //Debug.Log ("Cruise Phase : ");

        launchParticles.Stop ();
        launchingPhase = false;

        // Faire revenir la caméra
        LeanTween.cancel (ltFov);
        ltFov = LeanTween.value (gameObject, UpdateFOV, speedUpFovValue, defaultFOVValue, 1.5f).setEaseInOutCubic ().id;
        //LeanTween.value (gameObject, updateSensitivity, launchSensitivity, cruiseSensitivity, 1.5f).setEaseInOutCubic ();

        //Invoke ("DecreaseGameDuration", gameDurationDecreasingFrequency);

        // Display Timer
        //LeanTween.moveY (timerSlider.GetComponent<RectTransform> (), -200f, 0.5f).setEaseOutBounce ();

        //currentSpeed = cruiseSpeed;
        LeanTween.value (gameObject, updateSpeed, launchSpeed, cruiseSpeed, 0.5f).setEaseInOutCubic ();

        if (increaseSpeedWithTime && !USE_UPGRADES)
            Invoke ("IncreaseSpeed", increaseSpeedFrequency);

        speedParticles.emissionRate /= 2f;

        endTime = Time.time + gameDuration;
        cruisePhase = true;

        if (USE_FUEL) {
            FuelJauge.maxValue = CurrentFuelMax;
            FuelJauge.minValue = 0f;
            LeanTween.cancel (ltfuel);
            float duration = CurrentFuelMax / CurrentFuelConsumption;
            Debug.Log ("Fuel duration : " + duration);
            ltfuel = LeanTween.value (gameObject, updateFuelJauge, CurrentFuelMax, 0f, duration).setOnComplete (EmptyFuel).id;

        }
    }

    void updateFuelJauge (float f) {
        FuelJauge.value = f;
    }

    public void EmptyFuel () {
        LeanTween.cancel (ltFov);
        ltFov = LeanTween.value (gameObject, UpdateFOV, defaultFOVValue, stoppedFOVValue, FuelEndingDuration).setEaseInOutCubic ().id;
        //LeanTween.value (gameObject, updateSensitivity, launchSensitivity, cruiseSensitivity, 1.5f).setEaseInOutCubic ();
        LeanTween.value (gameObject, updateSpeed, cruiseSpeed, 0f, FuelEndingDuration).setEaseInOutCubic ().setOnComplete (GameOver);

    }

    public void AddTime (float t) {
        if (endTime < Time.time + gameDuration) {
            endTime += t;

        }

        if (endTime > Time.time + gameDuration) {
            endTime = Time.time + gameDuration;
        }

    }

    public void RefillTimer () {
        endTime = Time.time + gameDuration;
    }

    public void Boost () {
        if (cruisePhase == true && gameEnded == false) {
            if (increaseSpeedWithTime) {
                if (cruiseSpeed + increaseBoosterValue <= 40f) {
                    cruiseSpeed += increaseBoosterValue;
                    currentSpeed = cruiseSpeed;
                }

                //LeanTween.value(gameObject, UpdateFOV, defaultFOVValue, speedUpFovValue, 0.5f).setEaseInOutCubic().setLoopPingPong(1);
                speedParticles.emissionRate *= 2f;
            } else {
                if (isBoosting) {
                    CancelInvoke ("StopBoost");
                    Invoke ("StopBoost", boostDuration);
                } else {
                    isBoosting = true;
                    launchParticles.Play ();
                    currentSpeed = cruiseSpeed * boostMultiplier;
                    LeanTween.cancel (ltFov);
                    ltFov = LeanTween.value (gameObject, UpdateFOV, defaultFOVValue, speedUpFovValue, 0.5f).setEaseInOutCubic ().id;
                    speedParticles.emissionRate *= 2f;
                    Invoke ("StopBoost", boostDuration);
                }

            }

        }

    }

    void StopBoost () {
        isBoosting = false;
        currentSpeed = cruiseSpeed;
        launchParticles.Stop ();
        LeanTween.cancel (ltFov);
        ltFov = LeanTween.value (gameObject, UpdateFOV, speedUpFovValue, defaultFOVValue, 0.5f).setEaseInOutCubic ().id;
        speedParticles.emissionRate /= 2f;
    }

    public void Slow () {
        if (cruisePhase == true && gameEnded == false) {
            if (increaseSpeedWithTime) {
                if (cruiseSpeed > 20f) {
                    cruiseSpeed -= decreaseSlowerValue;
                    currentSpeed = cruiseSpeed;
                    slowParticles.Play ();
                }

            } else {
                if (isSlowed) {
                    CancelInvoke ("StopSlow");
                    Invoke ("StopSlow", slowerDuration);
                } else {
                    if (isBoosting == false) {
                        isSlowed = true;
                        currentSpeed = cruiseSpeed * slowerMultiplier;
                        //playerTrail.colorGradient = slowTrailGradient;
                        speedParticles.emissionRate /= 2f;
                        slowParticles.Play ();

                        Invoke ("StopSlow", slowerDuration);
                    }

                }
            }

        }

    }

    void StopSlow () {
        isSlowed = false;
        currentSpeed = cruiseSpeed;
        //playerTrail.colorGradient = defaultTrailGradient;
        speedParticles.emissionRate *= 2f;
        slowParticles.Stop ();
    }

    void UpdateTimer () {
        if (timerSlider == null)
            return;
        timerSlider.value = (endTime - Time.time) / gameDuration;
        if (((endTime - Time.time) / gameDuration) <= 0f) {
            if (gameEnded == false) {
                gameEnded = true;
                EndGame ();
            }

        }
    }

    void EndGame () {

        Vector3 ballPos = ball.transform.position;
        ballPos.x = 0f;
        //FeedbackManager.Instance.PlayFeedbackText (ballPos, 1.7f, 35f, 2.5f, 1f, 3f, "TIME'S UP", timeBonusColor);
        LeanTween.value (gameObject, DecreaseSpeed, currentSpeed, 0f, endDecreaseSpeedDuration).setOnComplete (GameOver);
        speedParticles.emissionRate = 0f;
        ballRenderer.material = gameOverBallMaterial;
        playerTrail.colorGradient = gameEndedTrailGradient;

    }

    void DecreaseSpeed (float value) {
        currentSpeed = value;
    }

    void GameOver () {
        if (UserConfig.SoundFX)
            BallRollLoop.Stop ();
        ExplosionParticles.Play ();
        FeedbackManager.Instance.PlayExplosionParticles (transform.position);
        CameraFeedback.Instance.ScreenshakeHard ();
        LeanTween.cancel (ball);
        LeanTween.scale (ball, Vector3.zero, 0.25f).setEase (LeanTweenType.easeInBack);
        if (GameOverParticles != null)
            GameOverParticles.Play ();
        CancelInvoke ();
        canMove = false;
        currentSpeed = 0f;
        GameManager.Instance.TryToDisplayRevive ();

        Debug.LogError ("Player Game Over");
    }

    public bool Revived = false;
    public void Revive () {
        Revived = true;
        canMove = true;
        r.isKinematic = false;
        ballRenderer.sharedMaterial = defaultMaterial;
        LeanTween.cancel (ball);
        LeanTween.scale (ball, Vector3.one * 1.75f, 0.25f).setEase (LeanTweenType.easeInBack);
        currentSpeed = cruiseSpeed;
        MakeInvincible ();
        UIController.Instance.ShowPanel (0, true);
        VibrationManager.Instance.VibrateHeavy ();
    }

    void MakeInvincible () {
        GameManager.Instance.SetFire (true);
        //Invoke("StopInvincible", 5f);
    }

    void StopInvincible () {
        //GameManager.Instance.SetFire(false);
    }
    public bool hasLevelSucceed;

    public void LevelSuccess () {

        if (hasLevelSucceed) {
            return;
        }

        hasLevelSucceed = true;
        GameManager.Instance.LevelSuccess ();
        CancelInvoke ();
        cam.transform.parent = null;
        FeedbackManager.Instance.DisableFeedbackTexts ();
    }

    public GameObject cam;

    private void OnTriggerEnter (Collider trig) {
        if (trig.gameObject.CompareTag ("ThemeSwitch")) {
            trig.transform.parent.GetComponent<ThemeSwitcher> ().ActivatePortal ();
            //GameManager.Instance.LevelUp ();
        }
        if (trig.gameObject.CompareTag ("Booster")) {
            Player.Instance.Boost ();
        }
        if (trig.gameObject.CompareTag ("Slower")) {
            Player.Instance.Slow ();
        }

        if (trig.gameObject.CompareTag ("BonusLetter")) {
            var pick = trig.GetComponent<BonusPickable> ();
            Freeze (0.5f);
            pick.CollectBonus ();
        }
        if (trig.gameObject.CompareTag ("Pattern")) {

            GameManager.Instance.TriggerCircle ();
            //SpawnManager.Instance.Spawn ();

            trig.gameObject.SetActive (false);
        }
        if (trig.gameObject.CompareTag ("Fog")) {
            SpawnManager.Instance.SwitchFogColor ();
            SpawnManager.Instance.SpawnDistanceMark ();
            FeedbackManager.Instance.PlayFogFireworks (transform.position);
        }
        if (trig.gameObject.CompareTag ("Multiplier")) {

            FeedbackManager.Instance.PlayMultiplierFireworks (transform.position);
        }
        if (trig.gameObject.CompareTag ("Powerup")) {
            AddTime (3f);
            // feedback to do

        }
        if (trig.gameObject.CompareTag ("Block Trigger")) {
            trig.transform.parent.GetComponentInParent<Pattern> ().InitializeBlocks ();

        }
        if (trig.gameObject.CompareTag ("Portal")) {
            LevelSuccess ();
        }

    }

    GameObject lastCollisionGo = null;

    private void OnCollisionEnter (Collision collision) {

        if (collision.gameObject.CompareTag ("Pin")) {

            collision.gameObject.GetComponent<Pin> ().Hit (transform.position);

            //ballRenderer.sharedMaterial = ballWhiteMaterial;

            impactSprite.transform.rotation = new Quaternion (0f, 0f, Random.Range (1f, 359f), 0f);
            impactSprite.SetActive (true);
            Invoke ("StopImpact", 0.01f);
            //Invoke("ResetMaterial", 0.1f);
            impactParticles1.Play ();

        } else if (collision.gameObject.CompareTag ("Ground")) {
            return;
            if (explosionAvailable) {
                //		Debug.LogError ("exploding : ");
                explosionAvailable = false;
                ExplosionParticles.Play ();
                foreach (var pin in FindObjectsOfType<Pin> ()) {

                    pin.m_rigidbody.AddExplosionForce (ImpactExplosionForce, JumpExplosionCenter.position, ImpactExplosionRadius);
                }
            }
        } else if (collision.gameObject.CompareTag ("Block") && lastCollisionGo != collision.gameObject) {
            //	Debug.LogError ("OBSTACLE COLLISION : ");
            lastCollisionGo = collision.gameObject;
            // super tolerant if (Mathf.Abs (collision.contacts [0].normal.z) > Mathf.Abs (collision.contacts [0].normal.x) && Mathf.Abs (collision.contacts [0].normal.z) > Mathf.Abs (collision.contacts [0].normal.y)) {
            // tolerant if (Mathf.Abs (collision.contacts [0].normal.z) > 0.1f) {//Mathf.Abs (collision.contacts [0].normal.x) && Mathf.Abs (collision.contacts [0].normal.z) > Mathf.Abs (collision.contacts [0].normal.y)) {
            //	if (Mathf.Abs (collision.contacts [0].normal.y) < 0.1f) {//Mathf.Abs (collision.contacts [0].normal.x) && Mathf.Abs (collision.contacts [0].normal.z) > Mathf.Abs (collision.contacts [0].normal.y)) {
            if (OnFire || Time.time < LastFireEnd + FireTolerance) {
                CameraFeedback.Instance.ScreenshakeHard ();
                //	SetFire (false);
                //Debug.Log ("Freeze for cllision with : " + collision.gameObject.name);
                Freeze ();
                //	GameManager.Instance.BreakCombo (true);
                FeedbackManager.Instance.PlayExplosionParticles (collision.contacts[0].point);

                Debug.LogError ("Hit block on fire");

                var g = collision.transform.GetComponentInParent<ObstacleBlockGroup> ();
                if (g != null) {
                    g.ExplodeGroup ();
                    SoundManager.Instance.Play (SoundManager.Instance.BreakObstacle);
                } else {
                    collision.transform.GetComponentInParent<ObstacleBlock> ().Explode ();
                    CameraFeedback.Instance.ScreenshakeHard ();
                    SoundManager.Instance.Play (SoundManager.Instance.BreakObstacle);

                }
            } else {
                r.isKinematic = true;

                GameOver ();
                collision.gameObject.GetComponent<MeshRenderer> ().sharedMaterial = gameOverObstacleMaterial;
                ballRenderer.sharedMaterial = gameOverBallMaterial;
                //	LeanTween.scale (ball, Vector3.zero, 0.2f);
                Debug.LogError ("Hit block and lose");

            }

            //}

        }

    }

    void StopImpact () {
        impactSprite.SetActive (false);
        impactParticles2.Play ();

    }

    void ResetMaterial () {
        ballRenderer.sharedMaterial = defaultMaterial;
    }

    public void ScorePin (Vector3 pos) {
        int value;

        if (OnFire) {
            value = ((pinCashUpgrade.CurrentLevel + 1) * 2);
            //   FeedbackManager.Instance.PlayFeedbackText(sphere.transform.position, 14f, -1.5f, 1f, 0f, 0.25f, "+$" + value.ToString() + "!", Color.red);
        } else {
            value = pinCashUpgrade.CurrentLevel + 1;
            // FeedbackManager.Instance.PlayFeedbackText(sphere.transform.position, 12f, -1.5f, 1f, 0f, 0.25f, "+$" + value.ToString(), Color.white);
            //AddFireCombo(1);
        }

        // GameManager.Instance.Score(value);

    }

    public void ScorePinGroup (Vector3 pos) {
        int value;

        if (OnFire) {
            value = ((pinCashUpgrade.CurrentLevel + 1) * 20);
            FeedbackManager.Instance.PlayFeedbackText (sphere.transform.position, 16f, -1.5f, 1f, 0f, 0.25f, "STRIKE!\n+$" + value.ToString () + "!", Color.red);
        } else {
            value = (pinCashUpgrade.CurrentLevel + 1) * 10;
            FeedbackManager.Instance.PlayFeedbackText (sphere.transform.position, 14f, -1.5f, 1f, 0f, 0.25f, "STRIKE!\n+$" + value.ToString (), Color.yellow);
            //AddFireCombo(10);
        }

        GameManager.Instance.Score (value);

    }

    void UpdateMultiplier () {
        int newMultiplier = (Mathf.FloorToInt (transform.position.z / distanceBetweenMultipliers) + 1);
        if (newMultiplier > currentMutliplier) {
            currentMutliplier = newMultiplier;

            // play feedback currentmultiplier
            currentMultiplierText.text = "X" + currentMutliplier.ToString ();
            LeanTween.scale (currentMultiplierText.gameObject, Vector3.one * 1.25f, 0.25f).setLoopPingPong (1);
        }

    }

    [Header ("FIRE COMBO")]
    const string Key_BoostCount = "P_BoostCount";
    public int pinToHitToFire;
    public int currentFireComboNumber;
    public Slider fireComboSlider;
    public float fireComboTime;
    public Image fireIcon;
    public GameObject TriggerFireButton;
    public int BoostCount;
    public Text currentFireBonusText;

    [Header ("Fire Start Animation")]
    public float FireAnimation_Duration = 1f;
    public LeanTweenType FireAnimation_EaseIn;
    public LeanTweenType FireAnimation_EaseOut;
    public ParticleSystem FireAnimation_IgniteParticles;
    public ParticleSystem FireAnimation_StartMovingParticles;

    public void StartFireAnimation () {
        canMove = false;
        CameraFeedback.Instance.ScreenRamble ();
        if (FireAnimation_IgniteParticles != null)
            FireAnimation_IgniteParticles.Play ();
        CancelInvoke ("startFireAnimationEnd");
        Invoke ("startFireAnimationEnd", FireAnimation_Duration);
    }
    void startFireAnimationEnd () {
        canMove = true;
        if (FireAnimation_StartMovingParticles != null)
            FireAnimation_StartMovingParticles.Play ();
    }

    void InitializeFirecombo () {
        currentFireComboNumber = PlayerPrefs.GetInt ("Fire Combo", currentFireComboNumber);
        BoostCount = PlayerPrefs.GetInt (Key_BoostCount, 3);

        currentFireBonusText.text = BoostCount.ToString ();

        fireComboSlider.maxValue = pinToHitToFire;
        fireComboSlider.value = currentFireComboNumber;

        // GameManager.Instance.SetFire(false);
        fireIcon.color = Color.white;
    }

    void ResetFireCombo () {
        currentFireComboNumber = 0;
        PlayerPrefs.SetInt ("Fire Combo", 0);

        InitializeFirecombo ();
    }

    public void AddFireCombo (int pinHitten) {
        if (OnFire) {
            return;
        }

        currentFireComboNumber += pinHitten;
        fireComboSlider.value = currentFireComboNumber;

        if (currentFireComboNumber >= pinToHitToFire) {
            FireComboComplete ();
        }

    }

    void FireComboComplete () {
        if (OnFire) {
            return;
        }
        AddBoost (1);
        ResetFireCombo ();
        /*
        GameManager.Instance.SetFire(true);
        fireIcon.color = Color.red;
        LeanTween.scale(fireIcon.gameObject, Vector3.one * 1.25f, 0.5f).setLoopPingPong(1);
        Invoke("ResetFireCombo", fireComboTime);
        */
    }

    public void TriggerFireFromGameplay () {
        if (!canMove)
            return;
        if (OnFire) {
            return;
        }

        if (BoostCount > 0) {
            OverrideFire ();
        } else {
            if (MonetizationManager.Instance != null && MonetizationManager.Instance.isRewardedAdAvailable) {
                OfferMoreBoosts ();
                //               MonetizationManager.Instance.ShowRewarded("firerewarded");
            } else {
                if (TriggerFireButton != null) {
                    if (!LeanTween.isTweening (TriggerFireButton))
                        LeanTween.moveLocalX (TriggerFireButton, TriggerFireButton.transform.localPosition.x - 15f, 0.05f).setLoopPingPong (3);
                }

                SoundManager.Instance.Play (SoundManager.Instance.FailedUpgrade);
            }
        }
    }

    public void OfferMoreBoosts () {
        canMove = false;
        UIController.Instance.ShowPanel (9, false);
    }
    public void RefuseBoosts () {
        if (GameManager.Instance.isGameStarted) {
            UIController.Instance.ShowPanel (0);
            CancelInvoke ("resumeMovementFromBoostOffer");
            Invoke ("resumeMovementFromBoostOffer", 0.25f);
        } else {
            UIController.Instance.HidePanel (9);

        }

    }
    void resumeMovementFromBoostOffer () {
        canMove = true;

    }

    void OverrideFire () {
        GameManager.Instance.SetFire (true);
        BoostCount--;
        currentFireBonusText.text = BoostCount.ToString ();
        PlayerPrefs.SetInt (Key_BoostCount, BoostCount);

    }

    public void AddBoost (int n) {
        BoostCount += n;
        currentFireBonusText.text = BoostCount.ToString ();
        PlayerPrefs.SetInt (Key_BoostCount, BoostCount);
        PlayerPrefs.Save ();
    }

    void ResetBonusFire () {
        currentFireComboNumber = 0;
        PlayerPrefs.SetInt (Key_BoostCount, 0);
        GameManager.Instance.SetFire (false);
        fireIcon.color = Color.white;

    }

    public GameObject NORMAL_PARTICLES;
    public GameObject FIRE_PARTICLES;
    public bool OnFire = false;
    public float FireTolerance;
    private float LastFireEnd;

    public void SetFire (bool state) {
        if (OnFire == state) {
            return;
        }

        OnFire = state;
        if (state) {

            StartFireAnimation ();
            if (USE_FUEL)
                LeanTween.pause (ltfuel);
            currentSpeed = cruiseSpeed + FireSpeedBoost;
            NORMAL_PARTICLES.SetActive (false);
            FIRE_PARTICLES.SetActive (true);
            SpeedUpFOVFx ();

            Debug.LogError ("Fire enabled");
        } else {
            if (USE_FUEL)
                LeanTween.resume (ltfuel);

            currentSpeed = cruiseSpeed;

            NORMAL_PARTICLES.SetActive (true);
            FIRE_PARTICLES.SetActive (false);
            LastFireEnd = Time.time;

            Debug.LogError ("Fire disabled");
        }
    }

    public void TryUpdateSpeedWithABTest () {
        return;

        if (GameAnalytics.IsCommandCenterReady ()) {
            //use command center

            string ab_cruiseSpeed = GameAnalytics.GetCommandCenterValueAsString ("ab_cruiseSpeed", cruiseSpeed.ToString ());
            string ab_fireBoost = GameAnalytics.GetCommandCenterValueAsString ("ab_cruiseSpeed", FireSpeedBoost.ToString ());
            float res_cruise = -1;
            res_cruise = float.Parse (ab_cruiseSpeed);
            if (res_cruise > 0f && !ab_cruiseSpeed.Equals ("control")) {
                Debug.Log ("Applying AB Cruise Speed : " + res_cruise.ToString ());
                cruiseSpeed = res_cruise;
                //	BaseRotationSpeed = res * (Mathf.Sign (BaseRotationSpeed));
            }
            float res_fireBoost = -1;
            res_fireBoost = float.Parse (ab_fireBoost);
            if (res_fireBoost >= 0f && !ab_fireBoost.Equals ("control")) {
                Debug.Log ("Applying AB Fire Boost Speed : " + res_fireBoost.ToString ());
                FireSpeedBoost = res_fireBoost;

                //	BaseRotationSpeed = res * (Mathf.Sign (BaseRotationSpeed));
            }
        } else {
            Debug.Log ("CommandCenter is not ready, defaulting");
        }
    }

    private int ltFov = -1;

    public void SpeedUpFOVFx () {
        LeanTween.cancel (ltFov);
        ltFov = LeanTween.value (gameObject, UpdateFOV, Camera.main.fieldOfView, defaultFOVValue + 15, 0.5f).setOnComplete (v => {
            ltFov = LeanTween.value (gameObject, UpdateFOV, Camera.main.fieldOfView, defaultFOVValue, 5f).setDelay (1f).id;

        }).id;
    }

    private void OnDestroy () {
        PlayerPrefs.SetInt ("Fire Combo", currentFireComboNumber);
    }

    #region JOYSTICK_CONTROl

    [Header ("JOYSTICK")]

    public float sensitivity;

    public Vector3 mouseBegin;
    public Vector2 mouseDelta;
    public Vector3 lastMousePos;

    public Vector3 mouseBeginWorld;
    public Vector3 mouseCurrentWorld;
    public float SlideMultiplier;
    public float RealDelta;
    public float targetX;
    float lastDelta;
    public Transform XTarget;
    public Camera JoystickCam;

    void JoystickControls () {

        if (Input.GetMouseButtonDown (0)) {
            //Debug.Log ("Input.mousePosition : " + Input.mousePosition);
            XTarget.position = ball.transform.position;

            mouseBegin = Input.mousePosition;
            lastMousePos = mouseBegin;
            mouseBeginWorld = JoystickCam.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 9.5f));

            mouseCurrentWorld = mouseBeginWorld;
        }

        if (Input.GetMouseButton (0)) {

            mouseCurrentWorld = JoystickCam.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 9.5f));

            RealDelta = mouseCurrentWorld.x - mouseBeginWorld.x;
            if (RealDelta == 0f) {
                //Debug.LogError ("overridez : ");
                //	XTarget.position = ball.transform.position;

            }

            if (Mathf.Sign (lastDelta) != Mathf.Sign (RealDelta) && (RealDelta) != (0f)) {

                XTarget.position = ball.transform.position;
                lastDelta = RealDelta;
            }
            // DIRECT MOVE
            //transform.position += Vector3.Lerp (Vector3.zero, Vector3.right * RealDelta * SlideMultiplier, 1f);

            // On bouge le target en permanence
            XTarget.position += Vector3.Lerp (Vector3.zero, Vector3.right * RealDelta * SlideMultiplier, 1f);

            // si target est en dehors de l'écran, on y va pas bordel de merde
            Vector3 neoPos = XTarget.position;
            neoPos.y = ball.transform.position.y;
            neoPos.z = ball.transform.position.z;
            float clampedNeoPosX = Mathf.Clamp (XTarget.position.x, (maxPosX - 1f) * -1f, maxPosX - 1f);
            neoPos.x = clampedNeoPosX;
            Vector3 res = Vector3.MoveTowards (ball.transform.position, neoPos, sensitivity * Time.deltaTime);
            res.y = ball.transform.position.y;
            res.z = ball.transform.position.z;

            //ball.transform.position = res;

            r.MovePosition (res);

            mouseBeginWorld = mouseCurrentWorld;
            if ((RealDelta) != (0f))
                lastDelta = RealDelta;

        }

        if (Input.GetMouseButtonUp (0))
            XTarget.position = ball.transform.position;

    }

    void JoystickControlsTWISTY () {

        if (Input.GetMouseButtonDown (0)) {
            //Debug.Log ("Input.mousePosition : " + Input.mousePosition);
            //XTarget.position = JoystickCam.ScreenToWorldPoint (Vector3.zero, 9.5f);

            mouseBegin = Input.mousePosition;
            lastMousePos = mouseBegin;
            mouseBeginWorld = JoystickCam.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 9.5f));

            mouseCurrentWorld = mouseBeginWorld;
        }

        if (Input.GetMouseButton (0)) {

            mouseCurrentWorld = JoystickCam.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 9.5f));

            RealDelta = mouseCurrentWorld.x - mouseBeginWorld.x;
            if (RealDelta == 0f) {
                //Debug.LogError ("overridez : ");
                //	XTarget.position = ball.transform.position;

            }

            if (Mathf.Sign (lastDelta) != Mathf.Sign (RealDelta) && (RealDelta) != (0f)) {

                //	XTarget.position = ball.transform.position;
                lastDelta = RealDelta;
            }
            // DIRECT MOVE
            //transform.position += Vector3.Lerp (Vector3.zero, Vector3.right * RealDelta * SlideMultiplier, 1f);

            // On bouge le target en permanence
            XTarget.position += Vector3.Lerp (Vector3.zero, Vector3.right * RealDelta * SlideMultiplier, 1f);

            // si target est en dehors de l'écran, on y va pas bordel de merde
            Vector3 neoPos = XTarget.position;
            neoPos.y = ball.transform.localPosition.y;
            neoPos.z = ball.transform.localPosition.z;
            float clampedNeoPosX = Mathf.Clamp (XTarget.position.x, (maxPosX - 1f) * -1f, maxPosX - 1f);
            neoPos.x = clampedNeoPosX;
            Vector3 res = Vector3.MoveTowards (ball.transform.localPosition, neoPos, sensitivity * Time.deltaTime);

            //res.y = ball.transform.localPosition.y;
            //res.z = ball.transform.localPosition.z;

            res.y = 0.85f;
            res.z = 0f;

            ball.transform.localPosition = res;
            //ball.transform.localPosition = new Vector3 (neoPos.x, 0.85f, 0f);

            mouseBeginWorld = mouseCurrentWorld;
            if ((RealDelta) != (0f))
                lastDelta = RealDelta;

        }

        //if (Input.GetMouseButtonUp (0))
        //	XTarget.position = ball.transform.position;

    }

    #endregion
}