﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopMenu : MonoBehaviour
{
    public bool ShopOpen = false;

    public static ShopMenu Instance;
    public GameObject m_SelfGameObject;

    [Header("SHOP")]
    public List<GameObject> ShopPanels = new List<GameObject>();
    public GameObject previewPanel;
    public List<Image> displayingButtons = new List<Image>();
    public Upgrade pinCashUpgrade;
    public Upgrade offlineUpgrade;

    [Header("PREVIEW")]
    public GameObject ballPreview;
    public Material ballPreviewMaterial;
    public ParticleSystem lastPreviewExplosionParticles;
    public Vector3 trailPreviewImagePosition;
    public GameObject lastTrailPreview;
    public GameObject lastPinModelPreview;
    public bool hasChangedPinModel;

    public GameObject pinPreview;

    [Header("DISCOUNT")]
    public int gameFrequencyForDiscountProposal;
    private bool discountAvailable;
    public CustomItem discountItem;
    private List<CustomItem> availableItemsForDiscount = new List<CustomItem>();
    public TextMeshProUGUI startingDiscountPrice;
    public TextMeshProUGUI discountedPrice;

    public GameObject discountRewardParent;
    public GameObject buyDiscountedItemButton;
    public GameObject discountContinueButton;

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        //HideShop();
    }

    public float CurrentReward
    {
        get
        {
            return (((pinCashUpgrade.CurrentUpgradeCost * 0.25f) + (offlineUpgrade.CurrentUpgradeCost * 0.25f)) * 2);
        }
    }


    public void ShowShop()
    {

        CustomizationManager.Instance.shopRedDot.SetActive(false);
        ShopOpen = true;
        SoundManager.Instance.Play(SoundManager.Instance.ClickSound);
        VibrationManager.Instance.VibrateLight();

        ShowPanel(0);

        //      Debug.Log("show shop");
        m_SelfGameObject.SetActive(true);
        ballPreview.SetActive(true);

        // preview le last item equipped
        UpdateDefaultPreview();

        //HideAllPanels();


        DisplayDiscountProposalPanel();

    }
    public void HideShop()
    {
        //    Debug.Log("hide shop");
        ShopOpen = false;

        m_SelfGameObject.SetActive(false);


        if (hasChangedPinModel)
        {
            SpawnManager.Instance.InitSpawn(LevelManager.Instance.CurrentLevel);
        }


        

    }

    public void UpdateDefaultPreview()
    {
        foreach (var g in CustomizationManager.Instance.AvailableGroups)
        {
            g.PreviewLastEquippedItem();
        }
    }

    public void ExitButton()
    {
        SoundManager.Instance.Play(SoundManager.Instance.BackSound);

        VibrationManager.Instance.VibrateLight();

        /*bool showingPanel = false;
        foreach (var p in ShopPanels)
        {
            if (p.activeSelf)
            {
                showingPanel = true;
            }
        }
        if (showingPanel)
            HideAllPanels();
        else*/
            HideShop();
    }

    public void ShowPanel(int i)
    {
        HideAllPanels();
        UpdateButtons();
        ShopPanels[i].SetActive(true);


        CustomizationManager.Instance.updateShopItems();
        

        Color dpc = displayingButtons[i].color;
        dpc.a = 1f;
        displayingButtons[i].color = dpc;

        

        

        if (i <= 1)
        {
            pinPreview.SetActive(false);
            ballPreview.SetActive(true);

            UpdateDefaultPreview();
        }
        else if(i == 2)
        {
            pinPreview.SetActive(true);
            ballPreview.SetActive(false);

            UpdateDefaultPreview();

            if (lastPreviewExplosionParticles != null)
            {
                lastPreviewExplosionParticles.Play();
            }


            if (lastTrailPreview != null)
            {
                Destroy(lastTrailPreview);
            }
            
        }
        else
        {
            pinPreview.SetActive(true);
            ballPreview.SetActive(false);
            
            UpdateDefaultPreview();

            if (lastTrailPreview != null)
            {
                Destroy(lastTrailPreview);
            }

            if (lastPreviewExplosionParticles != null)
            {
                lastPreviewExplosionParticles.Stop();
            }

        }

        
    }

    public void HideAllPanels()
    {
        foreach (var p in ShopPanels)
        {
            p.SetActive(false);
        }
        SoundManager.Instance.Play(SoundManager.Instance.BackSound);
    }

    void UpdateButtons()
    {
        foreach (var button in displayingButtons)
        {
            Color dpc = button.color;
            dpc.a = 0.5f;
            button.color = dpc;
        }
    }

    public void ChooseDiscountItem()
    {
        // choper tous les items du jeu et voir lesquels ne sont pas détenus par le joueur ET si joueur current money = leur prix / 2

        discountItem = null;
        discountAvailable = false;

        foreach (var g in CustomizationManager.Instance.AvailableGroups)
        {
            foreach (var i in g.AvailableItems)
            {
                //i.UpdateItemStateAndPrice();

                // si on a la thune du prix de l'item/2 et qu'on ne possède pas l'item on ajoute l'item à la lsite
                if (MoneyManager.Instance.CurrentMoney > i.PriceOrLevel/2 && !i.UnlockByLevel && !i.Owned)
                {
                    availableItemsForDiscount.Add(i);
                }
            }
        }

        // On choisit un item au hasard parmi ceux dispos s'il y en a un de dispo
        if(availableItemsForDiscount.Count > 0)
        {
            discountItem = availableItemsForDiscount[Random.Range(0, availableItemsForDiscount.Count)];
        }


    }

    private int currentGameNumber;
    // OnStart toutes les X games
    public void TryToMakeDiscountAvailable()
    {
        ChooseDiscountItem();

        currentGameNumber = PlayerPrefs.GetInt("DiscountGameNumber", 0);

        if(currentGameNumber > 0 && currentGameNumber >= gameFrequencyForDiscountProposal && discountItem != null)
        {
            discountAvailable = true;
            CustomizationManager.Instance.shopRedDot.SetActive(true);
            CustomizationManager.Instance.shopRedDotText.text = "SALE!";
        }
        else
        {
            discountAvailable = false;
            currentGameNumber++;
            PlayerPrefs.SetInt("DiscountGameNumber", currentGameNumber);
        }
        

        
    }

    // OnShop Opening
    public void DisplayDiscountProposalPanel()
    {

        if (!discountAvailable)
        {
            return;
        }

        if (discountItem == null || !MonetizationManager.Instance.isRewardedAdAvailable)
        {
            return;
        }

        UIController.Instance.ShowPanel(13, false);

        PlayerPrefs.SetInt("DiscountGameNumber", 0);

        buyDiscountedItemButton.SetActive(false);
        discountContinueButton.SetActive(true);

        discountRewardParent.SetActive(true);

        discountItem.DiscountPreview();


        // update les textes de prix
        startingDiscountPrice.text = "$" + ValueConvertor.ConvertDoubleToString(discountItem.PriceOrLevel);
        discountedPrice.text = "Only $" + ValueConvertor.ConvertDoubleToString(discountItem.PriceOrLevel/2);
    }

    public void BuyDiscountItem()
    {
        SoundManager.Instance.Play(SoundManager.Instance.BuyUpgrade);
        VibrationManager.Instance.VibrateSuccess();
        FeedbackManager.Instance.PlayRewardParticles();

        MoneyManager.Instance.RemoveCash(discountItem.PriceOrLevel/2);
        discountItem.Unlock();
        discountItem.TryEquipItem();

        // Hide proposal panel et show shop
        UIController.Instance.HidePanel(13);
        ShowPanel(0);
    }

    public void ValidateDiscount()
    {
        // afficher le bouton buy à la place de rewarded button et continue button

        buyDiscountedItemButton.SetActive(true);
        discountContinueButton.SetActive(false);

        discountRewardParent.SetActive(false);


    }

}
