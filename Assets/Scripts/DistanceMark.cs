﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceMark : MonoBehaviour {

    public TextMesh multiplierMarkText;
    public TextMesh distanceMarkText;
    public SpriteRenderer lineRenderer;

    public Color multiplierMarkColor;
    private Color distanceMarkColor;

    public GameObject multiplierTrigger;
    public GameObject fogTrigger;

    private void Awake()
    {
        distanceMarkText.GetComponent<Renderer>().sortingLayerName = "Score";
        multiplierMarkText.GetComponent<Renderer>().sortingLayerName = "Score";
        lineRenderer.GetComponent<Renderer>().sortingLayerName = "Score";
    }

    public void Initialize(bool isMultiplier, bool isNewZone, int distance, Color color)
    {
        // si c'est une distance mark multiplicateur
        if (isMultiplier)
        {
            multiplierMarkText.text = "X" + (Mathf.FloorToInt(transform.position.z / Player.Instance.distanceBetweenMultipliers) + 1).ToString();
            multiplierMarkText.color = multiplierMarkColor;
            lineRenderer.color = multiplierMarkColor;
            distanceMarkText.color = multiplierMarkColor;

            multiplierTrigger.SetActive(true);
        }
        else
        {
            /*if (isNewZone)
            {
                fogTrigger.SetActive(true);
                multiplierMarkText.text = "";
                multiplierMarkText.color = color;
            }*/

            distanceMarkColor = color;

            lineRenderer.color = color;
            distanceMarkText.text = distance.ToString() + "m";
            distanceMarkText.color = color;

        }
    }



}
