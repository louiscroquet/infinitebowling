﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThemeSwitcher : MonoBehaviour
{
	public TextMesh LevelNumberDisplayer;
	public MeshRenderer PortalMesh;
	public ParticleSystem EnterParticles;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.D)) {
			DestroyObstacles ();
		}	
	}

	bool applied = false;

	public void ApplyToGeneratedCurve ()
	{
		if (applied)
			return;
		applied = true;
		float distance = transform.position.z;

		transform.position = CurveManager.Instance.CalcPositionByDistance (distance);

		transform.forward = CurveManager.Instance.CalcTangentByDistance (distance);

        GameManager.Instance.LevelProgress.maxValue = transform.position.z + 80f;
        //transform.position += transform.right * offset;
        DestroyObstacles ();
        //	Debug.LogError ("Adapted : " + name);

        /*foreach (Pin p in GetComponentsInChildren<Pin>())
        {
            p.AdaptToCurve();

        }*/

    }

	private bool activated = false;

	public void ActivatePortal ()
	{
		if (activated)
			return;
		activated = true;
		CameraFeedback.Instance.ScreenshakeHard ();
		FeedbackManager.Instance.PlayFlashInstant ();
		EnterParticles.Play ();
		GameManager.Instance.LevelUp ();
        
		//Player.Instance.Freeze (0.25f);
	}

	public void Init (int transitionNumber, Color c)
	{
        LevelNumberDisplayer.text = (LevelManager.Instance.CurrentLevel + 2).ToString();
		c.a = 0.2f;

		//PortalMesh.material.color = c;
	}

	public LayerMask BlockLayer;

	public void DestroyObstacles ()
	{
		Collider[] hitColliders = Physics.OverlapSphere (transform.position, 10f, BlockLayer);
		int i = 0;
		while (i < hitColliders.Length) {
			var p = hitColliders [i].transform.GetComponentInParent<Pattern> ();
			if (p != null) {
				p.transform.gameObject.SetActive (false);
			}

			/*
			var block = hitColliders [i].transform.parent.GetComponent<ObstacleBlock> ();
			if (block != null) {
				block.gameObject.SetActive (false);
			} else {
				var blockg = hitColliders [i].transform.parent.GetComponent<ObstacleBlockGroup> ();
				if (blockg != null)
					blockg.gameObject.SetActive (false);
			}
			*/
			i++;
		}
	}
}
