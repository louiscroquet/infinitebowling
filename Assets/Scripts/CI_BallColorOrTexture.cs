﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CI_BallColorOrTexture : CustomItem {

    public Color BallColor;
    public Texture BallTexture;
    public override void ApplyCustomItem()
    {
        if (BallTexture != null)
        {
            Player.Instance.ballRenderer.material.mainTexture = BallTexture;
            Player.Instance.ballRenderer.material.color = Color.white;
        }
            
        else
        {
            Player.Instance.ballRenderer.material.color = BallColor;
            Player.Instance.ballRenderer.material.mainTexture = null;

        }
    }

    public override void Action()
    {
        base.Action();

        Preview();

    }

    public override void Preview()
    {
        if (UnlockByLevel && !Owned)
        {
            ShopMenu.Instance.ballPreviewMaterial.color = Color.black;
            ShopMenu.Instance.ballPreviewMaterial.mainTexture = null;
            return;
        }

        if (BallTexture != null)
        {
            ShopMenu.Instance.ballPreviewMaterial.mainTexture = BallTexture;
            ShopMenu.Instance.ballPreviewMaterial.color = Color.white;
        }

        else
        {
            ShopMenu.Instance.ballPreviewMaterial.color = BallColor;
            ShopMenu.Instance.ballPreviewMaterial.mainTexture = null;

        }
    }

    public override void DiscountPreview()
    {
        ShopMenu.Instance.ballPreview.SetActive(true);
        ShopMenu.Instance.pinPreview.SetActive(false);
        Preview();
    }

}
