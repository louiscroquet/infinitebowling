﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class Pattern : MonoBehaviour
{
	
	public int spawnedInLevel = -1;
	public int MinimumLevel = 0;

	public GameObject trigger;

	private bool hasBeenTouched;

	public List<Pin> pins = new List<Pin> ();

	public GameObject timerPowerup;

	public List<GameObject> blocks = new List<GameObject> ();
	// Use this for initialization
	void Start ()
	{
		InitializeBlocks ();

		
		float randomScalePercentage = Random.Range (0f, 100f);
		if (randomScalePercentage < 50f) {
			//	Debug.Log ("removed mirror : ");
			//transform.localScale = new Vector3 (-1f, 1f, 1f);
		}

		return;
        
		/*foreach (GameObject block in blocks)
        {
            Vector3 newScale = block.transform.localScale;
            newScale.x *= 1.2f;
            block.transform.localScale = newScale;
        }*/

		/*foreach(Rigidbody rb in GetComponentsInChildren<Rigidbody>())
        {
            childrenRigidbodys.Add(rb);
        }*/
		if (LevelManager.Instance.CurrentLevel == 0) {
			foreach (GameObject block in blocks) {
				block.SetActive (false);
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void InitializeBlocks ()
	{
		
		return;
		//if (Player.Instance.cruisePhase) {
		foreach (GameObject block in blocks) {
			/* Vector3 newScale = block.transform.localScale;
                newScale.x *= 1.2f;
                block.transform.localScale = newScale;*/
			block.SetActive (false);
			//LeanTween.moveY (block, Random.Range (-1f, 0f), 0.5f);
			LeanTween.moveY (block, Random.Range (-1f, 0f), 0.5f);
		}
		//}



        
	}

	public void SetTheme (LevelTheme t)
	{
		pins.Clear ();
		pins = new List<Pin> (GetComponentsInChildren<Pin> ());
		foreach (var p in pins) {
			if (p.PinBodyRenderer != null)
				p.PinBodyRenderer.material.mainTexture = t.PinColor;
			if (p.PinBorderRenderer != null)
				p.PinBorderRenderer.material.mainTexture = t.PinStripeColor;
		}
		//Debug.Log ("done : ");
	}

	public void ApplyPatternToGeneratedCurve ()
	{
		AdaptTriggerToCurve ();
		SpawnPinsOnCurve ();
		foreach (Pin p in GetComponentsInChildren<Pin>()) {
			p.AdaptToCurve ();

		}

		foreach (BonusPickable p in GetComponentsInChildren<BonusPickable>()) {
			p.AdaptToCurve ();

		}

		foreach (ObstacleBlockGroup o in GetComponentsInChildren<ObstacleBlockGroup>()) {
			o.AdaptGroup ();
			/*
			float distance = o.transform.position.z;

			float offset = o.transform.position.x;
			o.transform.position = CurveManager.Instance.CalcPositionByDistance (distance);
			
			o.transform.forward = CurveManager.Instance.CalcTangentByDistance (distance);
			o.transform.position += o.transform.right * offset;
			Debug.LogError ("Adapted : " + name);
*/

		}
		foreach (ObstacleBlock o in GetComponentsInChildren<ObstacleBlock>()) {
			o.AdaptToCurve ();
			/*
			float distance = o.transform.position.z;

			float offset = o.transform.position.x;
			o.transform.position = CurveManager.Instance.CalcPositionByDistance (distance);

			o.transform.forward = CurveManager.Instance.CalcTangentByDistance (distance);
			o.transform.position += o.transform.right * offset;
			Debug.LogError ("Adapted : " + name);

*/
			
		}
	}

	public void ApplyPatternToCurve (BGCurve curve)
	{

		var neo = new BGCurveBaseMath (curve, new BGCurveBaseMath.Config (BGCurveBaseMath.Fields.PositionAndTangent));

		foreach (Pin p in GetComponentsInChildren<Pin>()) {
			float distance = p.transform.position.z;

			p.GetComponent<Rigidbody> ().useGravity = false;

			if (distance < neo.GetDistance ()) {
				float offset = p.transform.position.x;

				p.transform.position = neo.CalcPositionByDistance (distance);

				p.transform.forward = neo.CalcByDistance (BGCurveBaseMath.Field.Tangent, distance, false);
				p.transform.position += p.transform.right * offset;

				//Debug.LogError ("Adapted : " + name);

			} else {
				Debug.LogError ("too far : " + distance + " out of " + neo.GetDistance ());
			}
		}
		foreach (ObstacleBlock o in GetComponentsInChildren<ObstacleBlock>()) {
			float distance = o.transform.position.z;

			if (distance < neo.GetDistance ()) {
				float offset = o.transform.position.x;
				o.transform.position = neo.CalcPositionByDistance (distance);

				o.transform.forward = neo.CalcByDistance (BGCurveBaseMath.Field.Tangent, distance, false);
				o.transform.position += o.transform.right * offset;
				//	Debug.LogError ("Adapted : " + name);

			} else {
				Debug.LogError ("too far : " + distance + " out of " + neo.GetDistance ());
			}
		}
		/*
		 *
		float distance = transform.position.z;
		 
		if (distance < neo.GetDistance ()) {
			transform.position = neo.CalcPositionByDistance (distance);

			transform.forward = neo.CalcByDistance (BGCurveBaseMath.Field.Tangent, distance, false);
			Debug.LogError ("Adapted : " + name);
		
		} else {
			Debug.LogError ("too far : " + distance + " out of " + neo.GetDistance ());
		}
		*/
	}

	void AdaptTriggerToCurve ()
	{
		
		float distance = trigger.transform.position.z;


		float offset = trigger.transform.position.x;

		trigger.transform.position = CurveManager.Instance.CalcPositionByDistance (distance);

		trigger.transform.forward = CurveManager.Instance.CalcTangentByDistance (distance);
		trigger.transform.position += trigger.transform.right * offset;

	
	}


	public BGCurve PinCurve;
	public static Vector3 lastCurveEnd = Vector3.zero;
	public static float lastCurveEndAngle = 0f;
	public static bool spawnState = false;
	public static int sameState = 0;

	public void SpawnPinsOnCurve ()
	{
		if (PinCurve == null) {
			//Debug.Log ("Pattern has no curve : " + name);
			return;
		}
	
		Debug.Log ("Points : " + PinCurve.PointsCount.ToString ());
		if (lastCurveEnd != Vector3.zero) {
			PinCurve.Points [0].PositionWorld = lastCurveEnd + Vector3.forward * 5f;
			PinCurve.Points [1].PointTransform.localEulerAngles = new Vector3 (0f, lastCurveEndAngle, 0f);

		} else {
			Vector3 localStartPos = PinCurve.Points [0].PositionLocal;
			localStartPos.x = Random.Range (-5f, 5f);
			PinCurve.Points [0].PositionLocal = localStartPos;
		}
		PinCurve.Points [1].PointTransform.localEulerAngles = new Vector3 (0f, Random.Range (-15f, 15f), 0f);

	


		lastCurveEnd = PinCurve.Points [1].PositionWorld;
		lastCurveEndAngle = PinCurve.Points [1].PointTransform.localEulerAngles.y;

		var neo = new BGCurveBaseMath (PinCurve, new BGCurveBaseMath.Config (BGCurveBaseMath.Fields.PositionAndTangent));

		for (int i = 0; i < SpawnManager.Instance.PinsPerCurve; i++) {
			if (Random.Range (0, 100) <= 25 && sameState > 2) {
				
				spawnState = !spawnState;
				sameState = 0;
			} else {
				sameState++;
			}
			if (spawnState) {
				if (Random.Range (0, 100) < SpawnManager.Instance.PinGroupPercentage && i >= 2 && i <= SpawnManager.Instance.PinsPerCurve - 2) {
					var pin = Instantiate (SpawnManager.Instance.PinGroupGO, transform);
					pin.gameObject.SetActive (true);
					pin.transform.position = neo.CalcPositionByDistance (i * neo.GetDistance () / (float)SpawnManager.Instance.PinsPerCurve, false);
					pin.transform.forward = neo.CalcTangentByDistance (i * neo.GetDistance () / (float)SpawnManager.Instance.PinsPerCurve, false);
					spawnState = false;
					sameState = 0;
				} else {
					var pin = Instantiate (SpawnManager.Instance.PinModelGO, transform);
					pin.gameObject.SetActive (true);
					pin.transform.position = neo.CalcPositionByDistance (i * neo.GetDistance () / (float)SpawnManager.Instance.PinsPerCurve, false);

				}
					
			}

		}
	}
}
