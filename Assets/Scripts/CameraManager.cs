﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance;
    public List<Camera> ActiveCameras = new List<Camera>();
    public Camera MainCamera;
    public BlurOptimized MainCameraBlur;
    public BlurOptimized UICameraBlur;
    public Camera UICamera;


    private List<LayerMask> cullingMasks = new List<LayerMask>();
    private void Awake()
    {
        Instance = this;
        MainCamera = Camera.main;
        for (int i = 0; i < ActiveCameras.Count; i++)
        {
            cullingMasks.Add(ActiveCameras[i].cullingMask);
        }
        //SetCameraEnabled(0, false);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetMainCameraBlur(bool state)
    {
        if (MainCameraBlur != null)
            MainCameraBlur.enabled = state;

        
    }
    public void SetUICameraBlur(bool state)
    {
        if (UICameraBlur != null)
            UICameraBlur.enabled = state;

        if (UICamera != null)
            UICamera.enabled = !state;

        SetMainCameraBlur(state);
    }

    public void SetCamerasEnabled(bool state)
    {
        for (int i = 0; i < ActiveCameras.Count; i++)
        {
            if( state)
                ActiveCameras[i].cullingMask = cullingMasks[i];

            else
                ActiveCameras[i].cullingMask = LayerMask.GetMask();
        }
    }

    public void SetCameraEnabled(int camIndex,bool state)
    {
       
        {
            if (state)
                ActiveCameras[camIndex].cullingMask = cullingMasks[camIndex];

            else
                ActiveCameras[camIndex].cullingMask = LayerMask.GetMask();
        }
    }


}
