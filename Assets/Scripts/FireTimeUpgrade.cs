﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTimeUpgrade : Upgrade
{

	protected override void UpdateButtonState ()
	{
		//	Debug.Log ("update fire time : " + GameManager.Instance.FireDuration);
		if (UpgradeNameDisplayer != null)
			UpgradeNameDisplayer.text = UpgradeName.ToString ();
		if (UpradeCostDisplayer != null)
			UpradeCostDisplayer.text = CurrentUpgradeCost.ToString () + " $";
		if (CurrentLevelDisplayer != null)
			CurrentLevelDisplayer.text = (GameManager.Instance.BaseFireDuration * CurrentPercentage / 100f).ToString () + "<size=50>sec</size>";
	}
}
