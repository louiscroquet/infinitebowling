﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PinGroupType
{
	STRIKE,
	PYRAMID,
	TOWER
}

public class PinGroup : MonoBehaviour
{
	public PinGroupType GroupType = PinGroupType.STRIKE;

	private bool striked;
	public List<Pin> PinsInGroup = new List<Pin> ();

	void Awake ()
	{
	}
	// Use this for initialization
	void Start ()
	{
		InitPinsInGroup ();

		striked = false;
	}

	void Update ()
	{
		#if UNITY_EDITOR
		if (Input.GetKeyDown (KeyCode.S)) {
			Strike ();
		}
		#endif
	}

	[ContextMenu ("Init Pins in children")]
	public void InitPinsInGroup ()
	{
		PinsInGroup.Clear ();
		PinsInGroup.AddRange (GetComponentsInChildren<Pin> ());
	
		foreach (Pin p in PinsInGroup) {
			p.ParentGroup = this;
		}
	}

	public void Strike ()
	{
		if (Player.Instance.launchingPhase)
			return;
		int value;
			
		if (striked == false) {
			striked = true;
			//CameraFeedback.Instance.ScreenshakeSoft ();
			CameraFeedback.Instance.ScreenshakeHard ();
		
			SoundManager.Instance.Play (SoundManager.Instance.PinFireSound);
			if (!Player.Instance.launchingPhase) {
				switch (GroupType) {
				case PinGroupType.STRIKE:
					if (SoundManager.Instance != null) {
						SoundManager.Instance.Play (SoundManager.Instance.StrikeSound);
						SoundManager.Instance.PlayPinSound ();
                        SoundManager.Instance.PlayPinSound();
                        SoundManager.Instance.PlayPinSound();

                        }

                        foreach (Pin p in PinsInGroup)
                        {
                            p.ScorePin(GameManager.Instance.OnFire);
                            p.EjectPin(Player.Instance.sphere.transform.position);
                        }
                        //FeedbackManager.Instance.ShowStrikeFeedback (transform.position);
                        //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.ImpactMedium);
                        VibrationManager.Instance.VibrateHeavy();

					break;
				case PinGroupType.PYRAMID:
					if (SoundManager.Instance != null)
						SoundManager.Instance.Play (SoundManager.Instance.MayhemSound);

					//iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.ImpactMedium);
                        VibrationManager.Instance.VibrateMedium();

					FeedbackManager.Instance.ShowMayhemFeedback (Player.Instance.ball.transform.position);
					foreach (Pin p in PinsInGroup) {
						p.m_rigidbody.AddExplosionForce (Player.Instance.ExplosionForce, Player.Instance.transform.position + Player.Instance.ExplosionOffet, Player.Instance.ExplosionRadius);
                        }
					break;
				case PinGroupType.TOWER:
					break;
				default:
					break;
				}
			}

			foreach (var pin in PinsInGroup) {
				pin.hasBeenHit = true;
                pin.PinShadow.SetActive(false);
			}

            
			// Hit strike in cruise phase
			if (Player.Instance.cruisePhase == true) {
				//Player.Instance.AddTime(Player.Instance.strikeTimeBonusValue);
				if (Player.Instance.gameEnded == false) {
					//FeedbackManager.Instance.PlayBonusTimeFeedback(Player.Instance.strikeTimeBonusValue);
				}

				//FeedbackManager.Instance.PlayFeedbackText (transform.position, 1.2f, Player.Instance.currentSpeed / 2f, 0.5f, 2f, 1f, "STRIKE!\n+" + (Player.Instance.strikeScoreBonus * Player.Instance.currentMutliplier).ToString (), Player.Instance.scoreBonusColor);
			}
			// Hit strike in launching phase
			if (Player.Instance.gameEnded == false && Player.Instance.launchingPhase == true) {
				//	Player.Instance.ScorePin (Player.Instance.strikeScoreBonus);
				//FeedbackManager.Instance.PlayFeedbackText (transform.position, 1.2f, Player.Instance.currentSpeed / 3f, 0.5f, 2f, 0.6f, "STRIKE!\n+" + (Player.Instance.strikeScoreBonus * Player.Instance.currentMutliplier).ToString (), Player.Instance.scoreBonusColor);
			}


			//Player.Instance.ScorePinGroup (transform.position);

			FeedbackManager.Instance.PlayExplosionParticles (transform.position);

			//FeedbackManager.Instance.ZoomEffect();
			//	FeedbackManager.Instance.Screenshake ();
            

		}

	}
}
