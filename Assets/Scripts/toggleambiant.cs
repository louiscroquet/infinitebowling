﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toggleambiant : MonoBehaviour
{
	public AudioSource Ambiant;
	float d;
	// Use this for initialization
	void Start ()
	{
		//d = Ambiant.volume;
		Ambiant.volume = UserConfig.BackgroundMusicVolume;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void Toggle ()
	{
		if (Ambiant.volume > 0f) {
			UserConfig.BackgroundMusicVolume = 0f;

		} else {
			UserConfig.BackgroundMusicVolume = 0.35f;

		}
		Ambiant.volume = UserConfig.BackgroundMusicVolume;
	}
}
