﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBlock : MonoBehaviour
{
	public GameObject BlockModelGO;
	public ParticleSystem ExplosionParticles;

	private bool adaptedToCurve = false;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void SetAdapted ()
	{
		adaptedToCurve = true;

	}

	public void AdaptToCurve ()
	{
		if (adaptedToCurve)
			return;
		adaptedToCurve = true;
		float distance = transform.position.z;

		float offset = transform.position.x;

		transform.position = CurveManager.Instance.CalcPositionByDistance (distance);

		transform.forward = CurveManager.Instance.CalcTangentByDistance (distance);
		transform.position += transform.right * offset;
	}

	public void Explode ()
	{
		FeedbackManager.Instance.PlayObstacleDestroyedParticles(transform.position,GetComponentInChildren<MeshRenderer>());

        BlockModelGO.SetActive (false);
        if(ExplosionParticles != null)
        {
            ExplosionParticles.Play();
        }
		
	}
}
