﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    const string Key_CurrentLevel = "LM_CurrentLevel";
    const string Key_CurrentPercentage = "LM_CurrentPercentage";

    public static LevelManager Instance;


    public int BasePinPerLevel = 500;
    public int PinPerLevel = 100;
    public float MaxLevelLength;

    public int BaseSectionPerLevel = 10;
    public int SectionPerLevel = 2;
    public int MaxSectionPerLevel = 20;

    public int CurrentLevel;

    public LevelTheme FireTheme;
    public Color NormalObstacleColor;
    public Color FireObstacleColor;

    [Header("Progress faker")]
    public float CurrentPercentage = 100f;
    public Vector2 PercentageVariation = new Vector2(0.92f, 0.96f);
    public List<LevelTheme> AvailableThemes = new List<LevelTheme>();

    public int CurrentLevelPinTotal
    {
        get
        {
            if (CurrentLevel == 0)
                return 250;
            else
                return Mathf.Min(BasePinPerLevel + CurrentLevel * PinPerLevel, 150);
        }
    }

    public int CurrentLevelSectionTotal
    {
        get
        {
            if (CurrentLevel == 0)
                return MaxSectionPerLevel;
            else
                return Mathf.Min(BaseSectionPerLevel + CurrentLevel * SectionPerLevel, MaxSectionPerLevel);
        }
    }

    public float CurrentLevelLength
    {
        get
        {
            if (CurrentLevel == 0)
                return MaxSectionPerLevel;
            else
                return Mathf.Min(CurrentLevel * 50f, MaxLevelLength);
        }
    }


    void Awake()
    {
        Instance = this;
        //SetFire (true);
        //SetFire (false);
        CurrentPercentage = PlayerPrefs.GetFloat(Key_CurrentPercentage, CurrentPercentage);

        CurrentLevel = PlayerPrefs.GetInt(Key_CurrentLevel, 0);

    }


    // Use this for initialization
    void Start()
    {
         //StartLevel (CurrentLevel);

        GameManager.Instance.InitLevel(CurrentLevel, 0);
        ApplyTheme(CurrentLevel);


    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LevelUp()
    {
        CurrentPercentage = CurrentPercentage * Random.Range(PercentageVariation.x, PercentageVariation.y);
        PlayerPrefs.SetFloat(Key_CurrentPercentage, CurrentPercentage);
        CurrentLevel++;
        PlayerPrefs.SetInt(Key_CurrentLevel, CurrentLevel);
    }

    public LevelTheme CurrentTheme;

    public void StartLevel(int i)
    {
        CurrentLevel = i;
        //CurrentTheme = AvailableThemes [i % AvailableThemes.Count];
        //ApplyTheme (CurrentTheme);
        GameManager.Instance.InitLevel(CurrentLevel, CurrentLevelPinTotal);
        SetFire(false);
    }


    [Header("Level Theme")]
    public SpriteRenderer BackgroundGradient;
    public List<MeshRenderer> Grounds;

    public Material PinMaterial;
    public Material PinStripeMaterial;
    public List<SpriteRenderer> GroundGradients = new List<SpriteRenderer>();
    public Material GroundLineRendererMaterial;
    public LevelTheme DEBUG_Theme;

    public Material obstacleMaterial;
    public Material movingObstacleMaterial;
    public bool DEBUG_DontApplyTheme = false;
    [ContextMenu("Apply Current Theme")]
    public void ApplyDEBUGTheme()
    {
        if (DEBUG_Theme != null)
        {
            bool state = DEBUG_DontApplyTheme;
            DEBUG_DontApplyTheme = false;
            ApplyTheme(DEBUG_Theme);
            DEBUG_DontApplyTheme = state;

        }
    }

    public void ApplyTheme(int i)
    {
        if (DEBUG_DontApplyTheme)
            return;
        LevelTheme lt = AvailableThemes[i % AvailableThemes.Count];
        CurrentTheme = lt;
        if (!OnFire)
            ApplyTheme(lt);
    }

    public void ApplyTheme(LevelTheme lt)
    {

        Camera.main.backgroundColor = lt.BackgroundColor;
        BackgroundGradient.color = lt.GradientColor;
        RenderSettings.fogColor = lt.FogColor;
        foreach (var g in Grounds)
        {
            g.material = lt.GroundMaterial;

        }
        PinMaterial.mainTexture = lt.PinColor;
        PinStripeMaterial.mainTexture = lt.PinStripeColor;

        obstacleMaterial.color = lt.obstacleColor;
        movingObstacleMaterial.color = lt.movingObstacleColor;
        NormalObstacleColor = lt.obstacleColor;

        foreach (var s in GroundGradients)
        {
            s.color = lt.GroundGradientsColor;
        }
        if (GroundLineRendererMaterial != null && lt.GroundLineRendererTexture != null)
            GroundLineRendererMaterial.mainTexture = lt.GroundLineRendererTexture;
    }

    [ContextMenu("Save Current Theme")]
    public void DEBUG_SaveToDEBUGTheme()
    {
        if (DEBUG_Theme == null)
        {
            Debug.LogError("No theme to save on : ");
            return;
        }
        LevelTheme lt = DEBUG_Theme;

        lt.BackgroundColor = Camera.main.backgroundColor;
        lt.GradientColor = BackgroundGradient.color;
        lt.FogColor = RenderSettings.fogColor;
        lt.GroundMaterial = Grounds[0].sharedMaterial;
        lt.PinColor = PinMaterial.mainTexture;
        lt.PinStripeColor = PinStripeMaterial.mainTexture;
        lt.GroundGradientsColor = GroundGradients[0].color;
    }

    bool OnFire = false;


    public void SetFire(bool state)
    {
        OnFire = state;
        if (state)
        {
            ApplyTheme(FireTheme);
            obstacleMaterial.color = FireObstacleColor;

        }
        else
        {
            ApplyTheme(CurrentTheme);

            obstacleMaterial.color = NormalObstacleColor;

        }
    }
}
