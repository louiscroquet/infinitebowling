﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPattern : MonoBehaviour
{
	public Pattern PatternTemplate;
	public List<GameObject> AvailablePatterns;
	public bool ExactList = false;
	public bool RepeatPattern = false;

	public int PatternsToSpawn = 5;
	public float DistanceBetweenPatterns = 50f;
	public string PatternName = "Auto Pattern";

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	[ContextMenu ("Generate Auto Pattern")]
	public void SpawnAutoPattern ()
	{
		if (AvailablePatterns.Count == 0) {
			Debug.LogError ("No pattern to spawn in list : ");
			return;
		}
		var neo = Instantiate (PatternTemplate, null);
		neo.gameObject.SetActive (true);
		neo.name = "(Clone) " + PatternName + " ";

		if (ExactList) {
			var p = AvailablePatterns [0];
			for (int i = 0; i < AvailablePatterns.Count; i++) {
				p = AvailablePatterns [i];
				var go = Instantiate (p, neo.transform);
				go.gameObject.SetActive (true);
				go.transform.localPosition = new Vector3 (0f, 0f, DistanceBetweenPatterns * i);
				neo.trigger.transform.localPosition = new Vector3 (0f, 0f, DistanceBetweenPatterns * (AvailablePatterns.Count - 1));

			}
		} else {
			var p = AvailablePatterns [Random.Range (0, AvailablePatterns.Count)];
			for (int i = 0; i < PatternsToSpawn; i++) {
				if (!RepeatPattern)
					p = AvailablePatterns [Random.Range (0, AvailablePatterns.Count)];
				var go = Instantiate (p, neo.transform);
				go.gameObject.SetActive (true);
				go.transform.localPosition = new Vector3 (0f, 0f, DistanceBetweenPatterns * i);
				neo.trigger.transform.localPosition = new Vector3 (0f, 0f, DistanceBetweenPatterns * (PatternsToSpawn - 1));

			}
		}



	}
}
