﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{

	public bool activateOnStart;

	// X
	public bool moveX;
	public float xTargetPosition;
	public float xSpeed;

	// Y
	public bool moveY;
	public float yTargetPosition;
	public float ySpeed;

	// Y
	public bool moveZ;
	public float zTargetPosition;
	public float zSpeed;

	public float PauseBetweenLoops;
	private Vector3 defaultLocalPosition;
	private bool started = false;
	// Use this for initialization
	void Start ()
	{

	}

	void Update ()
	{
		if (!started && activateOnStart) {
			if (Player.Instance.launchingPhase || Player.Instance.cruisePhase) {
				defaultLocalPosition = transform.position;

				started = true;
				Move ();
			}

		}
	}

	public void Move ()
	{
		//float speed = Mathf.Max (xSpeed, ySpeed, zSpeed);
		//LeanTween.moveLocal (gameObject, transform.localPosition + new Vector3 (xTargetPosition, yTargetPosition, zTargetPosition), speed).setLoopPingPong (1)

		if (moveX) {
			LeanTween.move (gameObject, defaultLocalPosition + xTargetPosition * 2f * transform.right, xSpeed).setDelay (PauseBetweenLoops).setOnComplete (
				x => {
					LeanTween.move (gameObject, defaultLocalPosition, xSpeed).setDelay (PauseBetweenLoops).setOnComplete (Move);

				});
			;
		}

		if (moveY) {
			LeanTween.moveLocalY (gameObject, defaultLocalPosition.y + yTargetPosition * 2f, ySpeed).setDelay (PauseBetweenLoops).setOnComplete (
				x => {
					LeanTween.moveLocalY (gameObject, defaultLocalPosition.y, ySpeed).setDelay (PauseBetweenLoops).setOnComplete (Move);

				});
			;
		}

		if (moveZ) {
			LeanTween.moveLocalZ (gameObject, defaultLocalPosition.z + zTargetPosition * 2f, zSpeed).setDelay (PauseBetweenLoops).setOnComplete (
				x => {
					LeanTween.moveLocalZ (gameObject, defaultLocalPosition.z, zSpeed).setDelay (PauseBetweenLoops).setOnComplete (Move);

				});
			;
		}

	}
}
