﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusPickable : MonoBehaviour
{
	public string Letter = "B";
	public ParticleSystem CollectParticles1;

	public ParticleSystem CollectParticles2;
	public List<GameObject> LettersGO = new List<GameObject> ();
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
	/*
	public void Init (int bonusLetter)
	{
		for (int i = 0; i < LettersGO.Count; i++) {
			if (i == bonusLetter)
				LettersGO [i].SetActive (true);
			else
				LettersGO [i].SetActive (false);

		}
	}
*/
	public void CollectBonus ()
	{
		if (CollectParticles1 != null)
			CollectParticles1.Play ();
		if (CollectParticles2 != null)
			CollectParticles2.Play ();
		LeanTween.scale (gameObject, Vector3.zero, 0.5f).setEase (LeanTweenType.easeInBack);
		FeedbackManager.Instance.Show3DFireTextFeedback (Letter, transform.position + transform.up * 2f, Color.white);
		BonusManager.Instance.CollectBonusLetter ();
		CameraFeedback.Instance.ScreenshakeHard ();
	}


	bool adapted = false;

	public void AdaptToCurve ()
	{
		if (adapted)
			return;
		adapted = true;
		float distance = transform.position.z;

		//GetComponent<Rigidbody> ().useGravity = false;

		float offset = transform.position.x;

		transform.position = CurveManager.Instance.CalcPositionByDistance (distance);

		transform.forward = CurveManager.Instance.CalcTangentByDistance (distance);
		transform.position += transform.right * offset;
	}
}
