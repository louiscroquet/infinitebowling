﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreativeMode : MonoBehaviour {

    public GameObject creativePanel;

    public List<GameObject> backgrounds = new List<GameObject>();

	public void ShowCreativePanel()
    {
        creativePanel.SetActive(true);
    }

    public void HideCreativePanel()
    {
        creativePanel.SetActive(false);
    }
	
	public void ActivateBackground(GameObject bgToActivate)
    {
        HideAllBackgrounds();
        bgToActivate.SetActive(true);
    }

    public void HideAllBackgrounds()
    {
        foreach (GameObject bg in backgrounds)
        {
            bg.SetActive(false);
        }
    }
}
