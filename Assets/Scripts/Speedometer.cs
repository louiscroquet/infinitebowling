﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Speedometer : MonoBehaviour
{
	public Text m_Text;


	public Gradient SpeedColors;
	public float MinSpeed;
	public float MaxSpeed;


	void Awake ()
	{
		//	m_Text = GetComponent<Text> ();
	}
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		UpdateSpeed ();
	}

	public void UpdateSpeed ()
	{
		float speed = Player.Instance.currentSpeed;
		m_Text.text = ((int)speed).ToString ();

		float t = Mathf.Clamp (speed, MinSpeed, MaxSpeed) / (MaxSpeed - MinSpeed);
		m_Text.color = SpeedColors.Evaluate (t);
		
	}
}
