﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscountRewardedButton : RewardedButton {

    public override void CollectReward()
    {
        if (collectedDuringThisGame)
        {
            return;
        }

        base.CollectReward();

        ShopMenu.Instance.ValidateDiscount();
    }
}
