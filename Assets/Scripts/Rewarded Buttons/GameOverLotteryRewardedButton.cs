﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverLotteryRewardedButton : RewardedButton {

    public GameObject wheelIcon;

    override protected void Start () {
        base.Start ();
        LeanTween.cancel (wheelIcon);
        LeanTween.rotateAroundLocal (wheelIcon, Vector3.forward, 360f, 2f).setEaseInOutSine ().setLoopPingPong (0);

    }

    public override void ShowRewardedVideo (string placementId) {

        base.ShowRewardedVideo (placementId);
    }

    public override void CollectReward () {
        if (collectedDuringThisGame) {
            return;
        }

        base.CollectReward ();
        UIController.Instance.ShowPanelInstantly (11);
        LotteryManager.Instance.Initialize ();
    }

}