﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LotterySpinAgainRewardedButton : RewardedButton {

    public override void CollectReward()
    {
        if (collectedDuringThisGame)
        {
            return;
        }


        base.CollectReward();
        LotteryManager.Instance.Initialize();
    }
}
