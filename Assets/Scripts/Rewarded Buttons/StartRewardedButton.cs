﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartRewardedButton : RewardedButton
{

    public Text startRewardText;
    public Upgrade pinCashUpgrade;
    public Upgrade offlineUpgrade;

    private float CurrentReward()
    {
        return (((pinCashUpgrade.CurrentUpgradeCost * 0.25f) + (offlineUpgrade.CurrentUpgradeCost * 0.25f)) * 2);
    }


    // Update button
    public override void EnableButton()
    {
        if (MonetizationManager.Instance != null)
        {
            base.EnableButton();
            startRewardText.text = "+$" + ValueConvertor.ConvertDoubleToString(CurrentReward());
        }

    }


    public override void CollectReward()
    {
        if (collectedDuringThisGame)
        {
            return;
        }

        base.CollectReward();
        MoneyManager.Instance.AddCash((int)CurrentReward(), false);
    }
}
