﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LimitedCashRewardedButton : RewardedButton {

    public Text limitedRewardText;

    // Update button
    public override void EnableButton () {
        base.EnableButton ();
        limitedRewardText.text = "+$" + ValueConvertor.ConvertDoubleToString (ShopMenu.Instance.CurrentReward);
    }

    public override void CollectReward () {
        if (collectedDuringThisGame) {
            return;
        }

        UIController.Instance.HidePanelInstantly (8);

        base.CollectReward ();

        // add cash
        MoneyManager.Instance.AddCash (ShopMenu.Instance.CurrentReward, false);
    }

    public override void RewardedClosed () {
        base.RewardedClosed ();
        UIController.Instance.HidePanelInstantly (8);
    }
}