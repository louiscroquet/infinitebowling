﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitedBoostRewardedButton : RewardedButton
{



    // Update button
    public override void EnableButton()
    {
        base.EnableButton();

    }

    public override void CollectReward()
    {
        if (collectedDuringThisGame)
        {
            return;
        }

        UIController.Instance.HidePanelInstantly(9);

        base.CollectReward();

        Player.Instance.AddBoost(3);
        Player.Instance.canMove = true;
        if (GameManager.Instance.isGameStarted)
            Player.Instance.TriggerFireFromGameplay();
    }

    public override void RewardedClosed()
    {
        base.RewardedClosed();
        UIController.Instance.HidePanelInstantly(9);
    }
}
