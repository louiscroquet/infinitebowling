﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfflineRewardedButton : RewardedButton
{



    private new void Start()
    {
        if (MonetizationManager.Instance != null)
        {

            base.Initialize();
            UpgradeManager.Instance.ShowOfflineReward();
        }
    }




    // Update button
    public override void EnableButton()
    {
        if (MonetizationManager.Instance != null)
        {
            base.EnableButton();
            collectedDuringThisGame = false;
        }

    }


    public override void CollectReward()
    {
        if (collectedDuringThisGame)
        {
            return;
        }

        collectedDuringThisGame = true;
        base.CollectReward();
        UpgradeManager.Instance.CollectOfflineReward(true);
    }


}
