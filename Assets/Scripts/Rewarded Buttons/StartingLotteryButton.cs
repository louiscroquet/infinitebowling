﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingLotteryButton : RewardedButton {

    public GameObject wheelImage;

    override protected void Start () {
        base.Start ();
        if (wheelImage != null) {
            LeanTween.cancel (wheelImage);
            LeanTween.rotateAroundLocal (wheelImage, Vector3.forward, 360f, 2f).setEaseInOutCubic ().setLoopPingPong (0);
        }
    }

    public override void CollectReward () {
        if (collectedDuringThisGame) {
            return;
        }

        base.CollectReward ();
        LotteryManager.Instance.Initialize ();
        UIController.Instance.ShowPanel (11, false);
    }
}