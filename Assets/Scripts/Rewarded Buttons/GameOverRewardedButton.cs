﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverRewardedButton : RewardedButton
{

    // Update button
    public override void EnableButton()
    {
        base.EnableButton();
    }

    public override void ShowRewardedVideo(string placementId)
    {
        GameManager.Instance.EndReviveCountdown();
            base.ShowRewardedVideo(placementId);
    }

    public override void CollectReward()
    {
        if (collectedDuringThisGame)
        {
            return;
        }


        base.CollectReward();
        GameManager.Instance.Revive();
    }

    public override void RewardedClosed()
    {
        base.RewardedClosed();
        if (!Player.Instance.OnFire)
        {
            UIController.Instance.HidePanelInstantly(4);
            GameManager.Instance.GameOver();
        }
    }

}
