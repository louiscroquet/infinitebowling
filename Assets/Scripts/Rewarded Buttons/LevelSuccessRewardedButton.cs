﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSuccessRewardedButton : RewardedButton
{

    public float CurrentLevelSuccessReward;
    public Text tripleRewardText;



    // Update button
    public override void EnableButton()
    {
        base.EnableButton();
        tripleRewardText.text = "<size=150>$</size>" + ValueConvertor.ConvertDoubleToString(GameManager.Instance.currentScore * 3);
    }



    public override void CollectReward()
    {

        if (collectedDuringThisGame)
        {
            return;
        }

        base.CollectReward();

        MoneyManager.Instance.AddCash(GameManager.Instance.currentScore * 3, false);
        UIController.Instance.HidePanel(2);

        if (MonetizationManager.Instance.isRewardedAdAvailable)
        {
            // On display la proposition de lottery si rewarded dispo
            UIController.Instance.ShowPanel(10);
        }
        else
        {
            // si reward pas dispo, 
            Invoke("Retry", 0.5f);
            
        }


    }

    void Retry()
    {
        GameManager.Instance.Retry();
    }



}
