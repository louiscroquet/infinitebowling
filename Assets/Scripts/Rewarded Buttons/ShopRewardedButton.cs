﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopRewardedButton : RewardedButton
{

    public Text shopRewardedText;

    public bool upgradeRewarded;

    private new void Start()
    {
        if (MonetizationManager.Instance != null)
        {
            base.Initialize();
            if (upgradeRewarded)
            {

                //  UIController.Instance.HidePanelInstantly(7);
            }
            else
            {
                //UIController.Instance.HidePanelInstantly(6);
            }

            shopRewardedText.text = "+$" + ValueConvertor.ConvertDoubleToString(ShopMenu.Instance.CurrentReward);
        }


    }

    // Update button
    public override void EnableButton()
    {
        base.EnableButton();
        shopRewardedText.text = "+$" + ValueConvertor.ConvertDoubleToString(ShopMenu.Instance.CurrentReward);
    }

    public override void CollectReward()
    {
        if (collectedDuringThisGame)
        {
            return;
        }

        if (upgradeRewarded)
        {
            UIController.Instance.HidePanelInstantly(7);
        }
        else
        {
            UIController.Instance.HidePanelInstantly(6);
        }

        base.CollectReward();

        // add cash
        MoneyManager.Instance.AddCash(Mathf.CeilToInt(ShopMenu.Instance.CurrentReward), false);
    }

    public override void RewardedClosed()
    {
        base.RewardedClosed();
        if (upgradeRewarded)
        {
            UIController.Instance.HidePanelInstantly(7);
        }
        else
        {
            UIController.Instance.HidePanelInstantly(6);
        }
    }
}
