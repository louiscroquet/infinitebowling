﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbianceMusic : MonoBehaviour {
    public static AmbianceMusic Instance;
    private AudioSource MusicSource;
    private void Awake () {
        Instance = this;
        MusicSource = GetComponent<AudioSource> ();
    }
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    public void Pause () {
        MusicSource.Pause ();
    }

    public void Resume () {
        MusicSource.UnPause ();

    }
}