﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DEBUG_AdmobRewarded : MonoBehaviour {

    public Text Status;

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        Status.text = "Rewarded\n" + MonetizationManager.Instance.isRewardedAdAvailable;
    }

    public void Action () {
        if (MonetizationManager.Instance.isRewardedAdAvailable) {
            // Time.timeScale = 0f;

            MonetizationManager.Instance.ShowRewarded ("test", null);
        }
    }
}