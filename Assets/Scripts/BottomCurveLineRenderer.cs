﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomCurveLineRenderer : MonoBehaviour
{
	public LineRenderer toCopy;
	public LineRenderer toDisplay;

	private bool scheduled = false;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (scheduled) {
			CopyValues ();
			scheduled = false;
		}
	}

	public void ScheduleCopy ()
	{
		scheduled = true;
//		Invoke ("CopyValues", 0.5f);
	}

	[ContextMenu ("Generate")]
	public void CopyValues ()
	{
		toCopy = transform.parent.GetComponentInParent<LineRenderer> ();
		if (toCopy == null) {
			Debug.LogError ("NO line renderer on parent : " + name);
			return;
		}
		toDisplay = GetComponent<LineRenderer> ();

		if (toDisplay == null) {
			Debug.LogError ("NO line renderer on component : " + name);
			return;
		}
		toDisplay.positionCount = toCopy.positionCount;
		//Debug.Log ("tocopy pos : " + toCopy.positionCount);
		var pos = new Vector3 [toDisplay.positionCount];
		toCopy.GetPositions (pos);
		toDisplay.SetPositions (pos);
		//Debug.Log ("toDisplay pos : " + toDisplay.positionCount);


	}
}
