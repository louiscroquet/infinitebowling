﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyNoAdsButton : MonoBehaviour {

    // Start is called before the first frame update
    void Start () {
        if (UserConfig.NoAds)
            gameObject.SetActive (false);

    }

    // Update is called once per frame
    void Update () {

    }

    public void Action () {
        Debug.Log ("Buying No Ads");

        IAPManager.Instance.BuyNonConsumable ();
    }
}