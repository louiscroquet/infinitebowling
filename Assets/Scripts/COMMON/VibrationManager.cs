﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class VibrationManager : MonoBehaviour {

    public static VibrationManager Instance;

    private void Awake()
    {
        Instance = this;
        MMVibrationManager.iOSInitializeHaptics();
    }

    public void VibrateLight()
    {
        if(!UserConfig.Vibration)
			{
				return;
			}
        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

    public void VibrateMedium()
    {
        if(!UserConfig.Vibration)
			{
				return;
			}
            MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }

    public void VibrateHeavy()
    {
        if(!UserConfig.Vibration)
			{
				return;
			}
            MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }

    public void VibrateSuccess()
    {
        
        if(!UserConfig.Vibration)
			{
				return;
			}
            MMVibrationManager.Haptic(HapticTypes.Success);
    }

    public void VibrateWarning()
    {
        if(!UserConfig.Vibration)
			{
				return;
			}
            MMVibrationManager.Haptic(HapticTypes.Warning);
    }

    public void VibrateFailure()
    {
        if(!UserConfig.Vibration)
			{
				return;
			}
            MMVibrationManager.Haptic(HapticTypes.Failure);
    }
}
