﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestorePurchases : MonoBehaviour {
    // Start is called before the first frame update
    void Start () {
#if !UNITY_IPHONE && !UNITY_EDITOR
        gameObject.SetActive (false);
#endif
    }

    // Update is called once per frame
    void Update () {

    }

    public void Action () {
        Debug.Log ("Restoring iOS Purchases");

        IAPManager.Instance.RestorePurchases ();
    }
}