﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScreenTools : MonoBehaviour
{
	public RectTransform ScreenCanvas;
	public RectTransform SafeZoneCanvas;

	public static Vector3[] ScreenWorldCorners;
	public static Vector3[] SafeZoneWorldCorners;

	public static float ScreenMinX {
		get {
			return ScreenWorldCorners [0].x;
		}
	}

	public static float ScreenMaxX {
		get {
			return ScreenWorldCorners [2].x;
		}
	}

	public static float ScreenMinY {
		get {
			return ScreenWorldCorners [0].y;
		}
	}

	public static float ScreenMaxY {
		get {
			return ScreenWorldCorners [2].y;
		}
	}

	public static float SafeZoneMinX {
		get {
			return SafeZoneWorldCorners [0].x;
		}
	}

	public static float SafeZoneMaxX {
		get {
			return SafeZoneWorldCorners [2].x;
		}
	}

	public static float SafeZoneMinY {
		get {
			return SafeZoneWorldCorners [0].y;
		}
	}

	public static float SafeZoneMaxY {
		get {
			return SafeZoneWorldCorners [2].y;
		}
	}



	void Awake ()
	{
		ScreenWorldCorners = new Vector3[4];
		SafeZoneWorldCorners = new Vector3[4];
		ScreenCanvas.GetWorldCorners (ScreenWorldCorners);	
		SafeZoneCanvas.GetWorldCorners (SafeZoneWorldCorners);	
	}


	public static Vector3 SafeZoneClamp (Vector3 v)
	{
		Vector3 res = v;
		res.x = Mathf.Clamp (v.x, SafeZoneMinX, SafeZoneMaxX);
		res.y = Mathf.Clamp (v.y, SafeZoneMinY, SafeZoneMaxY);
		return res;
	}
}
