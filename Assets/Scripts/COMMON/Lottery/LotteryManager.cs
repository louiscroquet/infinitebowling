﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LotteryManager : MonoBehaviour {

    public static LotteryManager Instance;

    [Header("Spins")]
    public int maxSpinsPossible;
    private int currentSpins;


    [Header("Rewards")]
    public List<LotteryReward> currentLotteryRewards = new List<LotteryReward>();
    public List<LotteryReward> LotteryRewardsRarityOrder = new List<LotteryReward>();
    public LotteryReward wonReward;


    [Header("Buttons")]
    public GameObject spinButton;
    public GameObject spinAgainButton;
    public GameObject continueButton;

    [Header("Feedback")]
    public GameObject prizeWheel;
    public ParticleSystem collectRewardParticles;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        currentSpins = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Initialize();
        }
    }

    public void Initialize()
    {

        spinButton.SetActive(true);
        spinAgainButton.SetActive(false);
        continueButton.SetActive(false);

        prizeWheel.transform.localRotation = Quaternion.identity;

        // on spawn autant de wheel parts que de rewards dans la liste et on les initialise en conséquence
        for (int rewardNb = 0; rewardNb < currentLotteryRewards.Count; rewardNb++)
        {
            float rewardAngle = (360f / currentLotteryRewards.Count) * rewardNb;
            currentLotteryRewards[rewardNb].Initialize(rewardAngle, 1f / currentLotteryRewards.Count, 360f / currentLotteryRewards.Count);
        }

    }

    public void InitializeBySpinAgain()
    {
        spinButton.SetActive(true);
        prizeWheel.transform.localRotation = Quaternion.identity;
    }


    void SelectPrize()
    {
        float totalRarity = 0f;
        // On prend la rarity totale
        foreach (var rew in LotteryRewardsRarityOrder)
        {
            totalRarity += (float)rew.rewardRarity;
        }

        // On tire la reward dans le total de rarité
        float reward = Random.Range(0f, totalRarity);
        float tmpRarity = 0f;

        // On trouve la reward correspodnante :
        for (int i = 0; i < LotteryRewardsRarityOrder.Count; i++)
        {
            tmpRarity += (float)LotteryRewardsRarityOrder[i].rewardRarity;
            if (reward < tmpRarity)
            {
                wonReward = LotteryRewardsRarityOrder[i];
                break;
            }
        }
    }

    public void SpinWheel()
    {
        SelectPrize();
        spinButton.SetActive(false);

        // faire tourner la roulette sur la wonReward
        // angle final = angle de la reward * -1f + angle de la reward pivot

        float finalAngle = wonReward.transform.localRotation.eulerAngles.z * -1f + wonReward.rewardPivot.transform.localRotation.eulerAngles.z * -1f;
        Debug.LogError("final angle: " + finalAngle.ToString());
        Debug.LogError("prize lolcal rotation Z:" + wonReward.transform.localRotation.eulerAngles.z.ToString());
        Debug.LogError("prize pivot local rotation Z:" + wonReward.rewardPivot.transform.localRotation.eulerAngles.z.ToString());
        //prizeWheel.transform.localRotation = Quaternion.Euler(Vector3.forward * finalAngle);

        LeanTween.rotateAroundLocal(prizeWheel, Vector3.forward, finalAngle + Random.Range(4, 7) * 360f, 3f).setEaseOutSine().setOnComplete(CollectReward);
    }

    public void CollectReward()
    {
        wonReward.CollectLotteryReward();
        currentSpins++;
        if(MonetizationManager.Instance.isRewardedAdAvailable)
        {
            spinAgainButton.SetActive(true);
        }
        continueButton.SetActive(true);

        collectRewardParticles.Play();

    }

}
