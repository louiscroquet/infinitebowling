﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LotteryReward : MonoBehaviour {

    public int rewardValue;

    public int rewardRarity;

    public Image rewardWheelPart; // circle image

    public Sprite rewardIcon;
    public Image rewardImage;
    public GameObject rewardPivot;

    public Text rewardText;

    public virtual void Initialize(float angle, float fillAmount, float pivotAngle)
    {
        transform.localScale = Vector3.one;
        transform.localEulerAngles = new Vector3(0f, 0f, angle);
        rewardWheelPart.fillAmount = fillAmount;

        rewardText.text = "+" + ValueConvertor.ConvertDoubleToString(rewardValue);
        rewardImage.sprite = rewardIcon;

        if (rewardPivot != null)
        {
            rewardPivot.transform.localEulerAngles = Vector3.forward * pivotAngle/-2f;
        }
    }

    public virtual void CollectLotteryReward()
    {
        //FeedbackManager.Instance.PlayRewardParticles();
        LeanTween.cancel(gameObject);
        LeanTween.scale(gameObject, Vector3.one * 1.5f, 0.4f).setEaseOutBack().setLoopPingPong(1);

    }
}
