﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LotteryRewardMoney : LotteryReward {

    public Upgrade pinCashUpgrade;
    public Upgrade offlineUpgrade;

    private float currentRewardAmount;

    public override void Initialize(float angle, float fillAmount, float pivotAngle)
    {
        // la reward vaut entre 1 et 3 fois le cout des upgrades * 0.25
        rewardValue = Random.Range(1, 4) * (int)CurrentDefaultReward;

        base.Initialize(angle, fillAmount, pivotAngle);
    }


    public override void CollectLotteryReward()
    {
        base.CollectLotteryReward();
        //MoneyManager.Instance.AddCash(rewardValue);
        MoneyManager.Instance.AddCash(rewardValue, false);
    }

    public float CurrentDefaultReward
    {
        get
        {
            return ((pinCashUpgrade.CurrentUpgradeCost * 0.25f) + (offlineUpgrade.CurrentUpgradeCost * 0.25f));
        }
    }

}
