﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LotteryRewardBooster : LotteryReward {

    public override void Initialize(float angle, float fillAmount, float pivotAngle)
    {
        // you can get 2 to 5 boosters
        rewardValue = Random.Range(2, 6);

        base.Initialize(angle, fillAmount, pivotAngle);
        
    }


    public override void CollectLotteryReward()
    {
        base.CollectLotteryReward();
        Player.Instance.AddBoost(rewardValue);
    }
}
