﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LotteryRewardShopItem : LotteryReward {

    public CustomItem itemReward;

    public List<CustomItem> customItems = new List<CustomItem>();

    private GameObject lastPreview;

    public override void Initialize(float angle, float fillAmount, float pivotAngle)
    {
        /*for (int items = customItems.Count; items > 0; items--)
        {
            if (!customItems[items].Owned)
            {
                itemReward = customItems[items];
                break;
            }
        }*/

        List<CustomItem> cis = new List<CustomItem>();
        foreach (CustomItem ci in cis)
        {
            if (ci.Owned)
            {
                customItems.Remove(ci);
            }
        }

        if(customItems.Count > 0)
        {
            itemReward = customItems[Random.Range(0, customItems.Count)];
        }
        else
        {
            Debug.LogError("No custom item to win in lottery");
        }

        

        base.Initialize(angle, fillAmount, pivotAngle);

        rewardImage.gameObject.SetActive(true);
        
        // si on spin again on supprime la précedente image previw
        if(lastPreview != null)
        {
            Destroy(lastPreview);
        }

        // On remplace la default reward icon par l'image preview de l'item
        GameObject newIcon = Instantiate(itemReward.previewImage, rewardImage.transform.localPosition, Quaternion.identity);
        newIcon.transform.parent = rewardPivot.transform;
        newIcon.transform.localPosition = rewardImage.transform.localPosition;
        newIcon.transform.localScale = Vector3.one;
        newIcon.SetActive(true);
        lastPreview = newIcon;
        rewardImage.gameObject.SetActive(false);

        rewardText.text = "UNLOCK";
    }

    public override void CollectLotteryReward()
    {
        base.CollectLotteryReward();
        itemReward.Unlock();
    }


}
