﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class OfflineReward : MonoBehaviour
{
	public static OfflineReward Instance;


	const string Key_LastChangeTime = "Offline_LastChangeTime";


	void Awake ()
	{
		Instance = this;
		LoadDailyChallenge ();
		//	Save ();
	}
	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

	int lastRemaining;

	public int SecondsSinceLastCollection {
		get {

			LoadDailyChallenge ();
			return diffInSeconds;
				
		}
	}

	private int diffInSeconds;

	public void LoadDailyChallenge ()
	{
		if (!PlayerPrefs.HasKey (Key_LastChangeTime)) {
			Initialize ();
		}

		int now = DateTime.Now.DayOfYear * 24 * 60 * 60 + DateTime.Now.Hour * 60 * 60 + DateTime.Now.Minute * 60 + DateTime.Now.Second;
		int last = PlayerPrefs.GetInt (Key_LastChangeTime);
		diffInSeconds = Mathf.Abs (now - last);
		//Debug.Log ("Seconds since last check : " + diffInSeconds);


	}

	void Initialize ()
	{
		//int now = DateTime.Now.DayOfYear * 24 + DateTime.Now.Hour;
		int now = DateTime.Now.DayOfYear * 24 * 60 * 60 + DateTime.Now.Hour * 60 * 60 + DateTime.Now.Minute * 60 + DateTime.Now.Second;

		PlayerPrefs.SetInt (Key_LastChangeTime, now);
		Save ();
	}



	public void Save ()
	{
		//	int now = DateTime.Now.DayOfYear * 24 + DateTime.Now.Hour;
		int now = DateTime.Now.DayOfYear * 24 * 60 * 60 + DateTime.Now.Hour * 60 * 60 + DateTime.Now.Minute * 60 + DateTime.Now.Second;
		PlayerPrefs.SetInt (Key_LastChangeTime, now);

	}

	public void CollectReward ()
	{
		Save ();
	}
}
