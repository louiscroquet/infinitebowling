﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUG_PlayerPrefs : MonoBehaviour
{

	public string Key;
	public int ValueInt;
	public float ValueFloat;
	public string ValueString;

	[ContextMenu ("Set Int")]
	public void SetInt ()
	{
		Debug.Log ("Setting Key : " + Key + " with value : " + ValueInt.ToString ());
		PlayerPrefs.SetInt (Key, ValueInt);
	}

	[ContextMenu ("Set Float")]
	public void SetFloat ()
	{
		Debug.Log ("Setting Key : " + Key + " with value : " + ValueFloat.ToString ());
		PlayerPrefs.SetFloat (Key, ValueFloat);
	}

	[ContextMenu ("Set String")]
	public void SetString ()
	{
		Debug.Log ("Setting Key : " + Key + " with value : " + ValueString);
		PlayerPrefs.SetString (Key, ValueString);
	}

	[ContextMenu ("Delete all")]
	public void DeleteAll ()
	{
		PlayerPrefs.DeleteAll();
	}
}
