﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CustomizationManager : MonoBehaviour
{
    public static CustomizationManager Instance;

    public List<CustomizationGroup> AvailableGroups = new List<CustomizationGroup>();

    public GameObject shopRedDot;
    public Text shopRedDotText;

    public CustomItem lastItemClicked;

    public GameObject buyButton;

    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start()
    {
        //Debug.LogError("TODO : CHECK EVERY ID");
        foreach (var g in AvailableGroups)
        {
            g.InitGroup();
        }
        foreach (var g in AvailableGroups)
        {
            g.EquipLastItem();
        }
        updateShopItems();

        ShopMenu.Instance.TryToMakeDiscountAvailable();
        ShopMenu.Instance.HideShop();
    }

    // Update is called once per frame
    void Update()
    {


    }


    public void updateShopItems()
    {
        shopRedDot.SetActive(false);

        foreach (var g in AvailableGroups)
        {
            foreach (var i in g.AvailableItems)
            {
                i.UpdateItemStateAndPrice();

                //continue;

                // si on a la thune et qu'on ne possède pas l'item on met le dot rouge sur le bouton shop
                if (MoneyManager.Instance.CurrentMoney >= i.PriceOrLevel && !i.UnlockByLevel && !i.Owned)
                {
                    shopRedDot.SetActive(true);
                    shopRedDotText.text = "NEW!";
                }
            }
        }
    }

    [ContextMenu("Unlock all items")]
    public void UnlockAllItems()
    {

        foreach (var g in AvailableGroups)
        {
            foreach (var i in g.AvailableItems)
            {
                i.Unlock();
            }
        }
    }


    private List<string> customGroupIds = new List<string>();
    public bool CheckIdValid(string s)
    {
        if (customGroupIds.Contains(s))
        {
            Debug.LogError("ID Already in use");
            return false;
        }
        else
        {
            customGroupIds.Add(s);
            return true;

        }
    }


    public void TryToBuyPreviewedItem()
    {
        lastItemClicked.TryUnlock();

    }


}
