﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Active le target gameobject OnEquip, et désactive tous les autres du groupe.
 * 
 */
public class CI_SwitchGameObject : CustomItem {

	public GameObject TargetGameObject;

    public override void ApplyCustomItem()
    {
        foreach(var item in ParentGroup.AvailableItems)
        {
            if(item.GetType()==typeof(CI_SwitchGameObject))
            {
                ((CI_SwitchGameObject)item).TargetGameObject.SetActive(false);
            }
        }
        TargetGameObject.SetActive(true);
    }
}
