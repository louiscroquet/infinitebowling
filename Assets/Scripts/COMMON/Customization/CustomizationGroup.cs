﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizationGroup : MonoBehaviour {
    public string UniqueGroupID;
    public List<CustomItem> AvailableItems = new List<CustomItem> ();
    

    public bool IsEquipped(CustomItem item)
    {
       if(AvailableItems.Contains(item))
       {
            if (item.Owned)
            {
                if (EquippedItemIndex == AvailableItems.IndexOf(item))
                {
                    return true;
                }
            }        
           
       }
       return false;
    }

    public int EquippedItemIndex
    {
        get
        {
           return PlayerPrefs.GetInt(UniqueGroupID,0);
        }
    }



    private void Awake()
    {
     
     
    }

    public void InitGroup()
    {
        if (UniqueGroupID.Equals(""))
        {
            Debug.LogError("!!!!!! GROUP ID MUST BE UNIQUE AND NOT EMPTY !!!!!");
            return;
        }
        foreach (var item in AvailableItems)
        {
            item.ParentGroup = this;
        }

        //if (AvailableItems.Count > 0)
        //AvailableItems[0].Unlock();

    }

    public void EquipLastItem()
    {
        EquipItem(AvailableItems[EquippedItemIndex]);
    }


    public void PreviewLastEquippedItem()
    {
        CustomItem lastItem = AvailableItems[EquippedItemIndex];
        if (AvailableItems.Contains(lastItem))
        {
            if (lastItem.Owned)
            {
                lastItem.Preview();
                lastItem.ApplyOutline();
            }
        }
    }

    public void EquipItem(CustomItem item) {
        if (AvailableItems.Contains(item))
        {
            if (item.Owned)
            {
                int i = AvailableItems.IndexOf(item);
                PlayerPrefs.SetInt(UniqueGroupID,i);
                item.ApplyCustomItem();
                item.ApplyOutline();
            }
            else
            {
                int i = AvailableItems.IndexOf(item);
            }

        }
        CustomizationManager.Instance.updateShopItems();
    }

}
