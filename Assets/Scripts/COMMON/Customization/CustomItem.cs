﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameAnalyticsSDK;

public class CustomItem : MonoBehaviour
{
    public CustomizationGroup ParentGroup;
    public GameObject EquippedGO;
    public Text PriceDisplayer;
    private Image buttonImage;
    float localXNormal;

    public GameObject previewImage;
    public GameObject mysteryImage;

    private void Awake()
    {
        buttonImage = GetComponent<Image>();
        localXNormal = gameObject.transform.localPosition.x;

        selfOutline = GetComponent<Outline>();
        selfOutline.enabled = false;

    }

    public string UniqueID
    {
        get
        {
            return ParentGroup.UniqueGroupID + "_" + name;
        }
    }

    public bool Owned
    {
        get
        {
            if (PriceOrLevel <= 0)
                return true;
            if (PlayerPrefs.GetInt(UniqueID, 0) == 1)
                return true;
            if (UnlockByLevel && LevelManager.Instance != null && LevelManager.Instance.CurrentLevel >= PriceOrLevel)
                return true;
            return false;
        }
    }

    public bool UnlockByLevel = false;
    public int PriceOrLevel;


    public void UpdateItemStateAndPrice()
    {
        if (buttonImage == null)
        {
            buttonImage = GetComponent<Image>();
        }

        //if (UnlockByLevel && LevelManager.Instance.CurrentLevel >= PriceOrLevel && !Owned)
            //Unlock();

        EquippedGO.SetActive(false);


        if (ParentGroup.IsEquipped(this))
        {
            EquippedGO.SetActive(true);
            ApplyOutline();

            PriceDisplayer.text = "";

            if (UnlockByLevel)
            {
                if(mysteryImage != null)
                {
                    mysteryImage.SetActive(false);
                }
                if (previewImage != null)
                {
                    previewImage.SetActive(true);
                }
            }

        }
        else if (Owned)
        {
            PriceDisplayer.text = "";
            buttonImage.color = Color.white;

            if (UnlockByLevel)
            {
                previewImage.SetActive(true);

                if(mysteryImage != null)
                {
                    mysteryImage.SetActive(false);
                }
            }

        }
        else
        {
            if (UnlockByLevel)
            {
                PriceDisplayer.text = "LVL " + PriceOrLevel.ToString();
                if(mysteryImage != null)
                {
                    mysteryImage.SetActive(true);
                }
                if(previewImage != null)
                {
                    previewImage.SetActive(false);
                }
            }

            else
            {
                PriceDisplayer.text = "$" + ValueConvertor.ConvertDoubleToString(PriceOrLevel);
                PriceDisplayer.color = Color.black;

                if (mysteryImage != null)
                {
                    mysteryImage.SetActive(false);
                }
                if (previewImage != null)
                {
                    previewImage.SetActive(true);
                }
            }
                

            buttonImage.color = Color.grey;

        }
    }

    public void TryUnlock()
    {
        if (Owned)
        {
            return;
        }

        if (!UnlockByLevel)
        {
            if (MoneyManager.Instance.CurrentMoney >= PriceOrLevel)
            {
                MoneyManager.Instance.RemoveCash(PriceOrLevel);
                Unlock();
                TryEquipItem();
                CustomizationManager.Instance.buyButton.SetActive(false);
            }
            else
            {
                FailedUpgradeAnimation();
            }
        }
        else
        {
            FailedUpgradeAnimation();

        }
    }

    protected Color defaultUpgradeCostColor;

    public void FailedUpgradeAnimation()
    {
        defaultUpgradeCostColor = PriceDisplayer.color;
        PriceDisplayer.color = Color.red;

        if (SoundManager.Instance != null)
            SoundManager.Instance.Play(SoundManager.Instance.FailedUpgrade);
        LeanTween.cancel(gameObject);
        LeanTween.moveLocalX(gameObject, localXNormal + 5f, 0.05f).setLoopPingPong(3).setOnComplete(c =>
        {
            PriceDisplayer.color = defaultUpgradeCostColor;

        });

        if(MonetizationManager.Instance != null && MonetizationManager.Instance.isRewardedAdAvailable && !UnlockByLevel)
        {
            UIController.Instance.ShowPanel(6, false);
        }

    }

    public void Unlock()
    {
        Debug.LogError("Unlock");

        PlayerPrefs.SetInt(UniqueID, 1);
        VibrationManager.Instance.VibrateSuccess();
        FeedbackManager.Instance.PlayRewardParticles();
        SoundManager.Instance.Play(SoundManager.Instance.BuyUpgrade);

        GameAnalytics.NewDesignEvent("Item:" + transform.name + ":Bought");
        //MonetizationManager.Instance.TenjinInstance.SendEvent("Item:" + transform.name + ":Bought");
    }

    public virtual void TryEquipItem()
    {
        if (ParentGroup != null)
        {
            ParentGroup.EquipItem(this);
            ApplyOutline();
            GameAnalytics.NewDesignEvent("Item:" + transform.name + ":Equiped");
            //MonetizationManager.Instance.TenjinInstance.SendEvent("Item:" + transform.name + ":Equiped");
        }
    }

    public virtual void ApplyCustomItem()
    {
        Debug.LogError("This should be overriden to equip the item");
    }


    public virtual void Action()
    {

        CustomizationManager.Instance.lastItemClicked = this;

        if (Owned)
        {
            TryEquipItem();
            SoundManager.Instance.Play(SoundManager.Instance.ClickSound);

            CustomizationManager.Instance.buyButton.SetActive(false);

        }
        else
        {
            ApplyOutline();

            // Update state of the buy button
            if (!UnlockByLevel)
            {
                CustomizationManager.Instance.buyButton.SetActive(true);
            }
            else
            {
                CustomizationManager.Instance.buyButton.SetActive(false);
            }
            
        }
    }

    private Outline selfOutline;
    public void ApplyOutline()
    {
        
            // checker tous les autres items et désactiver leurs outlines
                foreach (var i in ParentGroup.AvailableItems)
                {
                    if(i.selfOutline.enabled == true)
                    {
                        i.selfOutline.enabled = false;
                    }
                }

        if (selfOutline != null)
        {
            selfOutline.enabled = true;
        }


    }

    public virtual void Preview()
    {

    }


    public virtual void DiscountPreview()
    {

    }



}
