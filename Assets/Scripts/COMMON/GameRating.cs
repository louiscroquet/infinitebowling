﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRating : MonoBehaviour {

    public string ratingUrl;
    private bool hasAlreadyRated;
    private int displayedNumber;

    private void Awake()
    {
        if(PlayerPrefs.GetInt("rated", 0) <= 0)
        {
            hasAlreadyRated = false;
        }
        else
        {
            hasAlreadyRated = true;
        }
    }


    public void DisplayRatingPanel()
    {
        int lastLevelDisplayedRating = PlayerPrefs.GetInt("LastLevelDisplayedRating", 0);
        

        if (LevelManager.Instance.CurrentLevel % 4 != 0 || hasAlreadyRated || LevelManager.Instance.CurrentLevel > 8 || lastLevelDisplayedRating == LevelManager.Instance.CurrentLevel)
        {
            return;
        }

        PlayerPrefs.SetInt("LastLevelDisplayedRating", LevelManager.Instance.CurrentLevel);

        UIController.Instance.ShowPanel(12);
    }

    public void RedirectToGamePage()
    {
        SkipRating();
        Application.OpenURL(ratingUrl);
        hasAlreadyRated = true;
        PlayerPrefs.SetInt("rated", 1);
    }

    public void SkipRating()
    {
        UIController.Instance.ShowPanel(3);
    }



}
