﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueConvertor : MonoBehaviour {

    public static ValueConvertor Instance;


    private void Awake()
    {
        Instance = this;
    }



    public static string ConvertDoubleToString(double valueToConvert)
    {
        if (valueToConvert >= 1000 && valueToConvert < 1000000)
        {
            return (valueToConvert / 1000).ToString("0.0") + "K";
        }
        else if (valueToConvert >= 1000000 && valueToConvert < 1000000000)
        {
            return (valueToConvert / 1000000).ToString("0.0") + "M";
        }
        else if (valueToConvert >= 1000000000 && valueToConvert < 1000000000000)
        {
            return (valueToConvert / 1000000000).ToString("0.0") + "B";
        }
        else if (valueToConvert >= 1000000000000 && valueToConvert < 1000000000000000)
        {
            return (valueToConvert / 1000000000000).ToString("0.0") + "T";
        }
        else
        {
            return valueToConvert.ToString("0");
        }
    }
}
