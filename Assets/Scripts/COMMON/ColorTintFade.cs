﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class ColorTintFade : MonoBehaviour
{

	public float FadeDuration = 0.5f;
	public Gradient FadeColors;
	public bool PingPong = true;
	public Material MaterialColor;
	public Image ImageColor;
	public SpriteRenderer RendererColor;
	public TextMesh TextColor;
	public Text UIText;
	// Use this for initialization
	void Start ()
	{
		FadeToNextColor ();
	}


	Color start;
	Color end;

	public void FadeToNextColor ()
	{
//		LeanTween.cancel (gameObject);
	
		if (PingPong)
			LeanTween.value (gameObject, updateColor, 0f, 1f, FadeDuration).setLoopPingPong (-1);
		else
			LeanTween.value (gameObject, updateColor, 0f, 1f, FadeDuration).setLoopClamp (-1);
	}

	Vector2 currentoffset;

	public void updateColor (float t)
	{
		Color c = FadeColors.Evaluate (t);
		//	m_ImageToModify.color = Color.Lerp (start, end, t);
		if (ImageColor != null)
			ImageColor.color = c;
		if (MaterialColor != null)
			MaterialColor.color = c;
		if (RendererColor != null)
			RendererColor.color = c;
		if (TextColor != null)
			TextColor.color = c;
		if (UIText != null)
			UIText.color = c;

	}

}
