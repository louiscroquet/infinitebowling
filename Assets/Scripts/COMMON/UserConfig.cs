﻿using System.Collections;
using UnityEngine;

public class UserConfig : MonoBehaviour {
	const string Key_NoAds = "UserConfig_NoAds";
	const string Key_SoundFX = "UserConfig_SoundFX";
	const string Key_AmbiantSound = "UserConfig_AmbiantSound";
	const string Key_AutoConnectFacebook = "UserConfig_AutoConnectFacebook";
	const string Key_Vibration = "UserConfig_Vibration";
	const string Key_Notifications = "UserConfig_Notifications";
	const string Key_GamepadButtons = "UserConfig_GamepadButtons";
	const string Key_BatterySaver = "UsercConfig_BatterySaver";
	const string Key_LeftHanded = "UserConfig_LeftHanded";
	const string Key_EasyMode = "UserConfig_EasyMode";

	const string Key_SoundFXVolume = "UserConfig_SoundFXVolume";
	const string Key_BackgroundMusicVolume = "UserConfig_BackgroundMusicVolume";
	const string Key_QualityLevel = "UserConfig_QualityLevel";
	const string Key_ShowControls = "UserConfig_ShowControls";

	//const string Key_HasRated = "UserConfig_HasRated";

	/*
	static public bool HasRated {
		get{
			if(PlayerPrefs.GetInt(Key_HasRated,0)==1)
				return true;
			else
				return false;
		}
	}*/
	private static bool modifiedGamepadSetting = true;
	private static bool cacheGamepadButtons;

	static public bool NoAds {
		get {
			//return false;
			if (MonetizationManager.Instance != null) {
				if (MonetizationManager.Instance.TEST_MODE)
					return false;
			}
			if (PlayerPrefs.GetInt (Key_NoAds, 0) == 1)
				return true;
			else
				return false;
		}
		set {
			if (value) {
				PlayerPrefs.SetInt (Key_NoAds, 1);

			} else {
				PlayerPrefs.SetInt (Key_NoAds, 0);

			}
			PlayerPrefs.Save ();
		}
	}

	static public bool EasyMode {
		get {
			if (PlayerPrefs.GetInt (Key_EasyMode, 0) == 1)
				return true;
			else
				return false;
		}
		set {
			if (value) {
				PlayerPrefs.SetInt (Key_EasyMode, 1);

			} else {
				PlayerPrefs.SetInt (Key_EasyMode, 0);

			}

		}
	}

	static public bool LeftHanded {
		get {
			if (PlayerPrefs.GetInt (Key_LeftHanded, 0) == 1)
				return true;
			else
				return false;
		}
		set {
			if (value) {
				PlayerPrefs.SetInt (Key_LeftHanded, 1);

			} else {
				PlayerPrefs.SetInt (Key_LeftHanded, 0);

			}

		}
	}

	static public bool BatterySaver {
		get {
			if (PlayerPrefs.GetInt (Key_BatterySaver, 0) == 1)
				return true;
			else
				return false;
		}
		set {
			if (value) {
				PlayerPrefs.SetInt (Key_BatterySaver, 1);
				//				if (PerformanceManager.Instance != null)
				//					PerformanceManager.Instance.ResetPerformanceManager ();
			} else {
				PlayerPrefs.SetInt (Key_BatterySaver, 0);
				//				if (PerformanceManager.Instance != null)
				//					PerformanceManager.Instance.ResetPerformanceManager ();

			}

		}
	}

	static public bool ShowControls {
		get {
			if (PlayerPrefs.GetInt (Key_ShowControls, 1) == 1)
				return true;
			else
				return false;
		}
		set {
			if (value)
				PlayerPrefs.SetInt (Key_ShowControls, 1);
			else
				PlayerPrefs.SetInt (Key_ShowControls, 0);
		}
	}

	static public int QualityLevel {
		get {
			return PlayerPrefs.GetInt (Key_QualityLevel, 3);
		}
		set {
			PlayerPrefs.SetInt (Key_QualityLevel, value);
		}

	}

	static public float SoundFXVolume {
		get {
			return PlayerPrefs.GetFloat (Key_SoundFXVolume, 1f);
		}
		set {
			PlayerPrefs.SetFloat (Key_SoundFXVolume, value);
			PlayerPrefs.Save ();
			if (SoundManager.Instance != null)
				SoundManager.Instance.SetVolume (value);
		}
	}

	static public float BackgroundMusicVolume {
		get {
			return PlayerPrefs.GetFloat (Key_BackgroundMusicVolume, 0.35f);
		}
		set {
			PlayerPrefs.SetFloat (Key_BackgroundMusicVolume, value);
			PlayerPrefs.Save ();
			//			if (SoundtrackManager.Instance != null)
			//				SoundtrackManager.Instance.SetVolume (value);
		}
	}

	static public bool GamepadButtons {
		get {
			if (modifiedGamepadSetting) {
				cacheGamepadButtons = PlayerPrefs.GetInt (Key_GamepadButtons, 0) == 1;
				modifiedGamepadSetting = false;
			}
			return cacheGamepadButtons;
		}
	}

	static public bool SoundFX {
		get {
			if (PlayerPrefs.GetInt (Key_SoundFX, 1) == 1)
				return true;
			else
				return false;
		}
	}

	static public bool AmbiantSound {
		get {
			if (PlayerPrefs.GetInt (Key_AmbiantSound, 1) == 1)
				return true;
			else
				return false;
		}
	}

	static public bool AutoConnectFacebook {
		get {
			if (PlayerPrefs.GetInt (Key_AutoConnectFacebook, 0) == 1)
				return true;
			else
				return false;
		}
	}

	static public bool Vibration {
		get {
			if (PlayerPrefs.GetInt (Key_Vibration, 1) == 1)
				return true;
			else
				return false;
		}
	}

	static public bool Notifications {
		get {
			if (PlayerPrefs.GetInt (Key_Notifications, 0) == 1)
				return true;
			else
				return false;
		}
	}

	static public void SetGamepadButtons (bool isON) {
		modifiedGamepadSetting = true;
		if (isON)
			PlayerPrefs.SetInt (Key_GamepadButtons, 1);
		else
			PlayerPrefs.SetInt (Key_GamepadButtons, 0);

	}

	static public void SetSoundFX (bool isON) {
		if (isON)
			PlayerPrefs.SetInt (Key_SoundFX, 1);
		else
			PlayerPrefs.SetInt (Key_SoundFX, 0);
	}

	static public void SetAmbiantSound (bool isON) {
		if (isON)
			PlayerPrefs.SetInt (Key_AmbiantSound, 1);
		else
			PlayerPrefs.SetInt (Key_AmbiantSound, 0);

	}

	static public void SetFacebookAutoLogin (bool isON) {
		if (isON)
			PlayerPrefs.SetInt (Key_AutoConnectFacebook, 1);
		else
			PlayerPrefs.SetInt (Key_AutoConnectFacebook, 0);

	}

	static public void SetVibration (bool isON) {
		if (isON)
			PlayerPrefs.SetInt (Key_Vibration, 1);
		else
			PlayerPrefs.SetInt (Key_Vibration, 0);

	}

	static public void SetNotification (bool isON) {
		if (isON)
			PlayerPrefs.SetInt (Key_Notifications, 1);
		else
			PlayerPrefs.SetInt (Key_Notifications, 0);

	}

	static public void SetShowControls (bool isON) {
		if (isON)
			PlayerPrefs.SetInt (Key_ShowControls, 1);
		else
			PlayerPrefs.SetInt (Key_ShowControls, 0);

	}

	/*static public void NeverShowRateAgain()
	{
		PlayerPrefs.SetInt(Key_HasRated,1);
	}*/
}