﻿ using System.Collections.Generic;
 using System.Collections;
 using UnityEngine.UI;
 using UnityEngine;

 public class UIController : MonoBehaviour {
     public static UIController Instance;
     public RectTransform ScreenOffset;
     public float AnimationDuration = 0.25f;
     public LeanTweenType AnimationEase;
     public List<CanvasGroup> UIPanels = new List<CanvasGroup> ();
     public CanvasGroup CashOverlay;

     // Use this for initialization
     void Awake () {
         Instance = this;
         ShowCashOverlayInstantly ();
         ShowPanelInstantly (3);

     }

     void Start () {
         if (UserConfig.NoAds)
             lastBottomOffset = 0f;
         SetScreenBottomOffset (MonetizationManager.Instance.CurrentScreenOffsetNeeded);

     }

     // Update is called once per frame
     void Update () {
         if (scheduleUpdateOffset) {
             applyOffset ();
         }
     }

     public void ShowPanelInstantly (int n) {
         ShowPanelInstantly (n, true);
     }

     public void ShowPanelInstantly (int n, bool hideOtherPanels) {
         //    Debug.Log("Show Panel Instantly : " + n);

         UIPanels[n].gameObject.SetActive (true);
         if (hideOtherPanels) {
             for (int i = 0; i < UIPanels.Count; i++) {
                 HidePanelInstantly (i);
             }
         }
         if (n >= 0 && n < UIPanels.Count) {
             if (LeanTween.isTweening (UIPanels[n].gameObject))
                 LeanTween.cancel (UIPanels[n].gameObject);
             UIPanels[n].interactable = true;
             UIPanels[n].blocksRaycasts = true;
             UIPanels[n].alpha = 1f;
         }
     }

     public void ShowPanel (int n) {
         ShowPanel (n, true);
     }

     public void ShowPanel (int n, bool HideOtherPanels) {
         //  Debug.Log("Show Panel : " + n);
         UIPanels[n].gameObject.SetActive (true);

         if (HideOtherPanels) {
             for (int i = 0; i < UIPanels.Count; i++) {
                 if (i != n)
                     HidePanel (i);
             }
         }
         if (n >= 0 && n < UIPanels.Count) {
             LeanTween.cancel (UIPanels[n].gameObject);
             UIPanels[n].interactable = true;
             UIPanels[n].blocksRaycasts = true;

             LeanTween.alphaCanvas (UIPanels[n], 1f, AnimationDuration).setEase (AnimationEase).setIgnoreTimeScale (true);
         }
     }

     public void HidePanel (int i) {

         if (i >= 0 && i < UIPanels.Count) {
             LeanTween.cancel (UIPanels[i].gameObject);
             UIPanels[i].interactable = false;
             UIPanels[i].blocksRaycasts = false;

             LeanTween.alphaCanvas (UIPanels[i], 0f, AnimationDuration).setEase (AnimationEase).setIgnoreTimeScale (true);

         }
     }

     public void HidePanelInstantly (int i) {
         if (i >= 0 && i < UIPanels.Count) {
             UIPanels[i].alpha = 0f;
             UIPanels[i].interactable = false;
             UIPanels[i].blocksRaycasts = false;

         }
     }

     public void ShowCashOverlayInstantly () {
         CashOverlay.GetComponent<CanvasGroup> ().alpha = 1f;
         //    LeanTween.alphaCanvas (CashOverlay, 1f, AnimationDuration).setEase (AnimationEase).setIgnoreTimeScale (true);
     }
     public void ShowCashOverlay () {
         LeanTween.cancel (CashOverlay.gameObject);
         LeanTween.alphaCanvas (CashOverlay, 1f, AnimationDuration).setEase (AnimationEase).setIgnoreTimeScale (true);
     }

     public void HideCashOverlay () {
         LeanTween.cancel (CashOverlay.gameObject);
         LeanTween.alphaCanvas (CashOverlay, 0f, AnimationDuration).setEase (AnimationEase).setIgnoreTimeScale (true);
     }
     static float lastBottomOffset = 0f;
     bool scheduleUpdateOffset = false;
     public void SetScreenBottomOffset (float pixels) {
         scheduleUpdateOffset = true;
         lastBottomOffset = pixels / Screen.height * 1920F;

     }

     void applyOffset () {
         if (ScreenOffset == null) {
             Debug.LogError (" SCREEN OFFSET NOT FOUND");
             return;
         }
         ScreenOffset.offsetMin = new Vector2 (ScreenOffset.offsetMin.x, lastBottomOffset);
         scheduleUpdateOffset = false;
     }

    public GameObject RewardedLoadingLockGo;

    public void SetLoadingLockScreenState(bool locked)
    {

        if (RewardedLoadingLockGo != null)
            RewardedLoadingLockGo.SetActive(locked);
    }
}