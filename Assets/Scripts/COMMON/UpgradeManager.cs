﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour {
    public static UpgradeManager Instance;

    const string Key_CurrentXPLevel = "Exp_CurrentLevel";
    const string Key_CurrentCash = "Key_CurrentCash";

    public int CurrentXPLevel {
        get {
            return PlayerPrefs.GetInt (Key_CurrentXPLevel, 0);
        }
    }

    /*private int currentCash;

    public int CurrentCash
    {
        get
        {
           

                currentCash = PlayerPrefs.GetInt(Key_CurrentCash, 0);
            return currentCash;
        }
        set
        {
            currentCash = value;
            PlayerPrefs.SetInt(Key_CurrentCash, currentCash);
        }
    }*/

    void Awake () {

        Instance = this;
        //	Normal_ButtonGO.SetActive (false);
        //	Perfect_ButtonGO.SetActive (false);
        LevelUpTextGO.SetActive (false);

        if (LevelUpParticles != null)
            LevelUpParticles.Stop ();

    }
    // Use this for initialization
    void Start () {
        /*
		 * 
		Debug.LogError ("!!! DELETING EXPERIENCE !!! ");
		Debug.LogError ("!!! DELETING EXPERIENCE !!! ");
		Debug.LogError ("!!! DELETING EXPERIENCE !!! ");
		PlayerPrefs.DeleteKey (Key_CurrentLevel);
		PlayerPrefs.DeleteKey (Key_CurrentProgress);
*/

        ShowOfflineReward ();

        currentGameNumber = PlayerPrefs.GetInt ("CurrentGameNumber", 0);
        TryToDisplayLimited ();
        TryToDisplayRatingGamePanel ();
    }

    // Update is called once per frame
    void Update () {
#if UNITY_EDITOR
        if (Input.GetKeyDown (KeyCode.P)) {
            MoneyManager.Instance.AddCash (1);
        }
        if (Input.GetKeyDown (KeyCode.L)) {
            LevelUpAnimation ();
        }

#endif
    }

    private int ltprogress = -1;
    int oldcash;
    int neocash;
    public Color AddCashColor = Color.green;
    public Color RemoveCashColor = Color.red;
    public float CashUpdateAnimationDuration = 1f;

    /*
    public void AddCash(int n)
    {
        AddCash(n, true);
    }

    public void AddCash(int n, bool instant)
    {
        if (n <= 0)
            return;
        oldcash = CurrentCash;
        neocash = CurrentCash + n;
        CurrentCash = neocash;
        PlayerPrefs.SetInt(Key_CurrentCash, neocash);

        // Show ADD CASH text
        AddCashValueDisplayer.text = "+$" + n.ToString();
        AddCashValueDisplayer.color = AddCashColor;
        AddCashValueDisplayer.transform.localScale = Vector3.zero;
        if (instant)
        {
            CurrentCashDisplayer.text = " <size=100>$</size>" + ((int)CurrentCash).ToString();// + "/" + maxProgress.ToString ("00");

        }
        else
        {
            LeanTween.cancel(CurrentCashDisplayer.gameObject);
            LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.one, 0.15f);

            LeanTween.color(CurrentCashDisplayer.gameObject, new Color(1f, 0.8f, 0f), 0.15f);
            LeanTween.scale(CurrentCashDisplayer.gameObject, Vector3.one * 1.25f, 0.15f).setEase(LeanTweenType.easeOutExpo);
            LeanTween.value(CurrentCashDisplayer.gameObject, updateCashDisplay, oldcash, neocash, CashUpdateAnimationDuration).setEase(LeanTweenType.easeOutExpo).setOnComplete(c =>
            {
                LeanTween.scale(CurrentCashDisplayer.gameObject, Vector3.one, 0.15f).setEase(LeanTweenType.easeInOutBack);
                LeanTween.color(CurrentCashDisplayer.gameObject, new Color(1f, 1f, 1f), 0.15f);
                LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.zero, 0.15f);
                LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.zero, 0.15f);
            });
        }


    }

    public void RemoveCash(int n)
    {
        if (n > currentCash)
        {
            Debug.LogError("ERror : not enough cash to remove ");
            return;
        }

        AddCashValueDisplayer.color = RemoveCashColor;

        AddCashValueDisplayer.text = "-$" + n.ToString();

        oldcash = CurrentCash;
        neocash = CurrentCash - n;
        CurrentCash = neocash;

        PlayerPrefs.SetInt(Key_CurrentCash, neocash);

        LeanTween.cancel(CurrentCashDisplayer.gameObject);
        LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.one, 0.15f);

        LeanTween.color(CurrentCashDisplayer.gameObject, new Color(1f, 0f, 0f), 0.15f);
        LeanTween.scale(CurrentCashDisplayer.gameObject, Vector3.one * 1.25f, 0.15f).setEase(LeanTweenType.easeOutExpo);
        LeanTween.value(CurrentCashDisplayer.gameObject, updateCashDisplay, oldcash, neocash, CashUpdateAnimationDuration).setEase(LeanTweenType.easeOutExpo).setOnComplete(c =>
        {
            LeanTween.scale(CurrentCashDisplayer.gameObject, Vector3.one, 0.15f).setEase(LeanTweenType.easeInOutBack);
            LeanTween.color(CurrentCashDisplayer.gameObject, new Color(1f, 1f, 1f), 0.15f);
            LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.zero, 0.15f);
            LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.zero, 0.15f);
        });


    }


    void updateCashDisplay(float i)
    {
        if ((int)i != oldcash)
        {
            //AddCashValueDisplayer.text = ((int)neocash - i).ToString ("0") + " <size=100>$</size>";// + "/" + maxProgress.ToString ("00");

            oldcash = (int)i;
            CurrentCashDisplayer.text = "<size=100>$</size>" + ((int)i).ToString();// + "/" + maxProgress.ToString ("00");
            if (!CashTickAudioSource.isPlaying && UserConfig.SoundFX)
                CashTickAudioSource.Play();
            /*	LeanTween.scale (CurrentCashDisplayer.gameObject, Vector3.one * 1.05f, 0.025f).setEase (LeanTweenType.easeOutExpo).setOnComplete (c => {
				LeanTween.scale (CurrentCashDisplayer.gameObject, Vector3.one, 0.025f).setEase (LeanTweenType.easeInOutBack);
			});


        }

    }*/

    public ParticleSystem LevelUpParticles;
    public GameObject LevelUpTextGO;

    public void LevelUpAnimation () {
        int currentLevel = CurrentXPLevel;
        double currentProgress = MoneyManager.Instance.CurrentMoney;

        if (LevelUpParticles != null)
            LevelUpParticles.Play ();

        LevelUpTextGO.transform.localScale = Vector3.zero;
        LevelUpTextGO.SetActive (true);
        LeanTween.scale (LevelUpTextGO, Vector3.one, 0.25f).setEase (LeanTweenType.easeOutBack);
        if (SoundManager.Instance != null)
            SoundManager.Instance.Play (SoundManager.Instance.BuyUpgrade);
        Invoke ("levelupanimPart2", 1f);
        GameAnalyticsSDK.GameAnalytics.NewProgressionEvent (GameAnalyticsSDK.GAProgressionStatus.Complete, "XP_Level_" + currentLevel.ToString ("00"));

    }

    void levelupanimPart2 () {
        LeanTween.cancel (ltprogress);
        LeanTween.scale (LevelUpTextGO, Vector3.zero, 0.25f).setEase (LeanTweenType.easeOutBack);
        if (LevelUpParticles != null)
            LevelUpParticles.Stop ();
        //Initialize();
    }

    public bool shopActive = true;
    public float ShopAnimationTime = 0.5f;
    public LeanTweenType ShopAnimationEaseHide;
    public LeanTweenType ShopAnimationEaseShow;
    float shopDefaultY;
    float topDefaulty;

    [Header ("Rate the Game Pop-up")]
    public int ratingLevelFrequencyDisplaying = 5;
    public GameRating gameRating;

    void TryToDisplayRatingGamePanel () {

        if (gameRating != null) {
            gameRating.DisplayRatingPanel ();
        } else {
            Debug.LogError ("No game rating found");
        }
    }

    [Header ("Limited Reward")]
    public Vector2 randomLimitedFrequencyAmplitude;
    public int randomLimitedCashFrequencyAmplitude;
    public int randomLimitedBoostFrequencyAmplitude;
    public int randomStartingLotteryFrequencyAmplitude;
    public int currentGameNumber;

    public GameObject startingPrizeWheelContinueButton;

    public void TryToDisplayLimited () {
        // try to display limited cash reward popup
        if (currentGameNumber >= Random.Range (randomLimitedFrequencyAmplitude.x, randomLimitedFrequencyAmplitude.y)) {
            int randomPercentage = Random.Range (0, (randomLimitedBoostFrequencyAmplitude + randomLimitedCashFrequencyAmplitude + randomStartingLotteryFrequencyAmplitude));

            if (randomPercentage <= randomLimitedCashFrequencyAmplitude) {
                DisplayLimitedReward ();
            } else if (randomPercentage > randomLimitedCashFrequencyAmplitude && randomPercentage <= (randomLimitedCashFrequencyAmplitude + randomLimitedBoostFrequencyAmplitude)) {
                DisplayLimitedBoostOffer ();
            } else {
                DisplayStartingPrizeWheelScreen ();
            }
        }

        currentGameNumber++;
    }

    public void DisplayLimitedReward () {
        if (MonetizationManager.Instance != null && MonetizationManager.Instance.isRewardedAdAvailable) {
            UIController.Instance.ShowPanel (8, false);
            currentGameNumber = 0;
        }
    }

    public void DisplayLimitedBoostOffer () {
        if (MonetizationManager.Instance != null && MonetizationManager.Instance.isRewardedAdAvailable) {
            UIController.Instance.ShowPanel (9, false);
            currentGameNumber = 0;
        }
    }

    public void DisplayStartingPrizeWheelScreen () {
        if (MonetizationManager.Instance != null && MonetizationManager.Instance.isRewardedAdAvailable) {
            UIController.Instance.ShowPanel (10, false);
            currentGameNumber = 0;
            startingPrizeWheelContinueButton.SetActive (true);
        }
    }

    public void HideStartingPrizeWheel () {
        UIController.Instance.HidePanel (10);
        startingPrizeWheelContinueButton.SetActive (false);
    }

    [Header ("Offline Reward")]
    public Upgrade OfflineRewardUpgrade;

    public Text OfflineRewardText;
    public ParticleSystem CollectParticles;

    public GameObject multiplyOfflineRewardButton;
    public float BaseCashPerSecond;

    public float BaseMaxOffineCashTier1 = 100f;
    public float LevelCashPerSecondMultiplier = 1.5f;
    public float LevelMaxOfflineCashMultiplier = 1.5f;
    public float maxSeconds;

    public float CashPerSecond () {
        return OfflineRewardUpgrade.CurrentLevel * LevelCashPerSecondMultiplier * BaseCashPerSecond;
    }

    public int SecondsUntilReward {
        get {
            int sec = OfflineReward.Instance.SecondsSinceLastCollection;
            int res = (60 * 60) - sec;
            if (res <= 0)
                return 0;
            else
                return res;
        }
    }

    public int CurrentReward {
        get {
            int currentReward = (int) (OfflineReward.Instance.SecondsSinceLastCollection * CashPerSecond ());
            int maxReward = (int) (CashPerSecond () * maxSeconds);
            int res = Mathf.Clamp (currentReward, 1, maxReward);
            return res;

            /*int maxSeconds = 60 * 60;
			int sec = OfflineReward.Instance.SecondsSinceLastCollection;

			int maxReward = OfflineRewardUpgrade.CurrentUpgradeCost;

			//	float t = Mathf.Clamp01 ((float)sec / (float)maxSeconds);
			//	int res = (int)((float)maxReward * t);
			if (SecondsUntilReward > 0)
				return 0;
			else
				return maxReward;
			*/
        }
    }

    bool collected = false;

    public void ShowOfflineReward () {

        if (OfflineReward.Instance == null || Player.Instance.cruisePhase || LevelManager.Instance.CurrentLevel < 1 || OfflineRewardUpgrade.CurrentLevel < 1) {
            HideOfflineReward ();
            return;
        }

        if (OfflineReward.Instance.SecondsSinceLastCollection < 600) {
            //GameManager.Instance.DisplayIntroPanel();
            HideOfflineReward ();
            return;
            //Debug.LogError ("no reward : ");
            //OfflineRewardRemainingDisplayerGO.SetActive (true);
            //InvokeRepeating ("updateRemainingOffline", 0f, 0.25f);

        } else {
            //Debug.LogError (" reward : ");

            collected = false;
            OfflineRewardText.text = "<size=150> $</size>" + ValueConvertor.ConvertDoubleToString (CurrentReward);
            UIController.Instance.ShowPanel (5, false);

        }

    }

    public void HideOfflineReward () {
        UIController.Instance.HidePanelInstantly (5);
    }

    /*void  updateRemainingOffline ()
	{
		
		int min = SecondsUntilReward / 60;
		int sec = SecondsUntilReward % 60;
		if (min > 0)
			OfflineRewardRemainingTimeDisplayer.text = min.ToString () + "<size=100>min</size>" + sec.ToString () + "<size=100>s</size>";
		else
			OfflineRewardRemainingTimeDisplayer.text = sec.ToString () + "<size=100>s</size>";

		if (SecondsUntilReward == 0) {
			CancelInvoke ("updateRemainingOffline");
			ShowOfflineReward ();
		}
		if (!Player.Instance.introPhase) {
			CancelInvoke ("updateRemainingOffline");
			return;
		}
	}*/

    public void CollectOfflineReward (bool rewardedValidated) {
        if (collected)
            return;
        collected = true;
        SoundManager.Instance.Play (SoundManager.Instance.CollectOfflineCash);

        if (rewardedValidated) {
            MoneyManager.Instance.AddCash (CurrentReward * 4, false);
        } else {
            MoneyManager.Instance.AddCash (CurrentReward, false);
        }

        OfflineReward.Instance.CollectReward ();
        CollectParticles.Play ();

        HideOfflineReward ();

        //OfflineRewardScreenGO.SetActive (false);

    }

    /*	void updateOfflineRewardDisplayer (float f)
        {
            OfflineRewardDisplayer.text = f.ToString ("0") + "<size=100> $</size>";

        }
        */

    public void DEBUG_AddMaxCash () {
        MoneyManager.Instance.AddCash ((CashPerSecond () * maxSeconds));
        Debug.Log ("ADDED : " + ((int) (CashPerSecond () * maxSeconds)).ToString ());
    }

    [ContextMenu ("V2 Add 1 hour reward")]
    public void DEBUG_Add1HoursReward () {

        MoneyManager.Instance.AddCash ((CashPerSecond () * 360));
        Debug.Log ("ADDED : " + ((int) (CashPerSecond () * 360)).ToString ());

    }

    void OnApplicationFocus (bool focused) {
        if (focused) {
            ShowOfflineReward ();
        }
    }

    private void OnDestroy () {
        PlayerPrefs.SetInt ("CurrentGameNumber", currentGameNumber);
    }

}