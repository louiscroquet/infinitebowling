﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFeedback : MonoBehaviour
{
    public static CameraFeedback Instance;

    void Awake()
    {
        Instance = this;
        defaultLocalPosition = transform.localPosition;
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.S))
        {
            ScreenshakeSoft();
        }
#endif
    }

    [Header("Screenshake Settings")]
    public Vector3 SoftAmplitude;
    public float SoftDurationIn = 0.05f;
    public float SoftDurationOut = 0.05f;
    public float SoftDelayOut = 0.05f;
    //public int SoftLoops = 1;
    public LeanTweenType SoftEaseIn;
    public LeanTweenType SoftEaseOut;
    private Vector3 defaultLocalPosition;

    public void ScreenshakeSoft()
    {
        LeanTween.cancel(gameObject);
        transform.localPosition = defaultLocalPosition;
        LeanTween.moveLocal(gameObject, defaultLocalPosition + SoftAmplitude, SoftDurationIn).setEase(SoftEaseIn).setOnComplete(c =>
        {
            LeanTween.moveLocal(gameObject, defaultLocalPosition, SoftDurationOut).setEase(SoftEaseOut).setDelay(SoftDelayOut);
        });
    }


    public Vector3 HardAmplitude;
    public float HardDuration = 0.05f;
    public int HardLoops = 3;
    //public int HardLoops = 1;
    public LeanTweenType HardEaseIn;
    public LeanTweenType HardEaseOut;

    public void ScreenshakeHard()
    {
        LeanTween.cancel(gameObject);
        transform.localPosition = defaultLocalPosition;
        LeanTween.moveLocal(gameObject, defaultLocalPosition + HardAmplitude, HardDuration).setLoopPingPong(HardLoops).setEase(HardEaseIn).setOnComplete(c =>
        {
            //LeanTween.moveLocal (gameObject, defaultLocalPosition, HardDuration).setEase (HardEaseOut);
        });
    }


    public Vector3 RambleAmplitude;
    public float RambleDuration = 0.1f;
    public int RambleLoops = 5;
    //public int RambleLoops = 1;
    public LeanTweenType RambleEaseIn;
    public LeanTweenType RambleEaseOut;

    public void ScreenRamble()
    {
        LeanTween.cancel(gameObject);
        transform.localPosition = defaultLocalPosition;
        LeanTween.moveLocal(gameObject, defaultLocalPosition + RambleAmplitude, RambleDuration).setLoopPingPong(RambleLoops).setEase(RambleEaseIn).setOnComplete(c =>
        {
            //LeanTween.moveLocal (gameObject, defaultLocalPosition, HardDuration).setEase (HardEaseOut);
        });
    }
}
