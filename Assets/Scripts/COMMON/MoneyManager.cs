﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyManager : MonoBehaviour {

    public static MoneyManager Instance;

    const string Key_CurrentMoney = "Key_CurrentMoney";



    public Text CurrentCashDisplayer;
    public Text AddCashValueDisplayer;
    public GameObject LevelUpFeedbackGO;

    public AudioSource CashTickAudioSource;


    private double m_currentMoney;

    public double CurrentMoney
    {
        get
        {
            int oldCash = PlayerPrefs.GetInt("Key_CurrentCash", 0);

            // si le joeuur avait de la thune en int, on le transvase dans le nouveau système de cash
            if (oldCash != 0)
            {
                m_currentMoney = oldCash; //double.Parse(oldCash.ToString());
                PlayerPrefs.SetInt("Key_CurrentCash", 0);
            }
            else
            {
                string currentMoneyString = PlayerPrefs.GetString(Key_CurrentMoney, "0");
                m_currentMoney = double.Parse(currentMoneyString);
            }            
            return m_currentMoney;
        }
        set
        {
            m_currentMoney = value;
            PlayerPrefs.SetString(Key_CurrentMoney, m_currentMoney.ToString("0"));
        }
    }




    void Awake()
    {
        AddCashValueDisplayer.transform.localScale = Vector3.zero;

        Instance = this;

        

        Initialize();

    }
    // Use this for initialization
    void Start()
    {
        /*
		 * 
		Debug.LogError ("!!! DELETING MONEY !!! ");
		PlayerPrefs.DeleteKey (Key_CurrentMoney);
*/
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.P))
        {
            AddCash(1);
        }
#endif
    }

    void Initialize()
    {
        CurrentCashDisplayer.text = " <size=100>$</size>" + ValueConvertor.ConvertDoubleToString(CurrentMoney);


    }

    private int ltprogress = -1;
    private double oldcash;
    private double neocash;
    public Color AddCashColor = Color.green;
    public Color RemoveCashColor = Color.red;
    public float CashUpdateAnimationDuration = 1f;

    public void AddCash(double n)
    {
        AddCash(n, true);
    }

    public void AddCash(double n, bool instant)
    {
        if (n <= 0)
            return;
        oldcash = CurrentMoney;
        neocash = CurrentMoney + n;
        CurrentMoney = neocash;
        PlayerPrefs.SetString(Key_CurrentMoney, neocash.ToString());

        // Show ADD CASH text
        AddCashValueDisplayer.text = "+$" + n.ToString();
        AddCashValueDisplayer.color = AddCashColor;
        AddCashValueDisplayer.transform.localScale = Vector3.zero;
        if (instant)
        {
            CurrentCashDisplayer.text = " <size=100>$</size>" + ValueConvertor.ConvertDoubleToString(CurrentMoney);// + "/" + maxProgress.ToString ("00");

        }
        else
        {
            LeanTween.cancel(CurrentCashDisplayer.gameObject);
            LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.one, 0.15f);

            LeanTween.color(CurrentCashDisplayer.gameObject, new Color(1f, 0.8f, 0f), 0.15f);
            LeanTween.scale(CurrentCashDisplayer.gameObject, Vector3.one * 1.25f, 0.15f).setEase(LeanTweenType.easeOutExpo);
            LeanTween.value(CurrentCashDisplayer.gameObject, updateCashDisplay, (float)oldcash, (float)neocash, CashUpdateAnimationDuration).setEase(LeanTweenType.easeOutExpo).setOnComplete(c =>
            {
                LeanTween.scale(CurrentCashDisplayer.gameObject, Vector3.one, 0.15f).setEase(LeanTweenType.easeInOutBack);
                LeanTween.color(CurrentCashDisplayer.gameObject, new Color(1f, 1f, 1f), 0.15f);
                LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.zero, 0.15f);
                LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.zero, 0.15f);
                CurrentCashDisplayer.text = "<size=100>$</size>" + ValueConvertor.ConvertDoubleToString(CurrentMoney); ;
            });
        }


    }

    public void RemoveCash(int n)
    {
        if (n > CurrentMoney)
        {
            Debug.LogError("Error : not enough cash to remove ");
            return;
        }

        AddCashValueDisplayer.color = RemoveCashColor;

        AddCashValueDisplayer.text = "-$" + n.ToString();

        oldcash = CurrentMoney;
        neocash = CurrentMoney - n;
        CurrentMoney = neocash;

        PlayerPrefs.SetString(Key_CurrentMoney, CurrentMoney.ToString());

        LeanTween.cancel(CurrentCashDisplayer.gameObject);
        LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.one, 0.15f);

        LeanTween.color(CurrentCashDisplayer.gameObject, new Color(1f, 0f, 0f), 0.15f);
        LeanTween.scale(CurrentCashDisplayer.gameObject, Vector3.one * 1.25f, 0.15f).setEase(LeanTweenType.easeOutExpo);
        LeanTween.value(CurrentCashDisplayer.gameObject, updateCashDisplay, (float)oldcash, (float)neocash, CashUpdateAnimationDuration).setEase(LeanTweenType.easeOutExpo).setOnComplete(c =>
        {
            LeanTween.scale(CurrentCashDisplayer.gameObject, Vector3.one, 0.15f).setEase(LeanTweenType.easeInOutBack);
            LeanTween.color(CurrentCashDisplayer.gameObject, new Color(1f, 1f, 1f), 0.15f);
            LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.zero, 0.15f);
            LeanTween.scale(AddCashValueDisplayer.gameObject, Vector3.zero, 0.15f);
            CurrentCashDisplayer.text = " <size=100>$</size>" + ValueConvertor.ConvertDoubleToString(CurrentMoney);
        });


    }


    void updateCashDisplay(float i)
    {
        if ((int)i != oldcash)
        {
            //AddCashValueDisplayer.text = ((int)neocash - i).ToString ("0") + " <size=100>$</size>";// + "/" + maxProgress.ToString ("00");

            oldcash = (int)i;
            CurrentCashDisplayer.text = "<size=100>$</size>" + ((int)i).ToString();// + "/" + maxProgress.ToString ("00");
            if (!CashTickAudioSource.isPlaying && UserConfig.SoundFX)
                CashTickAudioSource.Play();
            /*	LeanTween.scale (CurrentCashDisplayer.gameObject, Vector3.one * 1.05f, 0.025f).setEase (LeanTweenType.easeOutExpo).setOnComplete (c => {
				LeanTween.scale (CurrentCashDisplayer.gameObject, Vector3.one, 0.025f).setEase (LeanTweenType.easeInOutBack);
			});

*/
        }

    }

}
