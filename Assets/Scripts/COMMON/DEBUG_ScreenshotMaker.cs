﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUG_ScreenshotMaker : MonoBehaviour
{
	public string screenshotName;
	public int currentScreenshot;
	public int superSizeScreenshot;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		#if UNITY_EDITOR
		if (Input.GetKeyDown (KeyCode.A)) {
			TakeScreenshot ();
		}
		#endif

	}

	[ContextMenu ("Take Screenshot")]
	public void TakeScreenshot ()
	{
		//PerformanceManager.Instance.enabled = false;
		ScreenCapture.CaptureScreenshot ("Screenshot_" + screenshotName + "_" + currentScreenshot.ToString ()+ System.DateTime.Now.Minute + System.DateTime.Now.Second+ ".png", superSizeScreenshot);
		currentScreenshot++;
		Debug.Log ("Screenshot captured");
	}
}
