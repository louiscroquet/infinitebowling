﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class OfflineManager : MonoBehaviour
{
	public static OfflineManager Instance;
	public DateTime LastRewardCollectionTime;

	public int MinutesSinceLastReward {
		get {
			TimeSpan t = DateTime.Now - LastRewardCollectionTime;
			return (int)t.TotalMinutes;
		}
	}

	void Awake ()
	{
		Instance = this;
		LoadDailyRewardInfo ();
	}

	void Start ()
	{
		
	}


	public void CollectReward ()
	{

		LastRewardCollectionTime = DateTime.Now;

		SaveDailyRewardInfo ();

		//	NotificationManager.ScheduleNotification (LanguageManager.LocalizedText_MagicChestNotificationTicker, LanguageManager.LocalizedText_MagicChestNotificationText, GameConstants.MagicChestMinutesCooldown * 60);

	

	}







	#region SAVE/LOAD

	const string saveFilename = "dr.sec";

	public string GetSaveFilePath ()
	{
		return Path.Combine (Application.persistentDataPath, saveFilename);
	}

	public void SaveDailyRewardInfo ()
	{
		BinaryFormatter binary = new BinaryFormatter ();
		FileStream fStream = File.Create (GetSaveFilePath ());

		bool success = true;
		string message = "";

		try {
			binary.Serialize (fStream, LastRewardCollectionTime);
			message = "Saved file to : " + GetSaveFilePath ();
			//	Debug.Log (message);

		} catch (Exception e) {
			success = false;
			message = "Unable to serialize Daily Reward: " + e.ToString ();
			//Debug.Log (message);
		}
		fStream.Close ();



	}

	public void LoadDailyRewardInfo ()
	{

		bool success = true;
		string message = "";
		if (File.Exists (GetSaveFilePath ())) {
			BinaryFormatter binary = new BinaryFormatter ();
			FileStream fStream = File.Open (GetSaveFilePath (), FileMode.Open);
			try {
				LastRewardCollectionTime = (DateTime)binary.Deserialize (fStream);
				message = "Loaded file from : " + GetSaveFilePath ();
				//Debug.Log (message);

			} catch (Exception e) {
				success = false;
				message = "Unable to deserialize Daily infos: " + e.ToString ();
				Debug.Log (message);
			}

			fStream.Close ();

		} else {
			success = false;
			message = "No file to load at path : " + GetSaveFilePath ();
			Debug.LogWarning (message);
			LastRewardCollectionTime = DateTime.Now;
			SaveDailyRewardInfo ();
		}
	
	}


	#endregion
}
