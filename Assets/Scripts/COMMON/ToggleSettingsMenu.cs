﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ToggleSettingsMenu : MonoBehaviour {
    public Image MenuIcon;
    public Color IconColorOPEN;
    private Color IconColorCLOSED;
    public float ClosedOffset = 200F;

    public GameObject MENU_GO;
    private void Awake () {
        IconColorCLOSED = MenuIcon.color;
        LeanTween.cancel (gameObject);
        LeanTween.moveLocalX (MENU_GO, ClosedOffset, 0f);

    }
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    bool MenuOpen = false;
    public void Toggle () {
        if (MenuOpen)
            Close ();
        else
            Open ();
    }

    public void Open () {
        if (MenuOpen) return;
        VibrationManager.Instance.VibrateMedium ();
        MenuOpen = true;
        LeanTween.cancel (gameObject);
        LeanTween.cancel (MenuIcon.gameObject);

        LeanTween.moveLocalX (MENU_GO, 0F, 0.35f).setEase (LeanTweenType.easeOutBack);
        LeanTween.rotateLocal (MenuIcon.gameObject, new Vector3 (0f, 0f, 90F), 0.35f).setEase (LeanTweenType.easeOutBack);
        MenuIcon.color = IconColorOPEN;
    }
    public void Close () {
        if (!MenuOpen) return;
        VibrationManager.Instance.VibrateLight ();

        MenuOpen = false;

        LeanTween.cancel (gameObject);
        LeanTween.cancel (MenuIcon.gameObject);

        LeanTween.moveLocalX (MENU_GO, ClosedOffset, 0.25f).setEase (LeanTweenType.easeInOutCubic);
        LeanTween.rotateLocal (MenuIcon.gameObject, new Vector3 (0f, 0f, 0F), 0.35f).setEase (LeanTweenType.easeOutBack);

        MenuIcon.color = IconColorCLOSED;
    }

}