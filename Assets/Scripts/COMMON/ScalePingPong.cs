﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalePingPong : MonoBehaviour {

    public Vector3 scaleUpSize;
    public float scaleUpTime;

    public LeanTweenType easeType;

	// Use this for initialization
	void Start ()
    {
        LeanTween.cancel(gameObject);
        transform.localScale = Vector3.one;
        LeanTween.scale(gameObject, scaleUpSize, scaleUpTime).setEase(easeType).setLoopPingPong(0);
	}
	
	
}
