﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using Facebook.Unity;
public class SocialManager : MonoBehaviour
{

    public static SocialManager Instance;

    [Header("AppsFlyer Settings")]
    public bool EnableAppsFlyer = false;
    public string AppsFlyerDevKey = "2vY2LevtNQYXpnQHZHsJVk";
    [Tooltip("Only the numbers, not the ID letters")]
    public string AppsFlyerAppId = "1355834030";

    void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        GameAnalytics.Initialize();

        Instance = this;
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(this);

        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    // Use this for initialization
    void Start()
    {
        if (EnableAppsFlyer) {
            InitializeAppsFlyer();
        }
    }

    private void InitializeAppsFlyer() {
        /* Mandatory - set your AppsFlyer’s Developer key. */
        AppsFlyer.setAppsFlyerKey("2vY2LevtNQYXpnQHZHsJVk");
        /* For detailed logging */
        AppsFlyer.setIsDebug(true);
#if UNITY_IOS
        /* Mandatory - set your apple app ID
         NOTE: You should enter the number only and not the "ID" prefix */
        AppsFlyer.setAppID (AppsFlyerAppId);
        AppsFlyer.trackAppLaunch ();
#elif UNITY_ANDROID
        /* Mandatory - set your Android package name */
        AppsFlyer.setAppID(Application.identifier);
        /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
        AppsFlyer.init(AppsFlyerDevKey, "AppsFlyerTrackerCallbacks");
#endif
    }


    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Debug.LogError("Paused game");
            // Pause the game - we will need to hide
            Time.timeScale = 0f;
        }
        else
        {
            Debug.LogError("Resumed game");

            // Resume the game - we're getting focus again
            Time.timeScale = 1f;
        }
    }
}
