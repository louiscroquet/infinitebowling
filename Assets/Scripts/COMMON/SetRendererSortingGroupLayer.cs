﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRendererSortingGroupLayer : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public string LayerName;

	[ContextMenu ("Apply Layer Name")]
	public void SetLayerSortingGroup ()
	{
		var mr = GetComponent<MeshRenderer> ();
		if (mr == null) {
			Debug.LogError ("No Meshrenderer attached : " + name);
			return;
		} else {
			mr.sortingLayerName = LayerName;
			
		}
	}
}
