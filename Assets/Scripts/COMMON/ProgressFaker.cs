﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressFaker : MonoBehaviour
{
	public static ProgressFaker Instance;
	public Text LevelDisplayer;

	public Text DifficultyDisplayer;
	public Gradient DifficultyGradient;
	public float PercentageAnimationDuration = 1.5f;
	public LeanTweenType PercentageAnimationEase = LeanTweenType.easeOutExpo;

	private int currentLevel = 1;
	private float currentDiffulty = 100f;

	public int CurrentLevel {
		get {
			return currentLevel;
		}
	}


	public float CurrentSuccessPercentage {
		get {
			return currentDiffulty;
		}
	}

	void Awake ()
	{
		currentLevel =	PlayerPrefs.GetInt ("CurrentLevel", 1);

		currentDiffulty = PlayerPrefs.GetFloat ("CurrentDifficulty", currentDiffulty);
		Instance = this;
	}
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void SaveProgress ()
	{
		PlayerPrefs.SetInt ("CurrentLevel", currentLevel);
		PlayerPrefs.SetFloat ("CurrentDifficulty", currentDiffulty);
	
	}

	public void NextLevel ()
	{
		currentLevel++;
		lastDiff = currentDiffulty;	
		currentDiffulty *= Random.Range (0.95f, 0.99f);
		SaveProgress ();

	}

	private int introlt = -1;
	private float targetDiff;
	private float lastDiff;
	private float lasttarget;

	public void UpdatePercentageAnimation ()
	{
		targetDiff = currentDiffulty;
		introlt = LeanTween.value (gameObject, updateIntroDiff, lastDiff, targetDiff, 1.5f).setEase (PercentageAnimationEase).id;

	}

	void updateIntroDiff (float t)
	{
		if ((int)(t * 10f) != (int)(10f * lasttarget)) {
			//SoundManager.Instance.Play (TickSound);
			lasttarget = t;
		}	

		// GERER ICI LE TEXTE A MODIFIER POUR L'ANIMATION
		/* EXEMPLES 
		 * IntroDifficulty.text = t.ToString ("00.0") + "%";
		 * IntroDifficulty.color = GetColorForDifficulty (t);
*/
	}
}
