﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureManager : MonoBehaviour {

    public static CaptureManager Instance;

    // THEME
    public LevelTheme captureTheme;
    

    // BALL
    public Texture captureBallTexture;
    public Color captureBallColor;
    public GameObject captureTrail;

    public ParticleSystem pinExplosionParticles;


    public float overrideCurrentSpawnZ = 150f;

    public bool disableUi;
    public bool disableScoreTexts;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        
    }

    public void InitializeCaptureTheme()
    {

        LevelManager.Instance.ApplyTheme(captureTheme);

        if(captureBallTexture != null)
        {
            Player.Instance.ballRenderer.material.mainTexture = captureBallTexture;
            Player.Instance.ballRenderer.material.color = Color.white;
        }
        else
        {
            Player.Instance.ballRenderer.material.color = captureBallColor;
        }

        captureTrail.SetActive(true);
        

        if (disableUi)
        {
            DisableUI();
        }
    }

    public void DisableUI()
    {
        foreach (var screen in UIController.Instance.UIPanels)
        {
            screen.alpha = 0f;
        }
    }

    // 
}
