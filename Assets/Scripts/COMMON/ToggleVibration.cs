﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleVibration : MonoBehaviour
{
	public Image ButtonImage;
	public Sprite ON;
	public Sprite OFF;

	public void Toggle ()
	{
		UserConfig.SetVibration (!UserConfig.Vibration);
		if (UserConfig.Vibration) {
			ButtonImage.sprite = ON;
            VibrationManager.Instance.VibrateMedium();
		} else
			ButtonImage.sprite = OFF;
	}
}
