﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameAnalyticsSDK;

public class Upgrade : MonoBehaviour
{
	public static List<string> UpgradeNamesUniquenessCheck = new List<string> ();

	string Key_UpgradeName {
		get {
			return "Key_" + UpgradeName;
		}
	}

	[Header ("Public References")]
	public GameObject ButtonGO;
	public Image ButtonBackgroundImage;
	public Text UpradeCostDisplayer;
	public Text CurrentLevelDisplayer;

	//public Text NextLevelDisplayer;
	public Text UpgradeNameDisplayer;
	public ParticleSystem BuyParticles;


	[Header ("Upgrade Settings")]
	public string UpgradeName;
	//public float BaseCost = 50f;
	//public float LevelMultiplier = 1.5f;
	public float PercentageIncreasePerLevel;
	public UnityEngine.Events.UnityEvent CallOnUpgrade;

	public List<int> PriceMap = new List<int> ();
	public int MaxPriceBumpPerLevel;

	// Cach values
	protected Color defaultButtoncolor;
	protected int m_CurrentLevel = 0;
	protected float localXNormal;



	public int CurrentLevel {
		get {
			return m_CurrentLevel;
		}
	}

	
	public float CurrentPercentage {
		get {
			return 100f + CurrentLevel * PercentageIncreasePerLevel;
		}
	}



	public int CurrentUpgradeCost {
		get {
			int res;
			if (m_CurrentLevel < PriceMap.Count) {
				return PriceMap [m_CurrentLevel];
			} else {
				int over = m_CurrentLevel - PriceMap.Count + 1;
				return PriceMap [PriceMap.Count - 1] + over * MaxPriceBumpPerLevel;
			}
			/*
			float res = BaseCost;
			for (int i = 0; i < m_CurrentLevel; i++) {
				res *= LevelMultiplier;
			}

			return(int)res;
*/
		}
	}


	void Awake ()
	{
		m_CurrentLevel = PlayerPrefs.GetInt (Key_UpgradeName, 0);

		UpgradeNamesUniquenessCheck.Clear ();

	}

	void Start ()
	{
		if (UpgradeNamesUniquenessCheck.Contains (UpgradeName)) {
			Debug.LogError ("Two upgrades have the same name : " + UpgradeName);
		} else {
			UpgradeNamesUniquenessCheck.Add (UpgradeName);
		}
		localXNormal = ButtonGO.transform.localPosition.x;
		UpdateButtonState ();
	}

	protected virtual  void UpdateButtonState ()
	{
		//	Debug.LogError ("TODO : Load current level, Check unique upgrade name, display price, display name, display level etc");
		if (UpgradeNameDisplayer != null)
			UpgradeNameDisplayer.text = UpgradeName.ToString ();
		if (UpradeCostDisplayer != null)
			UpradeCostDisplayer.text = "$" + ValueConvertor.ConvertDoubleToString(CurrentUpgradeCost);
		if (CurrentLevelDisplayer != null)
			CurrentLevelDisplayer.text = (CurrentPercentage).ToString () + "<size=50>%</size>";
		//	if (NextLevelDisplayer != null)
		//		NextLevelDisplayer.text = "Lv" + (CurrentLevel + 2).ToString ();

	}

	public virtual void TryUpgrade ()
	{
		if (MoneyManager.Instance.CurrentMoney >= CurrentUpgradeCost) {
			ValidateUpgrade ();
            //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.Success);
            VibrationManager.Instance.VibrateSuccess();

		} else {
            //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.Failure);
            VibrationManager.Instance.VibrateWarning();
			FailedUpgradeAnimation ();
			return;
		}

	}

	public virtual void ValidateUpgrade ()
	{
		
		int upgradeCost = CurrentUpgradeCost;
		MoneyManager.Instance.RemoveCash (upgradeCost);
	
		LeanTween.cancel (ButtonGO);
		LeanTween.moveLocalX (ButtonGO, localXNormal, 0.01f);
		ButtonGO.transform.localScale = Vector3.one;

		LeanTween.scale (ButtonGO, Vector3.one * 1.25f, 0.1f).setEase (LeanTweenType.easeInOutSine).setLoopPingPong (1);

		if (BuyParticles != null)
			BuyParticles.Play ();
		m_CurrentLevel++;
		PlayerPrefs.SetInt (Key_UpgradeName, m_CurrentLevel);
		PlayerPrefs.Save ();
		UpdateButtonState ();
		UpgradeManager.Instance.LevelUpAnimation ();
		if (CallOnUpgrade != null) {
			CallOnUpgrade
				.Invoke ();
		}

        GameAnalytics.NewDesignEvent("Upgrade:" + transform.name + ":To level " + m_CurrentLevel.ToString());
        //MonetizationManager.Instance.TenjinInstance.SendEvent("Upgrade:" + transform.name + ":To level " + m_CurrentLevel.ToString());

        FeedbackManager.Instance.PlayRewardParticles();


    }

	protected Color defaultUpgradeCostColor;

	public void FailedUpgradeAnimation ()
	{
		defaultUpgradeCostColor = UpradeCostDisplayer.color;
		UpradeCostDisplayer.color = Color.red;

		if (SoundManager.Instance != null)
			SoundManager.Instance.Play (SoundManager.Instance.FailedUpgrade);
		LeanTween.cancel (ButtonGO);
		LeanTween.moveLocalX (ButtonGO, localXNormal + 5f, 0.05f).setLoopPingPong (3).setOnComplete (c => {
			UpradeCostDisplayer.color = defaultUpgradeCostColor;

		});

        if (MonetizationManager.Instance != null && MonetizationManager.Instance.isRewardedAdAvailable)
        {
            UIController.Instance.ShowPanel(7, false);
        }

    }

	

}

