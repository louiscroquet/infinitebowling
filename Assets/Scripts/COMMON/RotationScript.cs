﻿using UnityEngine;
using System.Collections;

public class RotationScript : MonoBehaviour {

    public Vector3 rotationSpeed;
    private float startingRotationSpeed;
    public float maxRotationSpeed;
    public float speedVariation = 0.2f;

    public bool speedUp;
    bool speedDown;

    public bool pingPong;
    public Vector3 pingPongRotation;
    public float pingPongTime;

	private Transform selfTransform;

	// Use this for initialization
	void Start () {
        //startingRotationSpeed = rotationSpeed;
        
        selfTransform = transform;

        if (pingPong)
        {
            LeanTween.rotateLocal(gameObject, pingPongRotation, pingPongTime).setLoopPingPong(0);
        }
	}
	
	// Update is called once per frame
	void Update () {

        if (pingPong == false)
        {
            selfTransform.Rotate(rotationSpeed.x * Time.deltaTime, rotationSpeed.y * Time.deltaTime, rotationSpeed.z * Time.deltaTime);
        }
		
	}
/*
    public void SpeedUp()
    {
        //si la vitesse de rotation atteint la vitesse de rotation minimale, on accélère
        if (rotationSpeed <= startingRotationSpeed)
        {
            speedUp = true;
            speedDown = false;
        }
        // si la vitesse de rotation atteint la vitesse de rotation maximale, on ralentit
        else if (rotationSpeed >= maxRotationSpeed)
        {
            speedDown = true;
            speedUp = false;
        }
        if (speedUp == true)
        {
            rotationSpeed = rotationSpeed + speedVariation;
        }
        else if (speedDown == true)
        {
            rotationSpeed = rotationSpeed - speedVariation;
        }
    }
    */
}
