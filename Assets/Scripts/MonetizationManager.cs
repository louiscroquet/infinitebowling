﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.Advertisements;
//using Firebase;


public enum AdState
{
    NOT_READY,
    LOADING,
    READY,
    SHOWING,
    WATCHED_ENTIRELY,
    SKIPPED,
    FAILED_TO_LOAD
}

public class MonetizationManager : MonoBehaviour

{
    [SerializeField] private LayerMask _visibleThings;
    public static int LastIAPOpenSession
    {
        get
        {
            return PlayerPrefs.GetInt("LastIAPOpenSession", -1);
        }
        set
        {
            PlayerPrefs.SetInt("LastIAPOpenSession", value);

        }
    }


    public static int SessionCount
    {
        get
        {
            return PlayerPrefs.GetInt("SessionCount", 0);
        }
        set
        {
            PlayerPrefs.SetInt("SessionCount", value);

        }
    }

    public void IAP_PanelOpened()
    {
        LastIAPOpenSession = SessionCount;
    }

    public static bool IAPSale
    {
        get
        {
            int today = SessionCount;
            int lastOpened = LastIAPOpenSession;

            // Si IAP a été ouvert, 
            if (lastOpened != -1)
            {
                // SALE le lendemain
                if (today == lastOpened + 1)
                {
                    return true;
                }

            }

            // Dans tous les cas
            {
                // Sale toutes les 5 sessions : 
                if (SessionCount % 5 == 0 && SessionCount != 0 && SessionCount != 1)
                {
                    return true;
                }
            }

            return false;
        }
    }

    public static MonetizationManager Instance;

    public bool ActivateAds;
    public bool ActivateBanner;
    public bool TEST_MODE;
    public string TestDeviceId;

    public bool ResetTimerWithRewarded = true;
    private bool hasCompletedLastRewarded;

    private int interstitialTotalCount = 0;
    private int rewardedTotalCount = 0;


    [Header("Ad Settings")]
    public float AdShowingFrequency = 30f;
    private float lastTimeAnAdShowed;

    private string lastRewardTypeShowing;

    public static List<RewardedButton> rewardedButtons = new List<RewardedButton>();


    private bool m_IsRewardedAvailable = false;
    private bool m_IsInterstitialAvailable = false;
    public bool isRewardedAdAvailable
    {
        get
        {
#if UNITY_EDITOR
            return true;
#else
            return MaxSdk.IsRewardedAdReady(RewardedAdUnitID);// return m_IsRewardedAvailable;//IronSource.Agent.isRewardedVideoAvailable (); //RewardedAdState == AdState.READY;
#endif
        }
    }

    public bool isInterstitialAdAvailable
    {
        get
        {
#if UNITY_EDITOR

            return true;

#else
            return MaxSdk.IsInterstitialReady(InterstitialAdUnitID);//return m_IsInterstitialAvailable;//IronSource.Agent.isInterstitialReady (); //InterstitialAdState == AdState.READY;
#endif

        }
    }
    static int restartCount = 0;
    public static int CurrentSession
    {
        get
        {
            return restartCount;
        }
    }

    private AdState m_InterstitialAdState;
    public AdState InterstitialAdState
    {
        get
        {
            return m_InterstitialAdState;
        }
    }
    private AdState m_RewardedAdState;
    public AdState RewardedAdState
    {
        get
        {
            return m_RewardedAdState;
        }
    }


    [Header("MAX Settings")]
    public string MAX_iOS_interstitialAdUnitId;
    public string MAX_iOS_rewardedAdUnitId;
    public string MAX_iOS_bannerAdUnitId;

    public string MAX_Android_interstitialAdUnitId;
    public string MAX_Android_rewardedAdUnitId;
    public string MAX_Android_bannerAdUnitId;


    public string InterstitialAdUnitID
    {
        get
        {
#if UNITY_ANDROID 
            return MAX_Android_interstitialAdUnitId;

#else
            return MAX_iOS_interstitialAdUnitId;

#endif
        }
    }
    public string RewardedAdUnitID
    {
        get
        {
#if UNITY_ANDROID 
            return MAX_Android_rewardedAdUnitId;

#else
            return MAX_iOS_rewardedAdUnitId;

#endif
        }
    }
    public string BannerAdUnitID
    {
        get
        {
#if UNITY_ANDROID 
            return MAX_Android_bannerAdUnitId;

#else
            return MAX_iOS_bannerAdUnitId;

#endif
        }
    }

    public Color MAX_BannerBackgroundColor;
    private void Awake()
    {

        if (MonetizationManager.Instance == null)
        {
            Instance = this;
            GameAnalytics.Initialize();
            DontDestroyOnLoad(gameObject);
            interstitialTotalCount = PlayerPrefs.GetInt("MM_interstitialTotalCount", 0);
            rewardedTotalCount = PlayerPrefs.GetInt("MM_rewardedTotalCount", 0);
            if (Camera.main != null)
                m_CameraDefaultFar = Camera.main.farClipPlane;
            SessionCount = SessionCount + 1;
        }
        else
        {
            restartCount++;

            Destroy(gameObject);
        }

        RemoteSettings.Updated += SetRemoteSettings;
        GDPRManager.OnConsentStateReady += GDPRManager_OnConsentStateReady;


    }

    private void GDPRManager_OnConsentStateReady(bool consentState)
    {
        MaxSdk.SetHasUserConsent(consentState);
        if (ActivateBanner)
            LoadBannerAd();

    }

    private List<Action> callbacksToRun = new List<Action>();

    // Use this for initialization
    void Start()
    {

        ResetTime();
        InitializeAds();
        InvokeRepeating("UpdateAllRewardedButtonStates", 0.5f, 0.5f);
        restartCount = 0;
        foreach (var rb in rewardedButtons)
        {
            rb.Initialize();
        }




    }

    private void SetRemoteSettings()
    {
        if (RemoteSettings.HasKey("interstitial_frequency"))
        {
            AdShowingFrequency = RemoteSettings.GetFloat("interstitial_frequency");
        }

    }

    private void Update()
    {
        for (int i = 0; i < callbacksToRun.Count; i++)
        {
            var callback = callbacksToRun[0];
            if (callback != null) callback();
            callbacksToRun.RemoveAt(0);

        }
    }

    private int interstitialLoadTries = 0;
    private int rewardedLoadTries = 0;
    void CheckInterstitialCallbackState()
    {
        switch (m_InterstitialAdState)
        {
            case AdState.FAILED_TO_LOAD:
                interstitialLoadTries++;
                //RequestAdMobInterstitial ();
                break;
            case AdState.SKIPPED:
            case AdState.WATCHED_ENTIRELY:

                //  RequestAdMobInterstitial ();
                break;

        }
    }

    void CheckRewardedCallbackState()
    {
        switch (m_InterstitialAdState)
        {
            case AdState.FAILED_TO_LOAD:
                // RequestRewardBasedVideo ();
                break;
            case AdState.SKIPPED:
            case AdState.WATCHED_ENTIRELY:
                //  RequestRewardBasedVideo ();
                break;

        }
    }

    public void ResetTime()
    {
        lastTimeAnAdShowed = Time.time;
    }

    private string AdMobAppId;
    public string IronSourceAppIDAndroid;
    public string IronSourceAppIDiOS;
    private string IronSourceAppKey
    {
        get
        {
#if UNITY_ANDROID
            return IronSourceAppIDAndroid; //Dunk star"8db0c76d";
#else
            return IronSourceAppIDiOS; //Dunk star "8ca0da9d";
#endif
        }
    }
    void InitializeAds()
    {

        MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) => {
            // AppLovin SDK is initialized, start loading ads
            InitInterstitialVideo();
            InitRewardedVideo();
            if (TEST_MODE)
            {
                // Show Mediation Debugger
                MaxSdk.ShowMediationDebugger();
            }


            if (GDPRManager.Instance != null)
            {
                if (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.Applies)
                {
                    // Show user consent dialog
                    if (GDPRManager.Instance.ConsentAsked)
                    {
                        MaxSdk.SetHasUserConsent(GDPRManager.Instance.HasConsent);

                        if (ActivateBanner)
                            LoadBannerAd();
                    }
                    else
                    {
                        GDPRManager.Instance.AskConsent();

                    }
                }
                else if (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.DoesNotApply)
                {
                    // No need to show consent dialog, proceed with initialization
                    if (ActivateBanner)
                        LoadBannerAd();
                }
                else
                {
                    // Consent dialog state is unknown. Proceed with initialization, but check if the consent
                    // dialog should be shown on the next application initialization
                    if (ActivateBanner)
                        LoadBannerAd();
                }

            }
            else
            {
                if (ActivateBanner)
                    LoadBannerAd();
            }


        };

        MaxSdk.SetSdkKey("JFWFDPr8O8mfshiMpbVXe_vZWkfnf47-KdDX22fRBQ517pNxOtS2tMbO4F9lKHifwxBy-rO9GUANHTIgQmiI7I");
        MaxSdk.InitializeSdk();

        m_InterstitialAdState = AdState.NOT_READY;
        m_RewardedAdState = AdState.NOT_READY;
        if (TEST_MODE)
        {
            Debug.LogError("CANT VALIDATE INTEGRATION with MAX");

            // IronSource.Agent.validateIntegration ();

        }

        if (!ActivateAds)
        {
            return;
        }

        if (TEST_MODE)
        {
            Debug.LogError("Ad Test Mode enabled");
        }

        // ADMOB
        // Initialize the Google Mobile Ads SDK.



    }



    public void LoadBannerAd()
    {

        if (UserConfig.NoAds)
            return;

        MaxSdkCallbacks.OnBannerAdLoadedEvent += OnBannerAdLoaded;
        MaxSdkCallbacks.OnBannerAdLoadFailedEvent += OnBannerAdFailedToLoad;
        // Banners are automatically sized to 320x50 on phones and 728x90 on tablets
        // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments
        MaxSdk.CreateBanner(BannerAdUnitID, MaxSdkBase.BannerPosition.BottomCenter);

        // Set background or background color for banners to be fully functional
        MaxSdk.SetBannerBackgroundColor(BannerAdUnitID, MAX_BannerBackgroundColor);


    }

    private void OnBannerAdFailedToLoad(string arg1, int arg2)
    {
        //  HideBanner();
        CancelInvoke("LoadBannerAd");
        Invoke("LoadBannerAd", 2f);

    }

    private void OnBannerAdLoaded(string obj)
    {
        ShowBanner();
    }

    public void ShowBanner()
    {
        if (UserConfig.NoAds)
        {
            HideBanner();
            return;
        }
        UIController.Instance.SetScreenBottomOffset(100);
        MaxSdk.ShowBanner(BannerAdUnitID);
    }

    public void HideBanner()
    {
        UIController.Instance.SetScreenBottomOffset(0);
        MaxSdk.HideBanner(BannerAdUnitID);
    }

    public void ShowInterstitial()
    {
        if (!ActivateAds)
        {
            return;
        }

        if (UserConfig.NoAds)
            return;

        if ((Time.time - lastTimeAnAdShowed) < AdShowingFrequency)
        {
            return;
        }
        // ADMOB
        Debug.Log("Try to show an ad");
#if UNITY_EDITOR
        Debug.LogError("Interstitial ad would be here. Skipping for editor");
        ResetTime();

#else
        if (MaxSdk.IsInterstitialReady(InterstitialAdUnitID)) {
        SetCameraState(false);
        
        ResetTime();
        interstitialTotalCount++;
         PlayerPrefs.SetInt("MM_interstitialTotalCount", interstitialTotalCount);
       
            GameAnalytics.NewDesignEvent ("Interstitial:Displayed");
         System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
          if(interstitialTotalCount<=20 || interstitialTotalCount%5==0)
                AppsFlyer.trackRichEvent("af_Ads_"+interstitialTotalCount.ToString()+"_Interstitial_Watched", richEvent);
        richEvent.Add("af_interstitialCount",interstitialTotalCount.ToString());
        AppsFlyer.trackRichEvent("af_Ads_Interstitial_Displayed", richEvent);


            MaxSdk.ShowInterstitial(InterstitialAdUnitID);
            PauseGame ();

        } else {
            Debug.Log ("No Interstitial to display");

        }
#endif

    }

    public void ShowRewarded(string type, RewardedButton buttonClicked)
    {
        lastRewardTypeShowing = type;
        lastRewardedButtonClicked = buttonClicked;

#if UNITY_EDITOR
        LeanTween.delayedCall(2f, Editor_ValidateRewarded).setIgnoreTimeScale(true);

        return;
#endif

        if (MaxSdk.IsRewardedAdReady(RewardedAdUnitID))
        {
            SetCameraState(false);

            rewardedTotalCount++;
            PlayerPrefs.SetInt("MM_rewardedTotalCount", rewardedTotalCount);
            //  Debug.Log("888888 SHOW REWARDED");
            m_RewardedAdState = AdState.NOT_READY;
            GameAnalytics.NewDesignEvent("Rewarded:Displayed:" + type);
            System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
            AppsFlyer.trackRichEvent("af_Ads_Rewarded_DisplayedType_" + type, richEvent);

            if (rewardedTotalCount <= 20 || rewardedTotalCount % 5 == 0)
                AppsFlyer.trackRichEvent("af_Ads_" + rewardedTotalCount.ToString() + "_Rewarded_Watched", richEvent);

            richEvent.Add("af_rewardType", type);
            richEvent.Add("af_rewardedCount", rewardedTotalCount.ToString());
            AppsFlyer.trackRichEvent("af_Ads_Rewarded_Displayed", richEvent);

            hasCompletedLastRewarded = false;
            //Debug.LogWarning ("444 Displaying Reward");

            MaxSdk.ShowRewardedAd(RewardedAdUnitID);
            HideAllRewardedButtons();

            PauseGame();

        }
        else
        {
            //Debug.LogWarning ("444 NO Reward Available");
            Debug.LogWarning("No rewarded loaded to display");
            HideAllRewardedButtons();

        }
    }

    public void Editor_ValidateRewarded()
    {
        Debug.LogError("Auto validate Rewarded video in editor");

        callbacksToRun.Add(ValidateRewarded);
    }

    // CALLBACKS

    public RewardedButton lastRewardedButtonClicked;

    void TryReward()
    {
        //  Debug.Log("888888 TRY REWARD");
        if (hasCompletedLastRewarded)
        {
            //    Debug.Log("888888 HAS COMPLETED");
            ValidateRewarded();
        }

        else
        {
            //    Debug.Log("888888 CLOSED TOO SOON");

            CloseRewardedBeforeValidation();
        }

        hasCompletedLastRewarded = false;
    }
    void ValidateRewarded()
    {

        if (lastRewardedButtonClicked != null)
        {
            Debug.Log("888888 ABOUT TO COLLECT");

            lastRewardedButtonClicked.CollectReward();
        }
        GameAnalytics.NewDesignEvent("Rewarded:Watched:" + lastRewardTypeShowing);
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();

        AppsFlyer.trackRichEvent("af_Ads_Rewarded_Watched", richEvent);
        if (ResetTimerWithRewarded)
            ResetTime();
        hasCompletedLastRewarded = false;
    }

    void CloseRewardedBeforeValidation()
    {

        if (lastRewardedButtonClicked != null)
        {
            lastRewardedButtonClicked.RewardedClosed();

        }
        GameAnalytics.NewDesignEvent("Rewarded:Closed:" + lastRewardTypeShowing);
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();

        AppsFlyer.trackRichEvent("af_Ads_Rewarded_Closed", richEvent);
    }

    #region INTERSTITIAL
    public void InitInterstitialVideo()
    {
        // Attach callback
        MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
        MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialFailedEvent;
        MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += InterstitialFailedToDisplayEvent;
        MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialDismissedEvent;

        // Load the first interstitial
        LoadInterstitial();
    }


    private void LoadInterstitial()
    {

        if (!InterstitialAdUnitID.Equals(""))
            MaxSdk.LoadInterstitial(InterstitialAdUnitID);
        else
            Debug.LogError("[ERROR] MAX Interstitial Ad Unit id is empty");
    }

    private void OnInterstitialLoadedEvent(string adUnitId)
    {
        // Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(interstitialAdUnitId) will now return 'true'
    }

    private void OnInterstitialFailedEvent(string adUnitId, int errorCode)
    {
        // Interstitial ad failed to load. We recommend re-trying in 3 seconds.
        CancelInvoke("LoadInterstitial");
        Invoke("LoadInterstitial", 1);
    }

    private void InterstitialFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        // Interstitial ad failed to display. We recommend loading the next ad
        LoadInterstitial();
        SetCameraState(true);

    }

    private void OnInterstitialDismissedEvent(string adUnitId)
    {
        // Interstitial ad is hidden. Pre-load the next ad
        LoadInterstitial();
        SetCameraState(true);
    }

    #endregion

    #region REWARDED
    public void InitRewardedVideo()
    {

        // Attach callback
        MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
        MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
        MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
        MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
        MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
        MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
        MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;



        // Load the first RewardedAd
        LoadRewardedAd();

    }

    private void LoadRewardedAd()
    {
        if (!RewardedAdUnitID.Equals(""))
            MaxSdk.LoadRewardedAd(RewardedAdUnitID);
        else
            Debug.LogError("[ERROR] MAX Interstitial Ad Unit id is empty");
    }

    private void OnRewardedAdLoadedEvent(string adUnitId)
    {
        // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'
    }

    private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
    {
        // Rewarded ad failed to load. We recommend re-trying in 3 seconds.
        Invoke("LoadRewardedAd", 3);
    }

    private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        // Rewarded ad failed to display. We recommend loading the next ad
        LoadRewardedAd();
        SetCameraState(true);
    }

    private void OnRewardedAdDisplayedEvent(string adUnitId)
    {
        if (UIController.Instance != null)
            UIController.Instance.SetLoadingLockScreenState(true);
        shouldGiveReward = false;
        SetCameraState(false);

    }

    private void OnRewardedAdClickedEvent(string adUnitId) { }

    private void OnRewardedAdDismissedEvent(string adUnitId)
    {
        UIController.Instance.SetLoadingLockScreenState(false);

        // Rewarded ad is hidden. Pre-load the next ad
        LoadRewardedAd();
        SetCameraState(true);
    }

    private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
    {

        m_RewardedAdState = AdState.NOT_READY;
        shouldGiveReward = true;
        hasCompletedLastRewarded = true;
        // Rewarded ad was displayed and user should receive the reward
        rewardedClosed = true;
        SetCameraState(true);

        if (shouldGiveReward)
        {
            // ValidateRewarded ();
            callbacksToRun.Add(TryReward);
            callbacksToRun.Add(ResumeGame);
            UIController.Instance.SetLoadingLockScreenState(false);

            rewardGiven = true;
        }
    }


    bool rewardGiven = false;
    bool shouldGiveReward = false;
    bool rewardedClosed = false;



    public void UpdateAllRewardedButtonStates()
    {
        //        Debug.Log("nb of rewarded buttons " + rewardedButtons.Count);
        if (isRewardedAdAvailable)
        {
            ShowAllRewardedButtons();
        }
        else
        {
            HideAllRewardedButtons();
        }

    }
    public void HideAllRewardedButtons()
    {
        foreach (RewardedButton rewardedButton in rewardedButtons)
        {
            if (rewardedButton == null)
            {
                //   rewardedButtons.Remove (rewardedButton);
            }
            else
            {
                rewardedButton.SetStatus(false);

            }
        }
    }
    public void ShowAllRewardedButtons()
    {
        // Debug.Log("Show all rewarded buttons");
        foreach (RewardedButton rewardedButton in rewardedButtons)
        {
            if (rewardedButton == null)
            {
                // rewardedButtons.Remove (rewardedButton);
            }
            else
            {
                rewardedButton.SetStatus(true);
            }

        }
    }

    #endregion

    #region BANNER

    public float CurrentScreenOffsetNeeded
    {
        get
        {

            return 0f;

        }
    }

    void InitializeBanner()
    {
        // Banners are automatically sized to 320x50 on phones and 728x90 on tablets
        // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments
        MaxSdk.CreateBanner(BannerAdUnitID, MaxSdkBase.BannerPosition.BottomCenter);

        // Set background or background color for banners to be fully functional
        MaxSdk.SetBannerBackgroundColor(BannerAdUnitID, MAX_BannerBackgroundColor);

    }



    #endregion

    #region NO_ADS
    public void RemoveAds()
    {
        HideBanner();
        var noAdsButton = FindObjectOfType<BuyNoAdsButton>();
        if (noAdsButton != null)
        {
            noAdsButton.gameObject.SetActive(false);
        }

    }
    #endregion
    void PauseGame()
    {
        //Time.timeScale = 0f;
        if (SoundManager.Instance != null)
        {
            if (SoundManager.Instance.AmbiantSound != null)
                SoundManager.Instance.AmbiantSound.Pause();
        }
    }

    void ResumeGame()
    {
        //  Time.timeScale = 1f;
        if (SoundManager.Instance != null)
        {
            if (SoundManager.Instance.AmbiantSound != null)
                SoundManager.Instance.AmbiantSound.UnPause();
        }
    }



    private Camera m_CurrentCamera;
    private float m_CameraDefaultFar = 100F;

    void SetCameraState(bool enabled)
    {
        if (m_CurrentCamera == null)
            m_CurrentCamera = Camera.main;

        if (m_CurrentCamera == null)
            m_CurrentCamera = FindObjectOfType<Camera>();

        if (m_CurrentCamera != null)
        {
            if (CameraManager.Instance != null)
            {
                CameraManager.Instance.SetCamerasEnabled(enabled);
            }
            else
            {
                if (enabled)
                {
                    m_CurrentCamera.cullingMask = _visibleThings;//~0; // On met le culling mask sur "Everything"

                }
                else
                {
                    m_CurrentCamera.cullingMask = LayerMask.GetMask();// On met le culling mask sur "Nothing"
                }
            }




            //  m_CurrentCamera.enabled = enabled;
        }
        else
        {
            Debug.LogError("No camera found");
        }
    }
}