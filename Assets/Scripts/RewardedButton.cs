﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardedButton : MonoBehaviour {

    public static bool collectedDuringThisGame;

    protected virtual void Start () {
        if (MonetizationManager.Instance != null) {
            Initialize ();

        }
        // UIController.Instance.HidePanelInstantly(8);

    }

    bool inited = false;
    public void Initialize () {
        // Debug.LogError("INIT " + name);
        //m_SelfGameObject = gameObject;
        if (MonetizationManager.Instance != null) {

            if (!MonetizationManager.rewardedButtons.Contains (this))
                MonetizationManager.rewardedButtons.Add (this);

            if (MonetizationManager.Instance.isRewardedAdAvailable) {
                EnableButton ();
            } else {
                DisableButton ();
            }
        }
        inited = true;

    }
    private void OnEnable () {
        UpdateStatus ();
    }

    private void OnDestroy () {
        if (MonetizationManager.Instance != null) {
            if (MonetizationManager.rewardedButtons.Contains (this))
                MonetizationManager.rewardedButtons.Remove (this);
        }
    }

    public void UpdateStatus () {
        if (!inited) {
            Initialize ();
            return;
        }
        if (MonetizationManager.Instance != null) {
            if (MonetizationManager.Instance.isRewardedAdAvailable) {
                EnableButton ();
            } else {
                DisableButton ();
            }
        }

    }
    public void SetStatus (bool isActive) {
        if (isActive) {
            EnableButton ();
        } else {
            DisableButton ();
        }
    }

    public virtual void EnableButton () {
      //  Debug.Log ("Enable : " + name);

        gameObject.SetActive (true);

    }

    public void DisableButton () {
        //Debug.Log ("Disable : " + name);

        gameObject.SetActive (false);
    }

    public virtual void CollectReward () {
        DisableButton ();
        //FeedbackManager.Instance.PlayRewardParticles();
        VibrationManager.Instance.VibrateSuccess ();
    }

    public virtual void RewardedClosed () {

    }

    public virtual void ShowRewardedVideo (string placementId) {
        if (MonetizationManager.Instance != null) {
            MonetizationManager.Instance.ShowRewarded (placementId, this);
        } else {
            Debug.LogError ("No Monetization Manager found");
        }
    }
}