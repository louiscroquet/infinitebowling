﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CI_Trail : CI_SwitchGameObject {

    public override void Action()
    {
        base.Action();

        Preview();
    }


    public override void Preview()
    {

        if (UnlockByLevel && !Owned)
        {
            return;
        }


        GameObject pr = Instantiate(previewImage, ShopMenu.Instance.previewPanel.transform);
        pr.transform.SetSiblingIndex(0);
        RectTransform prTr = pr.GetComponent<RectTransform>();
        //pr.transform.parent = ShopMenu.Instance.ballPreview.transform;
        prTr.localPosition = ShopMenu.Instance.trailPreviewImagePosition;
        pr.transform.localRotation = Quaternion.Euler(0f, 0f, 250f);
        pr.transform.localScale = Vector3.one * 3f;
       

        if (ShopMenu.Instance.lastTrailPreview != null)
        {
            Destroy(ShopMenu.Instance.lastTrailPreview);
        }
        ShopMenu.Instance.lastTrailPreview = pr;
    }

    public override void DiscountPreview()
    {
        ShopMenu.Instance.ballPreview.SetActive(true);
        ShopMenu.Instance.pinPreview.SetActive(false);
        Preview();
    }
}
