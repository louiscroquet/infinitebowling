﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTheme : MonoBehaviour
{

	public Color BackgroundColor;
	public Color GradientColor;
	public Color FogColor;
	public Material GroundMaterial;
	public Color GroundGradientsColor;
	public Texture PinColor;
	public Texture PinStripeColor;
	public Color ThemeSwitcherColor;
	public Texture GroundLineRendererTexture;
    public Color obstacleColor;
    public Color movingObstacleColor;
}
