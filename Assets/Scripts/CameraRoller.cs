﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRoller : MonoBehaviour
{
	public Transform Target;
	public bool UseLocalPosition = false;
	public float MaxRollAngle = 10f;
	public float MaxTargetAmplitude = 5f;


	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Target != null) {
			if (UseLocalPosition) {
				float t = (Mathf.Clamp (Target.localPosition.x, -MaxTargetAmplitude, MaxTargetAmplitude) + MaxTargetAmplitude * 0.5f) / MaxTargetAmplitude;
				float roll = Mathf.Lerp (-MaxRollAngle, MaxRollAngle, t);
				transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, roll);
			} else {
				float t = (Mathf.Clamp (Target.position.x, -MaxTargetAmplitude, MaxTargetAmplitude) + MaxTargetAmplitude * 0.5f) / MaxTargetAmplitude;
				float roll = Mathf.Lerp (-MaxRollAngle, MaxRollAngle, t);
				transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, roll);
			}

		}
	}
}
