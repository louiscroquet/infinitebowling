﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pin : MonoBehaviour {
    //[System.NonSerialized]
    public PinGroup ParentGroup;

    public bool hasBeenHit;
    public Rigidbody m_rigidbody;
    public Transform CenterOfMass;

    public GameObject PinShadow;
    public Collider PinCollider;

    public ParticleSystem HitParticles;

    public MeshRenderer PinBodyRenderer;
    public MeshRenderer PinBorderRenderer;

    void Awake () {
#if UNITY_EDITOR
        //UnityEditor.PrefabUtility.DisconnectPrefabInstance (gameObject);
#endif

    }

    // Use this for initialization
    void Start () {
        SpawnManager.Instance.spawnedPins.Add (this);

        if (CenterOfMass != null)
            m_rigidbody.centerOfMass = CenterOfMass.localPosition;

        int colorNumber = Mathf.FloorToInt (transform.position.z / (SpawnManager.Instance.distanceBetweenColorswitch));
        colorNumber = Mathf.Clamp (colorNumber, 0, SpawnManager.Instance.stickerColors.Count - 1);

        InvokeRepeating ("CheckIfInactive", 0f, 0.25f);
    }

    void Initialize () {
        /*if(gameObject.GetComponent<MeshRenderer>() != null)
        {
            PinBodyRenderer = gameObject.transform.Find("model").GetComponent<MeshRenderer>();

            if(PinBodyRenderer == null)
            {
                Debug.LogError("Please rename the 3D pin object name by model");
            }
        }
        else
        {
            PinBodyRenderer = gameObject.GetComponent<MeshRenderer>();
        }
        */

    }

    public void CheckIfInactive () {
        if (GameManager.Instance.isGameOver)
            return;
        if (Player.Instance.TWISTY) {
            return;
        }

        if (transform.position.y < -2f) {
            //		} || transform.position.z < Player.Instance.transform.position.z - 10f) {
            CancelInvoke ("CheckIfInactive");
            gameObject.SetActive (false);
        } else if (transform.position.z < Player.Instance.transform.position.z - 0.5f) {
            CancelInvoke ("CheckIfInactive");

            if (!hasBeenHit) {
                //Debug.LogError ("Missed pin : " + name);
                GameManager.Instance.DecreaseCombo ();
                //	GameManager.Instance.BreakCombo ();
                //	FeedbackManager.Instance.ShowMissedPinFeedback (transform.position);
            }
            //gameObject.SetActive (false);
            //Explode (transform.position);
            LeanTween.scale (gameObject, Vector3.zero, 3f).setOnComplete (w => {
                gameObject.SetActive (false);
            });
        }
    }

    public void Hit (Vector3 pos) {

        if (hasBeenHit == false) {
            //PinCollider.enabled = false;
            hasBeenHit = true;
            //HitParticles.Play ();
            //PinShadow.SetActive (false);

            if (ParentGroup != null) {
                ParentGroup.Strike ();
            } else {

                CameraFeedback.Instance.ScreenshakeSoft ();
                ScorePin (GameManager.Instance.OnFire);
                //Player.Instance.ScorePin (transform.position);
                //iOSHapticFeedback.Instance.Trigger (iOSHapticFeedback.iOSFeedbackType.ImpactLight);
                VibrationManager.Instance.VibrateLight ();
                if (SoundManager.Instance != null)
                    SoundManager.Instance.PlayPinSound ();

            }
            // hit pin single in cruise phase
            if (Player.Instance.cruisePhase == true) {

                //Player.Instance.AddTime(Player.Instance.singlePinTimeBonusValue);
                //FeedbackManager.Instance.PlayFeedbackText (transform.position, 1f, Player.Instance.currentSpeed / 2f, 2f, 1f, 1f, "+" + (Player.Instance.singlePinScoreBonus * Player.Instance.currentMutliplier).ToString (), Player.Instance.scoreBonusColor);
            }
            // hit pin during launchPhase
            if (Player.Instance.gameEnded == false && Player.Instance.launchingPhase == true) {

                //FeedbackManager.Instance.PlayFeedbackText (transform.position, 1f, Player.Instance.currentSpeed / 3f, 2f, 1f, 0.6f, "+" + (Player.Instance.singlePinScoreBonus * Player.Instance.currentMutliplier).ToString (), Player.Instance.scoreBonusColor);
            }

            //FeedbackManager.Instance.PlaySmokeParticles (transform.position + transform.forward * 2f + transform.up * 2f, transform.forward);
            EjectPin (pos);
        }
    }

    int pinValue;
    public void ScorePin (bool fire) {
        int value = (Player.Instance.pinCashUpgrade.CurrentLevel + 1);
        if (fire) {
            value *= 2;

        } else {
            // value = + 1;

        }
        pinValue = value;
        GameManager.Instance.Score (value);
    }

    void OnTriggerEnter (Collider c) {

    }

    void OnCollisionEnter (Collision c) {
        if (c.gameObject.CompareTag ("Pin")) { // HIT BY RICOCHET
            return;
            hasBeenHit = true;
            var p = c.gameObject.GetComponent<Pin> ();
            p.Hit (c.contacts[0].point);
            //	p.hasBeenHit = true;
            //	p.PinShadow.SetActive (false);
            HitParticles.Play ();
            PinShadow.SetActive (false);
            hasBeenHit = true;
            if (ParentGroup != null && !hasBeenHit) {
                //GameManager.Instance.Score (1 + GameManager.Instance.CashUpgrade.CurrentLevel);
                Player.Instance.ScorePin (transform.position);
            }
            EjectPin (c.contacts[0].point);
            if (ParentGroup != null) {
                //		ParentGroup.Strike ();
            }
        }
    }

    private bool exploded = false;

    public void EjectPin (Vector3 pos) {
        if (exploded)
            return;

        if (Player.Instance.OnFire) {
            FeedbackManager.Instance.PlayExplosionParticles (transform.position + new Vector3 (0f, 1f, 1f));

            for (int i = 0; i < PinBodyRenderer.materials.Length; i++) {
                PinBodyRenderer.materials[i] = GameManager.Instance.FirePinExplodeMaterial;
            }

            //PinBodyRenderer.material = GameManager.Instance.FirePinExplodeMaterial;
            PinBorderRenderer.material = GameManager.Instance.FirePinExplodeMaterial;

            if (ParentGroup == null)
                SoundManager.Instance.Play (SoundManager.Instance.PinFireSound);
        }
        exploded = true;
        HitParticles.Play ();

        m_rigidbody.velocity = Vector3.zero;
        //	m_rigidbody.AddForce (new Vector3 (0f, 5000f, 15000f));

        PinShadow.SetActive (false);

        PinCollider.enabled = false;

        m_rigidbody.AddForce (transform.right * 3000f * Player.Instance.lastExplosionX * Random.Range (0.75f, 1.25f));

        m_rigidbody.AddForce (transform.up * 6000f * Random.Range (0.85f, 1.15f));
        m_rigidbody.AddForce (transform.forward * 30000f * Random.Range (0.75f, 1.25f));
        m_rigidbody.AddTorque (transform.forward * 15000f * Random.Range (-3f, 3f)); //new Vector3 (0f, 0f, 5000f));
        m_rigidbody.useGravity = true;

        if (Random.Range (0f, 100f) < 50f) {
            Player.Instance.lastExplosionX *= -1f;
        }

        //Invoke("Fall", 1.5f);

        LeanTween.scale (gameObject, Vector3.zero, 3f).setDelay (3f).setOnComplete (w => {
            gameObject.SetActive (false);
        });
        //Debug.Log ("no explosion force : ");
        //GetComponentInChildren<MeshRenderer> ().enabled = false;
        Invoke ("ExplodeAnimation", 0.25f);
        //	m_rigidbody.AddExplosionForce (50f, pos, 2f, 0.5f, ForceMode.Impulse);
    }

    public void ExplodeAnimation () {
        //  m_rigidbody.isKinematic=true;
        m_rigidbody.useGravity = false;
        //   m_rigidbody.angularVelocity*= 0.5f;//Vector3.zero;
        // FeedbackManager.Instance.Show3DTMPFeedback("+$1",transform.position,0.5f);
        m_rigidbody.velocity = Vector3.zero;
        // var mainEP = ExplosionParticles.main;
        // var neoGradient = new ParticleSystem.MinMaxGradient (PinBorderRenderer.material.color,PinBodyRenderer.material.color);
        // mainEP.startColor=neoGradient   ;
        SoundManager.Instance.PlayPinExplosionSound ();
        FeedbackManager.Instance.PlayPinExplosionParticles (transform.position);
        FeedbackManager.Instance.PlayFeedbackText (transform.position, 30f, 0f, 0f, 0f, 0.5f, "+$" + pinValue.ToString (), Color.white);

        LeanTween.scale (PinBodyRenderer.gameObject, Vector3.zero, 0.25f).setEase (LeanTweenType.easeInBack);
    }

    void Fall () {
        m_rigidbody.AddForce (transform.up * -2000f);
    }

    bool adapted = false;

    public void AdaptToCurve () {
        if (adapted)
            return;
        adapted = true;
        float distance = transform.position.z;

        m_rigidbody.useGravity = false;

        float offset = transform.position.x;

        transform.position = CurveManager.Instance.CalcPositionByDistance (distance);

        transform.forward = CurveManager.Instance.CalcTangentByDistance (distance);
        transform.position += transform.right * offset;
    }
}