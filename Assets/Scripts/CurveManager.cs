﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class CurveManager : MonoBehaviour
{

	public static  CurveManager Instance;
	public List<BGCurve> AvailableCurves = new List<BGCurve> ();



	public List<float> GeneratedCurveDistances = new List<float> ();
	public List<BGCurve> GeneratedCurves = new List<BGCurve> ();
	public List<BGCurveBaseMath> GeneratedMath = new List<BGCurveBaseMath> ();
	public float CurrentCurveDistance = 0f;

	void Awake ()
	{
		Instance = this;
		AddCurveToGenerated ();
	}
	// Use this for initialization
	void Start ()
	{
	}

	public float DEBUG_T = 0f;
	// Update is called once per frame
	void Update ()
	{
		//	Debug.Log ("Debug : " + CalcTangentByDistance (DEBUG_T).ToString ());
		
	}

	public Vector3 CalcPositionByDistance (float d)
	{
		float t = d;
		if (d > CurrentCurveDistance) {
			Debug.LogError ("d : " + d + " is out of current curve distance : " + CurrentCurveDistance);
			AddCurveToGenerated ();
			return CalcPositionByDistance (d);
			//	return new Vector3 (0f, 0f, -10f);
		}
		BGCurve c;
		Vector3 res = Vector3.zero;
		//	Debug.Log ("GeneratedMath : " + GeneratedMath.Count);
		for (int i = 0; i < GeneratedCurveDistances.Count - 1; i++) {
			
			if (d < GeneratedCurveDistances [i]) {
				c = GeneratedCurves [i];
				//var neo = new BGCurveBaseMath (c, new BGCurveBaseMath.Config (BGCurveBaseMath.Fields.PositionAndTangent));
				if (i > 0)
					t = d - GeneratedCurveDistances [i - 1];
				res = GeneratedMath [i].CalcPositionByDistance (t);
				return res;
			} else {
				//	t -= GeneratedCurveDistances [i];

			}
		}
		//	Debug.LogError ("No result found : reached end of generated curves array");
		AddCurveToGenerated ();

		return CalcPositionByDistance (d);



	}

	public Vector3 CalcTangentByDistance (float d)
	{
		float t = d;
		if (d >= CurrentCurveDistance) {
			//		Debug.LogError (" TANGENT d : " + d + " is out of current curve distance : " + CurrentCurveDistance);
			return Vector3.zero;
		}
		BGCurve c;
		Vector3 res = Vector3.zero;
		//	Debug.Log ("GeneratedMath : " + GeneratedMath.Count);
		for (int i = 0; i < GeneratedCurveDistances.Count - 1; i++) {

			if (d <= GeneratedCurveDistances [i]) {
				c = GeneratedCurves [i];
				//	var neo = new BGCurveBaseMath (c, new BGCurveBaseMath.Config (BGCurveBaseMath.Fields.PositionAndTangent));
				if (i > 0)
					t = d - GeneratedCurveDistances [i - 1];
				
				res = GeneratedMath [i].CalcTangentByDistance (t);
				return res;
			} else {
				//t -= GeneratedCurveDistances [i];

			}
		}
		//Debug.LogError (" TANGENT No result found : reached end of generated curves array");
		return res;

	}



	[ContextMenu ("Clear Curve")]
	public void ClearGenerated ()
	{
		foreach (var item in GeneratedCurves) {
			if (item != null) {
				if (Application.isPlaying)
					Destroy (item.gameObject);
				else
					DestroyImmediate (item.gameObject);
			}	
		}

		GeneratedCurveDistances.Clear ();
		GeneratedCurves.Clear ();
		GeneratedMath.Clear ();
		CurrentCurveDistance = 0f;

	}

	[ContextMenu ("Spawn Curve")]
	public void AddCurveToGenerated ()
	{
		bool forceFirstPoint = false;
		var model = AvailableCurves [Random.Range (0, AvailableCurves.Count)];
		BGCurve toAdd = Instantiate (model, transform);
		toAdd.gameObject.SetActive (true);
		toAdd.transform.rotation = model.transform.rotation;
		if (GeneratedCurves.Count == 0) {
			forceFirstPoint = true;

			toAdd.transform.position = Vector3.zero;
			toAdd.EventMode = BGCurve.EventModeEnum.NoEvents;
			toAdd.GetComponent<BGCcVisualizationLineRenderer> ().UpdateAtStart = false;
			//	var p = toAdd.Points [0];
			//	p.PositionWorld = new Vector3 (0f, 0f, -5f);
			toAdd.GetComponent<LineRenderer> ().SetPosition (0, new Vector3 (0f, 0f, -10f));

		} else {
			BGCurve lastCurve = GeneratedCurves [GeneratedCurves.Count - 1];
			toAdd.transform.position = lastCurve.Points [lastCurve.PointsCount - 1].PositionWorld;
			toAdd.GetComponent<BGCcVisualizationLineRenderer> ().FireChangedParams ();

			//toAdd.transform.forward = GeneratedMath [GeneratedMath.Count - 1].CalcTangentByDistanceRatio (1f);

			//toAdd.Points [0].PositionWorld = lastCurve.Points [lastCurve.PointsCount - 1].PositionWorld;
			//	toAdd.Points [0].ControlFirstWorld = lastCurve.Points [lastCurve.PointsCount - 1].ControlFirstWorld;
			//	toAdd.Points [0].ControlSecondWorld = lastCurve.Points [lastCurve.PointsCount - 1].ControlSecondWorld;

		}
		var groundLine = toAdd.GetComponentInChildren<BottomCurveLineRenderer> ();
		if (groundLine != null)
			groundLine.ScheduleCopy ();
		else
			Debug.LogError ("no groundline : " + toAdd.name);
		var neo = new BGCurveBaseMath (toAdd, new BGCurveBaseMath.Config (BGCurveBaseMath.Fields.PositionAndTangent));

		GeneratedCurves.Add (toAdd);
		GeneratedMath.Add (neo);
		CurrentCurveDistance += neo.GetDistance ();
		GeneratedCurveDistances.Add (CurrentCurveDistance);
		//toAdd.Changed ();
		//neo.Changed ();
		toAdd.GetComponent<LineRenderer> ().SetPosition (0, new Vector3 (0f, 0f, -10f));


	}

	[Header ("Point Generation")]
	public BGCurve Generated;
	public Vector3 MaxAmplitude;

	[ContextMenu ("Add Point to curve")]
	public void AutoGeneratePoint ()
	{
		if (Generated == null) {
			Debug.LogError ("No curve to add point to : ");
			return;
		} 
		Vector3 amp = MaxAmplitude;
		if (Random.Range (0, 100) > 50)
			amp.x *= -1f;
		
		var lastPoint = Generated.Points [Generated.PointsCount - 1];
		Vector3 neoPos = lastPoint.PositionWorld + amp;
		Vector3 c1 = amp * 0.5f;
		Vector3 c2 = amp * -0.5f;


		BGCurvePoint neoPoint = new BGCurvePoint (Generated, neoPos, BGCurvePoint.ControlTypeEnum.BezierSymmetrical, c2, c1, true);
		Generated.AddPoint (neoPoint);

	}

	public void UpdateGeneratedControlPoints ()
	{
		
	}
}
