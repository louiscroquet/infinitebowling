﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PïnCashUpgrade : Upgrade
{

	protected override void UpdateButtonState ()
	{
		if (UpgradeNameDisplayer != null)
			UpgradeNameDisplayer.text = UpgradeName.ToString ();
		if (UpradeCostDisplayer != null)
			UpradeCostDisplayer.text = "$" + ValueConvertor.ConvertDoubleToString(CurrentUpgradeCost);
		if (CurrentLevelDisplayer != null)
			CurrentLevelDisplayer.text = (CurrentLevel + 1).ToString () + "<size=50>$</size>";
	}
}
