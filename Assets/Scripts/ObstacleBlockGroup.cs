﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBlockGroup : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	bool exploded = false;
	bool adaptedToCurve = false;

	public void AdaptGroup ()
	{
		if (adaptedToCurve)
			return;
		adaptedToCurve = true;
		float distance = transform.position.z;

		float offset = transform.position.x;
		float offsety = transform.position.y;

		transform.position = CurveManager.Instance.CalcPositionByDistance (distance);

		transform.forward = CurveManager.Instance.CalcTangentByDistance (distance);
		transform.position += transform.right * offset;
		//transform.position += transform.up * offsety;
		foreach (var o in GetComponentsInChildren<ObstacleBlock>()) {
			//o.AdaptToCurve ();
			o.SetAdapted ();
		}
	}

	public void ExplodeGroup ()
	{
		if (exploded)
			return;
		exploded = true;
		foreach (var o in GetComponentsInChildren<ObstacleBlock>()) {
			o.Explode ();
		}

	}
}
